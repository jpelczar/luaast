// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


#include "LuaAST.h"
#include "LuaASTVisitor.h"
#include "LuaASTMatcher.h"
#include "LuaStringRef.h"
#include "LuaDocument.h"

namespace LuaAST {
    
    StringRef SourceLocation::toString(const LuaDocument * doc) const
    {
        return doc->midRef(offset, length);
    }
    
    AST::AST()
    {
    }
    
    AST::~AST()
    {
    }
    
    void AST::accept(ASTVisitor * visitor)
    {
        if(visitor->preVisit(this))
            accept0(visitor);
        visitor->postVisit(this);
    }
    
    bool AST::match(AST * pattern, ASTMatcher * matcher)
    {
        return match0(pattern, matcher);
    }
    
    bool AST::match(AST * ast, AST * pattern, ASTMatcher * matcher)
    {
        if(ast == pattern)
            return true;
        if(!ast || !pattern)
            return false;
        return ast->match0(pattern, matcher);
    }
    
    EmptyStatementAST::~EmptyStatementAST()
    {
    }

    StatementAST * EmptyStatementAST::clone(MemoryPool * pool) const
    {
        EmptyStatementAST * ast = new(pool) EmptyStatementAST();
        ast->semicolon_token = semicolon_token;
        return ast;
    }
    
    void EmptyStatementAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
        }
        visitor->endVisit(this);
    }
    
    bool EmptyStatementAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(EmptyStatementAST * _other = pattern->asEmptyStatement())
            return matcher->match(this, _other);
        return false;
    }
    
    UnaryExpressionAST::~UnaryExpressionAST()
    {
    }
    
    ExpressionAST * UnaryExpressionAST::clone(MemoryPool * pool) const
    {
        UnaryExpressionAST * ast = new(pool) UnaryExpressionAST();
        ast->unop_token = unop_token;
        ast->subexpr = subexpr ? subexpr->clone(pool) : NULL;
        ast->op = op;
        return ast;
    }
    
    void UnaryExpressionAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(subexpr, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool UnaryExpressionAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(UnaryExpressionAST * _other = pattern->asUnaryExpression())
            return matcher->match(this, _other);
        return false;
    }

    BinaryExpressionAST::~BinaryExpressionAST()
    {
    }
    
    ExpressionAST * BinaryExpressionAST::clone(MemoryPool * pool) const
    {
        BinaryExpressionAST * ast = new(pool) BinaryExpressionAST();
        ast->lhs = lhs ? lhs->clone(pool) : NULL;
        ast->binop_token = binop_token;
        ast->rhs = rhs ? rhs->clone(pool) : NULL;
        ast->op = op;
        return ast;
    }
    
    void BinaryExpressionAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(lhs, visitor);
            accept(rhs, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool BinaryExpressionAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(BinaryExpressionAST * _other = pattern->asBinaryExpression())
            return matcher->match(this, _other);
        return false;
    }

    NumberAST::~NumberAST()
    {
    }
    
    ExpressionAST * NumberAST::clone(MemoryPool * pool) const
    {
        NumberAST * ast = new(pool) NumberAST();
        ast->number_token = number_token;
        ast->value = value;
        return ast;
    }
    
    void NumberAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
        }
        visitor->endVisit(this);
    }
    
    bool NumberAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(NumberAST * _other = pattern->asNumber())
            return matcher->match(this, _other);
        return false;
    }
    
    StringAST::StringAST()
    {
    }

    StringAST::~StringAST()
    {
    }
    
    ExpressionAST * StringAST::clone(MemoryPool * pool) const
    {
        StringAST * ast = new(pool) StringAST();
        ast->lquot_token = lquot_token;
        ast->rquot_token = rquot_token;
        ast->value = value;
        ast->full_string_token = full_string_token;
        return ast;
    }
    
    void StringAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
        }
        visitor->endVisit(this);
    }
    
    bool StringAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(StringAST * _other = pattern->asString())
            return matcher->match(this, _other);
        return false;
    }
    
    NilAST::~NilAST()
    {
    }
    
    ExpressionAST * NilAST::clone(MemoryPool * pool) const
    {
        NilAST * ast = new(pool) NilAST();
        ast->nil_token = nil_token;
        return ast;
    }
    
    void NilAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
        }
        visitor->endVisit(this);
    }
    
    bool NilAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(NilAST * _other = pattern->asNil())
            return matcher->match(this, _other);
        return false;
    }
    
    BooleanAST::~BooleanAST()
    {
    }
    
    ExpressionAST * BooleanAST::clone(MemoryPool * pool) const
    {
        BooleanAST * ast = new(pool) BooleanAST();
        ast->bool_token = bool_token;
        ast->value = value;
        return ast;
    }
    
    void BooleanAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
        }
        visitor->endVisit(this);
    }
    
    bool BooleanAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(BooleanAST * _other = pattern->asBoolean())
            return matcher->match(this, _other);
        return false;
    }
    
    DotsAST::~DotsAST()
    {
    }
    
    ExpressionAST * DotsAST::clone(MemoryPool * pool) const
    {
        DotsAST * ast = new(pool) DotsAST();
        ast->dots_token = dots_token;
        return ast;
    }
    
    void DotsAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
        }
        visitor->endVisit(this);
    }
    
    bool DotsAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(DotsAST * _other = pattern->asDots())
            return matcher->match(this, _other);
        return false;
    }
    
    IfStatementAST::~IfStatementAST()
    {
    }
    
    StatementAST * IfStatementAST::clone(MemoryPool * pool) const
    {
        IfStatementAST * ast = new(pool) IfStatementAST();
        ast->if_token = if_token;
        ast->cond = cond ? cond->clone(pool) : NULL;
        ast->then_token = then_token;
        ast->block = block ? block->clone(pool) : NULL;
        ast->elseif_list = elseif_list ? elseif_list->clone(pool) : NULL;
        ast->else_token = else_token;
        ast->else_block = else_block ? else_block->clone(pool) : NULL;
        ast->end_token = end_token;
        return ast;
    }
    
    void IfStatementAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(cond, visitor);
            accept(block, visitor);
            accept(elseif_list, visitor);
            accept(else_block, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool IfStatementAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(IfStatementAST * _other = pattern->asIf())
            return matcher->match(this, _other);
        return false;
    }
    
    BlockAST::~BlockAST()
    {
    }
    
    BlockAST * BlockAST::clone(MemoryPool * pool) const
    {
        BlockAST * ast = new(pool) BlockAST();
        ast->list = list ? list->clone(pool) : NULL;
        return ast;
    }

    void BlockAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(list, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool BlockAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(BlockAST * _other = pattern->asBlock())
            return matcher->match(this, _other);
        return false;
    }
 
    WhileStatementAST::~WhileStatementAST()
    {
    }
    
    StatementAST * WhileStatementAST::clone(MemoryPool * pool) const
    {
        WhileStatementAST * ast = new(pool) WhileStatementAST();
        ast->while_token = while_token;
        ast->cond = cond ? cond->clone(pool) : NULL;
        ast->do_token = do_token;
        ast->block = block ? block->clone(pool) : NULL;
        ast->end_token = end_token;
        return ast;
    }
    
    void WhileStatementAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(cond, visitor);
            accept(block, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool WhileStatementAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(WhileStatementAST * _other = pattern->asWhile())
            return matcher->match(this, _other);
        return false;
    }
    
    DoStatementAST::~DoStatementAST()
    {
    }
    
    StatementAST * DoStatementAST::clone(MemoryPool * pool) const
    {
        DoStatementAST * ast = new(pool) DoStatementAST();
        ast->do_token = do_token;
        ast->block = block ? block->clone(pool) : NULL;
        ast->end_token = end_token;
        return ast;
    }
    
    void DoStatementAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(block, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool DoStatementAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(DoStatementAST * _other = pattern->asDo())
            return matcher->match(this, _other);
        return false;
    }
    
    RepeatStatementAST::~RepeatStatementAST()
    {
    }
    
    StatementAST * RepeatStatementAST::clone(MemoryPool * pool) const
    {
        RepeatStatementAST * ast = new(pool) RepeatStatementAST();
        ast->repeat_token = repeat_token;
        ast->block = block ? block->clone(pool) : NULL;
        ast->until_token = until_token;
        ast->cond = cond ? cond->clone(pool) : NULL;
        return ast;
    }
    
    void RepeatStatementAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(block, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool RepeatStatementAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(RepeatStatementAST * _other = pattern->asRepeat())
            return matcher->match(this, _other);
        return false;
    }
    
    IdentifierAST::~IdentifierAST()
    {
    }
    
    IdentifierAST * IdentifierAST::clone(MemoryPool * pool) const
    {
        IdentifierAST * ast = new(pool) IdentifierAST();
        ast->identifier_token = identifier_token;
        return ast;
    }
    
    void IdentifierAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
        }
        visitor->endVisit(this);
    }
    
    bool IdentifierAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(IdentifierAST * _other = pattern->asIdentifier())
            return matcher->match(this, _other);
        return false;
    }
    
    ComaSeparatedIdentifierAST::~ComaSeparatedIdentifierAST()
    {
    }
    
    AST * ComaSeparatedIdentifierAST::clone(MemoryPool * pool) const
    {
        ComaSeparatedIdentifierAST * ast = new(pool) ComaSeparatedIdentifierAST();
        ast->identifier = identifier->clone(pool);
        ast->coma_token = coma_token;
        return ast;
    }
    
    void ComaSeparatedIdentifierAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(identifier, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool ComaSeparatedIdentifierAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(ComaSeparatedIdentifierAST * _other = pattern->asComaSeparatedIdentifier())
            return matcher->match(this, _other);
        return false;
    }
    
    ComaSeparatedExpressionAST::~ComaSeparatedExpressionAST()
    {
    }
    
    AST * ComaSeparatedExpressionAST::clone(MemoryPool * pool) const
    {
        ComaSeparatedExpressionAST * ast = new(pool) ComaSeparatedExpressionAST();
        ast->expr = expr->clone(pool);
        ast->coma_token = coma_token;
        return ast;
    }
    
    void ComaSeparatedExpressionAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(expr, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool ComaSeparatedExpressionAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(ComaSeparatedExpressionAST * _other = pattern->asComaSeparatedExpression())
            return matcher->match(this, _other);
        return false;
    }

    ForNumStatementAST::~ForNumStatementAST()
    {
    }
    
    StatementAST * ForNumStatementAST::clone(MemoryPool * pool) const
    {
        ForNumStatementAST * ast = new(pool) ForNumStatementAST();
        ast->for_token = for_token;
        ast->name = name ? name->clone(pool) : NULL;
        ast->equal_token = equal_token;
        ast->index = index ? index->clone(pool) : NULL;
        ast->coma1 = coma1;
        ast->limit = limit ? limit->clone(pool) : NULL;
        ast->coma2 = coma2;
        ast->step = step ? step->clone(pool) : NULL;
        ast->do_token = do_token;
        ast->block = block ? block->clone(NULL) : NULL;
        ast->end_token = end_token;
        return ast;
    }
    
    void ForNumStatementAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(name, visitor);
            accept(index, visitor);
            accept(limit, visitor);
            accept(step, visitor);
            accept(block, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool ForNumStatementAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(ForNumStatementAST * _other = pattern->asForNum())
            return matcher->match(this, _other);
        return false;
    }
    
    ForListStatementAST::~ForListStatementAST()
    {
    }
    
    StatementAST * ForListStatementAST::clone(MemoryPool * pool) const
    {
        ForListStatementAST * ast = new(pool) ForListStatementAST();
        ast->for_token = for_token;
        ast->forlist = forlist ? forlist->clone(pool) : NULL;
        ast->in_token = in_token;
        ast->explist = explist ? explist->clone(pool) : NULL;
        ast->do_token = do_token;
        ast->block = block ? block->clone(NULL) : NULL;
        ast->end_token = end_token;
        return ast;
    }
    
    void ForListStatementAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(forlist, visitor);
            accept(explist, visitor);
            accept(block, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool ForListStatementAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(ForListStatementAST * _other = pattern->asForList())
            return matcher->match(this, _other);
        return false;
    }
    
    FieldSelectorAST::~FieldSelectorAST()
    {
    }
    
    SelectorAST * FieldSelectorAST::clone(MemoryPool * pool) const
    {
        FieldSelectorAST * ast = new(pool) FieldSelectorAST();
        ast->selector_token = selector_token;
        ast->name = name ? name->clone(pool) : NULL;
        return ast;
    }
    
    void FieldSelectorAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(name, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool FieldSelectorAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(FieldSelectorAST * _other = pattern->asFieldSelector())
            return matcher->match(this, _other);
        return false;
    }
    
    FunctionNameAST::~FunctionNameAST()
    {
    }
    
    AST * FunctionNameAST::clone(MemoryPool * pool) const
    {
        FunctionNameAST * ast = new(pool) FunctionNameAST();
        ast->name0 = name0 ? name0->clone(pool) : NULL;
        ast->fieldsel = fieldsel ? fieldsel->clone(pool) : NULL;
        ast->method_token = method_token;
        ast->method_name = method_name ? method_name->clone(pool) : NULL;
        return ast;
    }
    
    void FunctionNameAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(name0, visitor);
            accept(fieldsel, visitor);
            accept(method_name, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool FunctionNameAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(FunctionNameAST * _other = pattern->asFunctionName())
            return matcher->match(this, _other);
        return false;
    }
    
    FunctionBodyAST::~FunctionBodyAST()
    {
    }
    
    AST * FunctionBodyAST::clone(MemoryPool * pool) const
    {
        FunctionBodyAST * ast = new(pool) FunctionBodyAST();
        ast->lparen_token = lparen_token;
        ast->parameters = parameters ? parameters->clone(pool) : NULL;
        ast->rparen_token = rparen_token;
        ast->block = block ? block->clone(pool) : NULL;
        ast->end_token = end_token;
        return ast;
    }
    
    void FunctionBodyAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(parameters, visitor);
            accept(block, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool FunctionBodyAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(FunctionBodyAST * _other = pattern->asFunctionBody())
            return matcher->match(this, _other);
        return false;
    }
    
    FunctionStatementAST::~FunctionStatementAST()
    {
    }
    
    StatementAST * FunctionStatementAST::clone(MemoryPool * pool) const
    {
        FunctionStatementAST * ast = new(pool) FunctionStatementAST();
        ast->function_token = function_token;
        ast->name = name ? static_cast<FunctionNameAST *>(name->clone(pool)) : NULL;
        ast->body = body ? static_cast<FunctionBodyAST *>(body->clone(pool)) : NULL;
        return ast;
    }
    
    void FunctionStatementAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(name, visitor);
            accept(body, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool FunctionStatementAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(FunctionStatementAST * _other = pattern->asFunction())
            return matcher->match(this, _other);
        return false;
    }
    
    LocalFunctionStatementAST::~LocalFunctionStatementAST()
    {
    }
    
    StatementAST * LocalFunctionStatementAST::clone(MemoryPool * pool) const
    {
        LocalFunctionStatementAST * ast = new(pool) LocalFunctionStatementAST();
        ast->local_token = local_token;
        ast->function_token = function_token;
        ast->name = name ? name->clone(pool) : NULL;
        ast->body = body ? static_cast<FunctionBodyAST *>(body->clone(pool)) : NULL;
        return ast;
    }
    
    void LocalFunctionStatementAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(name, visitor);
            accept(body, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool LocalFunctionStatementAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(LocalFunctionStatementAST * _other = pattern->asLocalFunction())
            return matcher->match(this, _other);
        return false;
    }
    
    LocalStatementAST::~LocalStatementAST()
    {
    }
    
    StatementAST * LocalStatementAST::clone(MemoryPool * pool) const
    {
        LocalStatementAST * ast = new(pool) LocalStatementAST();
        ast->local_token = local_token;
        ast->names = names ? names->clone(pool) : NULL;
        ast->equal_token = equal_token;
        ast->explist = explist ? explist->clone(pool) : NULL;
        return ast;
    }
    
    void LocalStatementAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(names, visitor);
            accept(explist, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool LocalStatementAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(LocalStatementAST * _other = pattern->asLocal())
            return matcher->match(this, _other);
        return false;
    }
    
    LabelStatementAST::~LabelStatementAST()
    {
    }
    
    StatementAST * LabelStatementAST::clone(MemoryPool * pool) const
    {
        LabelStatementAST * ast = new(pool) LabelStatementAST();
        ast->ldb_token = ldb_token;
        ast->name = name ? name->clone(pool) : NULL;
        ast->rdb_token = rdb_token;
        return ast;
    }
    
    void LabelStatementAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(name, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool LabelStatementAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(LabelStatementAST * _other = pattern->asLabel())
            return matcher->match(this, _other);
        return false;
    }
    
    ReturnStatementAST::~ReturnStatementAST()
    {
    }
    
    StatementAST * ReturnStatementAST::clone(MemoryPool * pool) const
    {
        ReturnStatementAST * ast = new(pool) ReturnStatementAST();
        ast->return_token = return_token;
        ast->explist = explist ? explist->clone(pool) : NULL;
        ast->semicolon_token = semicolon_token;
        return ast;
    }
    
    void ReturnStatementAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(explist, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool ReturnStatementAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(ReturnStatementAST * _other = pattern->asReturn())
            return matcher->match(this, _other);
        return false;
    }
    
    BreakStatementAST::~BreakStatementAST()
    {
    }
    
    StatementAST * BreakStatementAST::clone(MemoryPool * pool) const
    {
        BreakStatementAST * ast = new(pool) BreakStatementAST();
        ast->break_token = break_token;
        return ast;
    }
    
    void BreakStatementAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
        }
        visitor->endVisit(this);
    }
    
    bool BreakStatementAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(BreakStatementAST * _other = pattern->asBreak())
            return matcher->match(this, _other);
        return false;
    }
    
    GotoStatementAST::~GotoStatementAST()
    {
    }
    
    StatementAST * GotoStatementAST::clone(MemoryPool * pool) const
    {
        GotoStatementAST * ast = new(pool) GotoStatementAST();
        ast->goto_token = goto_token;
        ast->label = label ? label->clone(pool) : NULL;
        return ast;
    }
    
    void GotoStatementAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(label, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool GotoStatementAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(GotoStatementAST * _other = pattern->asGoto())
            return matcher->match(this, _other);
        return false;
    }
    
    ExpressionStatementAST::~ExpressionStatementAST()
    {
    }
    
    StatementAST * ExpressionStatementAST::clone(MemoryPool * pool) const
    {
        ExpressionStatementAST * ast = new(pool) ExpressionStatementAST();
        ast->expr = expr->clone(pool);
        return ast;
    }
    
    void ExpressionStatementAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(expr, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool ExpressionStatementAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(ExpressionStatementAST * _other = pattern->asExpressionStatement())
            return matcher->match(this, _other);
        return false;
    }
    
    BracketExpressionAST::~BracketExpressionAST()
    {
    }
    
    ExpressionAST * BracketExpressionAST::clone(MemoryPool * pool) const
    {
        BracketExpressionAST * ast = new(pool) BracketExpressionAST();
        ast->lbracket_token = lbracket_token;
        ast->expr = expr ? expr->clone(pool) : NULL;
        ast->rbracket_token = rbracket_token;
        return ast;
    }
    
    void BracketExpressionAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(expr, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool BracketExpressionAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(BracketExpressionAST * _other = pattern->asBracketExpression())
            return matcher->match(this, _other);
        return false;
    }
    
    IndexSelectorAST::~IndexSelectorAST()
    {
    }
    
    SelectorAST * IndexSelectorAST::clone(MemoryPool * pool) const
    {
        IndexSelectorAST * ast = new(pool) IndexSelectorAST();
        ast->lsquare_token = lsquare_token;
        ast->expr = expr ? expr->clone(pool) : NULL;
        ast->rsquare_token = rsquare_token;
        return NULL;
    }
    
    void IndexSelectorAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(expr, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool IndexSelectorAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(IndexSelectorAST * _other = pattern->asIndexSelector())
            return matcher->match(this, _other);
        return false;
    }
    
    MethodCallSelectorAST::~MethodCallSelectorAST()
    {
    }
    
    SelectorAST * MethodCallSelectorAST::clone(MemoryPool * pool) const
    {
        MethodCallSelectorAST * ast = new(pool) MethodCallSelectorAST();
        ast->colon_token = colon_token;
        ast->method = method ? method->clone(pool) : NULL;
        ast->arguments = arguments ? arguments->clone(pool) : NULL;
        return NULL;
    }
    
    void MethodCallSelectorAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(method, visitor);
            accept(arguments, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool MethodCallSelectorAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(MethodCallSelectorAST * _other = pattern->asMethodCallSelector())
            return matcher->match(this, _other);
        return false;
    }
    
    FunctionCallSelectorAST::~FunctionCallSelectorAST()
    {
    }
    
    SelectorAST * FunctionCallSelectorAST::clone(MemoryPool * pool) const
    {
        FunctionCallSelectorAST * ast = new(pool) FunctionCallSelectorAST();
        ast->arguments = arguments ? arguments->clone(pool) : NULL;
        return NULL;
    }
    
    void FunctionCallSelectorAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(arguments, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool FunctionCallSelectorAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(FunctionCallSelectorAST * _other = pattern->asFunctionCallSelector())
            return matcher->match(this, _other);
        return false;
    }
    
    RecordFieldAST::~RecordFieldAST()
    {
    }
    
    FieldAST * RecordFieldAST::clone(MemoryPool * pool) const
    {
        RecordFieldAST * ast = new(pool) RecordFieldAST();
        ast->selector = selector ? selector->clone(pool) : NULL;
        ast->equal_token = equal_token;
        ast->value = value ? value->clone(pool) : NULL;
        ast->separator_token = separator_token;
        return ast;
    }
        
    void RecordFieldAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(selector, visitor);
            accept(value, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool RecordFieldAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(RecordFieldAST * _other = pattern->asRecordField())
            return matcher->match(this, _other);
        return false;
    }
    
    ListFieldAST::~ListFieldAST()
    {
    }
    
    FieldAST * ListFieldAST::clone(MemoryPool * pool) const
    {
        ListFieldAST * ast = new(pool) ListFieldAST();
        ast->expr = expr ? expr->clone(pool) : NULL;
        ast->separator_token = separator_token;
        return ast;
    }
    
    void ListFieldAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(expr, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool ListFieldAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(ListFieldAST * _other = pattern->asListField())
            return matcher->match(this, _other);
        return false;
    }

    ConstructorAST::~ConstructorAST()
    {
    }
    
    ExpressionAST * ConstructorAST::clone(MemoryPool * pool) const
    {
        ConstructorAST * ast = new(pool) ConstructorAST();
        ast->lcurl_token = lcurl_token;
        ast->fields = fields ? fields->clone(pool) : NULL;
        ast->rcurl_token = rcurl_token;
        return ast;
    }
    
    void ConstructorAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(fields, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool ConstructorAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(ConstructorAST * _other = pattern->asConstructor())
            return matcher->match(this, _other);
        return false;
    }
    
    FunctionExpressionAST::~FunctionExpressionAST()
    {
    }
    
    ExpressionAST * FunctionExpressionAST::clone(MemoryPool * pool) const
    {
        FunctionExpressionAST * ast = new(pool) FunctionExpressionAST();
        ast->function_token = function_token;
        ast->body = body ? static_cast<FunctionBodyAST *>(body->clone(pool)) : NULL;
        return ast;
    }
    
    void FunctionExpressionAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(body, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool FunctionExpressionAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(FunctionExpressionAST * _other = pattern->asFunctionExpression())
            return matcher->match(this, _other);
        return false;
    }
    
    NameSelectorAST::~NameSelectorAST()
    {
    }
    
    SelectorAST * NameSelectorAST::clone(MemoryPool * pool) const
    {
        NameSelectorAST * ast = new(pool) NameSelectorAST();
        ast->name = name ? name->clone(pool) : NULL;
        return ast;
    }
    
    void NameSelectorAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(name, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool NameSelectorAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(NameSelectorAST * _other = pattern->asNameSelector())
            return matcher->match(this, _other);
        return false;
    }
    
    SelectorExpressionAST::~SelectorExpressionAST()
    {
    }
    
    ExpressionAST * SelectorExpressionAST::clone(MemoryPool * pool) const
    {
        SelectorExpressionAST * ast = new(pool) SelectorExpressionAST();
        ast->prefixexp = prefixexp->clone(pool);
        ast->chain = chain ? chain->clone(pool) : NULL;
        return ast;
    }
    
    void SelectorExpressionAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(prefixexp, visitor);
            accept(chain, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool SelectorExpressionAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(SelectorExpressionAST * _other = pattern->asSelectorExpression())
            return matcher->match(this, _other);
        return false;
    }
    
    FunctionArgumentsExpressionAST::~FunctionArgumentsExpressionAST()
    {
    }
    
    ExpressionAST * FunctionArgumentsExpressionAST::clone(MemoryPool * pool) const
    {
        FunctionArgumentsExpressionAST * ast = new(pool) FunctionArgumentsExpressionAST();
        ast->lbracket = lbracket;
        ast->args = args ? args->clone(pool) : NULL;
        ast->rbracket = rbracket;
        return ast;
    }
    
    void FunctionArgumentsExpressionAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(args, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool FunctionArgumentsExpressionAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(FunctionArgumentsExpressionAST * _other = pattern->asFunctionArgumentsExpression())
            return matcher->match(this, _other);
        return false;
    }
    
    AssignmentExpressionAST::~AssignmentExpressionAST()
    {
    }
    
    ExpressionAST * AssignmentExpressionAST::clone(MemoryPool * pool) const
    {
        AssignmentExpressionAST * ast = new(pool) AssignmentExpressionAST();
        ast->lhs = lhs ? static_cast<ComaSeparatedExpressionListAST *>(lhs->clone(pool)) : NULL;
        ast->equal_token = equal_token;
        ast->rhs = rhs ? static_cast<ComaSeparatedExpressionListAST *>(rhs->clone(pool)) : NULL;
        return ast;
    }
    
    void AssignmentExpressionAST::accept0(ASTVisitor * visitor)
    {
        if(visitor->visit(this)) {
            accept(lhs, visitor);
            accept(rhs, visitor);
        }
        visitor->endVisit(this);
    }
    
    bool AssignmentExpressionAST::match0(AST * pattern, ASTMatcher * matcher)
    {
        if(AssignmentExpressionAST * _other = pattern->asAssignmentExpression())
            return matcher->match(this, _other);
        return false;
    }
    
}

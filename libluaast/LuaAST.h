// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


#ifndef __libluaast__LuaAST__
#define __libluaast__LuaAST__

#include "LuaASTFwd.h"
#include "LuaASTManaged.h"
#ifndef LUAAST_QT
#include "LuaStringRef.h"
#endif

namespace LuaAST {
    
    template <typename Tptr>
    class List: public Managed
    {
        List(const List &other);
        void operator =(const List &other);
        
    public:
        List()
        : value(Tptr()), next(0)
        { }
        
        List(const Tptr &value)
        : value(value), next(0)
        { }
        
        SourceLocation firstSourceLocation() const
        {
            if (value)
                return value->firstSourceLocation();
            
            return SourceLocation();
        }
        
        SourceLocation lastSourceLocation() const
        {
            Tptr lv = lastValue();
            
            if (lv)
                return lv->lastSourceLocation();
            
            return SourceLocation();
        }
        
        Tptr lastValue() const
        {
            Tptr lastValue = 0;
            
            for (const List *it = this; it; it = it->next) {
                if (it->value)
                    lastValue = it->value;
            }
            
            return lastValue;
        }
        
        Tptr nthValue(size_t index) const
        {
            Tptr lastValue = 0;
            for (const List *it = this; it; it = it->next) {
                if(it->value) {
                    lastValue = it->value;
                    if(index == 0)
                        break;
                    --index;
                }
            }
            return lastValue;
        }
        
        List<Tptr> * clone(MemoryPool * pool) const
        {
            List<Tptr> * res = NULL;
            const List<Tptr> * iter = this;
            List<Tptr> ** ast_iter = &res;
            
            for( ; iter ; iter = iter->next, ast_iter = &(*ast_iter)->next)
            {
                *ast_iter = new(pool) List<Tptr>(iter->value ? static_cast<Tptr>(iter->value->clone(pool)) : NULL);
            }
            return res;
        }
        
        Tptr value;
        List *next;
    };

    class AST : public Managed
    {
    public:
        AST();
        virtual ~AST();
        
        void accept(ASTVisitor * visitor);
        
        static void accept(AST * ast, ASTVisitor * visitor) {
            if(ast) ast->accept(visitor);
        }
        
        template <typename Tptr>
        static void accept(List<Tptr> *it, ASTVisitor *visitor)
        {
            for (; it; it = it->next)
                accept(it->value, visitor);
        }
        
        bool match(AST * pattern, ASTMatcher * matcher);
        static bool match(AST * ast, AST * pattern, ASTMatcher * matcher);
        
        template <typename Tptr>
        static bool match(List<Tptr> *it, List<Tptr> *patternIt, ASTMatcher *matcher)
        {
            while(it && patternIt) {
                if(!match(it->value, patternIt->value, matcher))
                    return false;
                it = it->next;
                patternIt = patternIt->next;
            }
            if(!it && !patternIt)
                return false;
            return true;
        }
        
        virtual SourceLocation firstSourceLocation() const = 0;
        virtual SourceLocation lastSourceLocation() const = 0;
        
        virtual AST * clone(MemoryPool *) const = 0;
        
        virtual StatementAST * asStatement() { return NULL; }
        virtual EmptyStatementAST * asEmptyStatement() { return NULL; }
        virtual ExpressionAST * asExpression() { return NULL; }
        virtual UnaryExpressionAST * asUnaryExpression() { return NULL; }
        virtual BinaryExpressionAST * asBinaryExpression() { return NULL; }
        virtual NumberAST * asNumber() { return NULL; }
        virtual StringAST * asString() { return NULL; }
        virtual NilAST * asNil() { return NULL; }
        virtual BooleanAST * asBoolean() { return NULL; }
        virtual DotsAST * asDots() { return NULL; }
        virtual IfStatementAST * asIf() { return NULL; }
        virtual BlockAST * asBlock() { return NULL; }
        virtual WhileStatementAST * asWhile() { return NULL; }
        virtual DoStatementAST * asDo() { return NULL; }
        virtual ForNumStatementAST * asForNum() { return NULL; }
        virtual ForListStatementAST * asForList() { return NULL; }
        virtual RepeatStatementAST * asRepeat() { return NULL; }
        virtual FunctionStatementAST * asFunction() { return NULL; }
        virtual LocalStatementAST * asLocal() { return NULL; }
        virtual LabelStatementAST * asLabel() { return NULL; }
        virtual ReturnStatementAST * asReturn() { return NULL; }
        virtual BreakStatementAST * asBreak() { return NULL; }
        virtual GotoStatementAST * asGoto() { return NULL; }
        virtual ExpressionStatementAST * asExpressionStatement() { return NULL; }
        virtual IdentifierAST * asIdentifier() { return NULL; }
        virtual ComaSeparatedIdentifierAST * asComaSeparatedIdentifier() { return NULL; }
        virtual ComaSeparatedExpressionAST * asComaSeparatedExpression() { return NULL; }
        virtual FieldSelectorAST * asFieldSelector() { return NULL; }
        virtual FunctionNameAST * asFunctionName() { return NULL; }
        virtual FunctionBodyAST * asFunctionBody() { return NULL; }
        virtual LocalFunctionStatementAST * asLocalFunction() { return NULL; }
        virtual BracketExpressionAST * asBracketExpression() { return NULL; }
        virtual SelectorAST * asSelector() { return NULL; }
        virtual IndexSelectorAST * asIndexSelector() { return NULL; }
        virtual MethodCallSelectorAST * asMethodCallSelector() { return NULL; }
        virtual FunctionCallSelectorAST * asFunctionCallSelector() { return NULL; }
        virtual ConstructorAST * asConstructor() { return NULL; }
        virtual FieldAST * asField() { return NULL; }
        virtual RecordFieldAST * asRecordField() { return NULL; }
        virtual ListFieldAST * asListField() { return NULL; }
        virtual FunctionExpressionAST * asFunctionExpression() { return NULL; }
        virtual NameSelectorAST * asNameSelector() { return NULL; }
        virtual SelectorExpressionAST * asSelectorExpression() { return NULL; }
        virtual FunctionArgumentsExpressionAST * asFunctionArgumentsExpression() { return NULL; }
        virtual AssignmentExpressionAST * asAssignmentExpression() { return NULL; }

    protected:
        virtual void accept0(ASTVisitor *) = 0;
        virtual bool match0(AST *, ASTMatcher *) = 0;
    };
    
    class StatementAST : public AST
    {
    public:
        StatementAST() { }
        
        virtual StatementAST * asStatement() { return NULL; }
        virtual StatementAST * clone(MemoryPool *) const = 0;
    };
    
    class EmptyStatementAST : public StatementAST
    {
    public:
        SourceLocation semicolon_token;
        
        EmptyStatementAST() { }
        virtual ~EmptyStatementAST();
        
        virtual SourceLocation firstSourceLocation() const { return semicolon_token; }
        virtual SourceLocation lastSourceLocation() const { return semicolon_token; }
        virtual EmptyStatementAST * asEmptyStatement() { return this; }
        
        virtual StatementAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
    };
    
    class ExpressionAST : public AST
    {
    public:
        ExpressionAST() { }
        
        virtual ExpressionAST * asExpression() { return this; }
        virtual ExpressionAST * clone(MemoryPool *) const = 0;
    };
    
    class SelectorAST : public AST
    {
    public:
        SelectorAST() { }
        
        virtual SelectorAST * asSelector() { return this; }
        virtual SelectorAST * clone(MemoryPool *) const = 0;
    };
    
    class FieldAST : public AST
    {
    public:
        FieldAST() { }
        
        virtual FieldAST * asField() { return this; }
        virtual FieldAST * clone(MemoryPool *) const = 0;
    };
    
    class UnaryExpressionAST : public ExpressionAST
    {
    public:
        enum UnOpr { OPR_MINUS, OPR_NOT, OPR_LEN, OPR_NOUNOPR };

        SourceLocation unop_token;
        ExpressionAST * subexpr;
        UnOpr op;
        
        UnaryExpressionAST() : subexpr(NULL), op(OPR_NOUNOPR) { }
        virtual ~UnaryExpressionAST();
        
        virtual SourceLocation firstSourceLocation() const { return unop_token; }
        virtual SourceLocation lastSourceLocation() const { return subexpr ? subexpr->lastSourceLocation() : unop_token; }
        virtual UnaryExpressionAST * asUnaryExpression() { return this; }

        virtual ExpressionAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
   };
    
    class BinaryExpressionAST : public ExpressionAST
    {
    public:
        /*
         ** grep "ORDER OPR" if you change these enums  (ORDER OP)
         */
        enum BinOpr {
            OPR_ADD, OPR_SUB, OPR_MUL, OPR_DIV, OPR_MOD, OPR_POW,
            OPR_CONCAT,
            OPR_EQ, OPR_LT, OPR_LE,
            OPR_NE, OPR_GT, OPR_GE,
            OPR_AND, OPR_OR,
            OPR_NOBINOPR
        };
        
        ExpressionAST * lhs;
        SourceLocation binop_token;
        ExpressionAST * rhs;
        BinOpr op;
        
        BinaryExpressionAST() : lhs(NULL), rhs(NULL), op(OPR_NOBINOPR) { }
        virtual ~BinaryExpressionAST();
        
        virtual SourceLocation firstSourceLocation() const { return lhs ? lhs->firstSourceLocation() : binop_token; }
        virtual SourceLocation lastSourceLocation() const { return rhs ? rhs->lastSourceLocation() : binop_token; }
        virtual BinaryExpressionAST * asBinaryExpression() { return this; }
        
        virtual ExpressionAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
   };
    
    class NumberAST : public ExpressionAST
    {
    public:
        SourceLocation number_token;
        lua_Number value;
        
        NumberAST(){ }
        virtual ~NumberAST();
        
        virtual SourceLocation firstSourceLocation() const { return number_token; }
        virtual SourceLocation lastSourceLocation() const { return number_token; }
        virtual NumberAST * asNumber() { return this; }
        
        virtual ExpressionAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
    };
    
    class StringAST : public ExpressionAST
    {
    public:
        SourceLocation lquot_token;
        SourceLocation rquot_token;
        StringRef value;
        SourceLocation full_string_token;
        
        StringAST();
        virtual ~StringAST();
        
        virtual SourceLocation firstSourceLocation() const { return lquot_token; }
        virtual SourceLocation lastSourceLocation() const { return rquot_token; }
        virtual StringAST * asString() { return this; }
        
        virtual ExpressionAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
        
    private:
        StringAST(const StringAST&);
        StringAST& operator = (const StringAST&);
    };
    
    class NilAST : public ExpressionAST
    {
    public:
        SourceLocation nil_token;
        
        NilAST() { }
        virtual ~NilAST();
        
        virtual SourceLocation firstSourceLocation() const { return nil_token; }
        virtual SourceLocation lastSourceLocation() const { return nil_token; }
        virtual NilAST * asNil() { return this; }
        
        virtual ExpressionAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
    };
    
    class BooleanAST : public ExpressionAST
    {
    public:
        SourceLocation bool_token;
        bool value;
        
        BooleanAST() : value(false) { }
        virtual ~BooleanAST();
        
        virtual SourceLocation firstSourceLocation() const { return bool_token; }
        virtual SourceLocation lastSourceLocation() const { return bool_token; }
        virtual BooleanAST * asBoolean() { return this; }

        virtual ExpressionAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
   };
    
    class DotsAST : public ExpressionAST
    {
    public:
        SourceLocation dots_token;
        
        DotsAST() { }
        virtual ~DotsAST();
        
        virtual SourceLocation firstSourceLocation() const { return dots_token; }
        virtual SourceLocation lastSourceLocation() const { return dots_token; }
        virtual DotsAST * asDots() { return this; }
        
        virtual ExpressionAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
    };
    
    class IfStatementAST : public StatementAST
    {
    public:
        SourceLocation if_token;
        ExpressionAST * cond;
        SourceLocation then_token;
        BlockAST * block;
        ElseIfListAST * elseif_list;
        SourceLocation else_token;
        BlockAST * else_block;
        SourceLocation end_token;
        
        IfStatementAST() : cond(NULL), block(NULL), elseif_list(NULL), else_block(NULL) { }
        virtual ~IfStatementAST();
        
        virtual SourceLocation firstSourceLocation() const { return if_token; }
        virtual SourceLocation lastSourceLocation() const { return end_token; }
        virtual IfStatementAST * asIf() { return this; }
      
        virtual StatementAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
    };
    
    class BlockAST : public AST
    {
    public:
        StatementListAST * list;
        
        BlockAST() : list(NULL) { }
        virtual ~BlockAST();
        
        virtual SourceLocation firstSourceLocation() const { return list ? list->firstSourceLocation() : SourceLocation(); }
        virtual SourceLocation lastSourceLocation() const { return list ? list->lastSourceLocation() : SourceLocation(); }
        virtual BlockAST * asBlock() { return this; }
        
        virtual BlockAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
    };
    
    class WhileStatementAST : public StatementAST
    {
    public:
        SourceLocation while_token;
        ExpressionAST * cond;
        SourceLocation do_token;
        BlockAST * block;
        SourceLocation end_token;
        
        WhileStatementAST() : cond(NULL), block(NULL) { }
        virtual ~WhileStatementAST();
        
        virtual SourceLocation firstSourceLocation() const { return while_token; }
        virtual SourceLocation lastSourceLocation() const { return end_token; }
        virtual WhileStatementAST * asWhile() { return this; }
        
        virtual StatementAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
    };
    
    class DoStatementAST : public StatementAST
    {
    public:
        SourceLocation do_token;
        BlockAST * block;
        SourceLocation end_token;
        
        DoStatementAST() : block(NULL) { }
        virtual ~DoStatementAST();
        
        virtual SourceLocation firstSourceLocation() const { return do_token; }
        virtual SourceLocation lastSourceLocation() const { return end_token; }
        virtual DoStatementAST * asDo() { return this; }
       
        virtual StatementAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
   };

    class RepeatStatementAST : public StatementAST
    {
    public:
        SourceLocation repeat_token;
        BlockAST * block;
        SourceLocation until_token;
        ExpressionAST * cond;
        
        RepeatStatementAST() : block(NULL), cond(NULL) { }
        virtual ~RepeatStatementAST();
        
        virtual SourceLocation firstSourceLocation() const { return repeat_token; }
        virtual SourceLocation lastSourceLocation() const { return cond ? cond->lastSourceLocation() : until_token; }
        virtual RepeatStatementAST * asRepeat() { return this; }
        
        virtual StatementAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
    };
    
    class IdentifierAST : public ExpressionAST
    {
    public:
        SourceLocation identifier_token;
        
        IdentifierAST() { }
        virtual ~IdentifierAST();
        
        virtual SourceLocation firstSourceLocation() const { return identifier_token; }
        virtual SourceLocation lastSourceLocation() const { return identifier_token; }
        virtual IdentifierAST * asIdentifier() { return this; }
        
        virtual IdentifierAST * clone(MemoryPool *) const;
    protected:
        virtual bool match0(AST *, ASTMatcher *);
        virtual void accept0(ASTVisitor *);
    };
    
    class ComaSeparatedIdentifierAST : public AST
    {
    public:
        IdentifierAST * identifier;
        SourceLocation coma_token;
        
        ComaSeparatedIdentifierAST() : identifier(NULL) { }
        virtual ~ComaSeparatedIdentifierAST();
        
        virtual SourceLocation firstSourceLocation() const { return identifier->firstSourceLocation(); }
        virtual SourceLocation lastSourceLocation() const { return coma_token.isValid() ? coma_token : identifier->lastSourceLocation(); }
        virtual ComaSeparatedIdentifierAST * asComaSeparatedIdentifier() { return this; }

        virtual AST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
    };
    
    class ComaSeparatedExpressionAST : public AST
    {
    public:
        ExpressionAST * expr;
        SourceLocation coma_token;
        
        ComaSeparatedExpressionAST() : expr(NULL) { }
        virtual ~ComaSeparatedExpressionAST();
        
        virtual SourceLocation firstSourceLocation() const { return expr->firstSourceLocation(); }
        virtual SourceLocation lastSourceLocation() const { return coma_token.isValid() ? coma_token : expr->lastSourceLocation(); }
        virtual ComaSeparatedExpressionAST * asComaSeparatedExpression() { return this; }
        
        virtual AST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
    };
    
    class ForNumStatementAST : public StatementAST
    {
    public:
        SourceLocation for_token;
        IdentifierAST * name;
        SourceLocation equal_token;
        ExpressionAST * index;
        SourceLocation coma1;
        ExpressionAST * limit;
        SourceLocation coma2;
        ExpressionAST * step;
        SourceLocation do_token;
        BlockAST * block;
        SourceLocation end_token;
        
        ForNumStatementAST() :
            name(NULL),
            index(NULL),
            limit(NULL),
            step(NULL),
            block(NULL)
        {
        }
        
        virtual ~ForNumStatementAST();
        
        virtual SourceLocation firstSourceLocation() const { return for_token; }
        virtual SourceLocation lastSourceLocation() const { return end_token; }
        virtual ForNumStatementAST * asForNum() { return this; }

        virtual StatementAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
    };
    
    class ForListStatementAST : public StatementAST
    {
    public:
        SourceLocation for_token;
        ComaSeparatedIdentifierListAST * forlist;
        SourceLocation in_token;
        ComaSeparatedExpressionListAST * explist;
        SourceLocation do_token;
        BlockAST * block;
        SourceLocation end_token;
        
        ForListStatementAST() :  forlist(NULL), explist(NULL), block(NULL) { }
        
        virtual ~ForListStatementAST();
        
        virtual SourceLocation firstSourceLocation() const { return for_token; }
        virtual SourceLocation lastSourceLocation() const { return end_token; }
        virtual ForListStatementAST * asForList() { return this; }
        
        virtual StatementAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
   };
    
    class FieldSelectorAST : public SelectorAST
    {
    public:
        SourceLocation selector_token;
        IdentifierAST * name;
        
        FieldSelectorAST() : name(NULL) { }
        virtual ~FieldSelectorAST();
        
        virtual SourceLocation firstSourceLocation() const { return selector_token; }
        virtual SourceLocation lastSourceLocation() const { return name ? name->lastSourceLocation() : selector_token; }
        virtual FieldSelectorAST * asFieldSelector() { return this; }
        
        virtual SelectorAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
   };
    
    class FunctionNameAST : public AST
    {
    public:
        IdentifierAST * name0;
        FieldSelectorListAST * fieldsel;
        SourceLocation method_token;
        IdentifierAST * method_name;
        
        FunctionNameAST() : name0(NULL), fieldsel(NULL), method_name(0) { }
        virtual ~FunctionNameAST();
        
        
        virtual SourceLocation firstSourceLocation() const { return name0->firstSourceLocation(); }
        
        virtual SourceLocation lastSourceLocation() const {
            if(method_name)
                return method_name->lastSourceLocation();
            if(fieldsel)
                return fieldsel->lastSourceLocation();
            return name0->lastSourceLocation();
        }
        
        IdentifierAST * base_name() const {
            if(method_name)
                return method_name;
            if(fieldsel)
                return fieldsel->lastValue()->name;
            return name0;
        }
        
        virtual FunctionNameAST * asFunctionName() { return NULL; }
        
        virtual AST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
  };
    
    class FunctionBodyAST : public AST
    {
    public:
        SourceLocation lparen_token;
        ComaSeparatedExpressionListAST * parameters;
        SourceLocation rparen_token;
        BlockAST * block;
        SourceLocation end_token;
        
        FunctionBodyAST() : parameters(NULL), block(NULL)
        {
        }
        
        virtual ~FunctionBodyAST();
        
        virtual SourceLocation firstSourceLocation() const { return lparen_token; }
        virtual SourceLocation lastSourceLocation() const { return end_token; }
        virtual FunctionBodyAST * asFunctionBody() { return this; }
        
        virtual AST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
    };
    
    class FunctionStatementAST : public StatementAST
    {
    public:
        SourceLocation function_token;
        FunctionNameAST * name;
        FunctionBodyAST * body;
        
        FunctionStatementAST() : name(NULL), body(NULL) { }
        virtual ~FunctionStatementAST();
        
        virtual SourceLocation firstSourceLocation() const { return function_token; }
        
        virtual SourceLocation lastSourceLocation() const {
            if(body)
                return body->lastSourceLocation();
            if(name)
                return name->lastSourceLocation();
            return function_token;
        }
        
        virtual FunctionStatementAST * asFunction() { return this; }
        
        virtual StatementAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
   };
    
    class LocalFunctionStatementAST : public StatementAST
    {
    public:
        SourceLocation local_token;
        SourceLocation function_token;
        IdentifierAST * name;
        FunctionBodyAST * body;
        
        LocalFunctionStatementAST() : name(NULL), body(NULL) { }
        virtual ~LocalFunctionStatementAST();
        
        virtual SourceLocation firstSourceLocation() const { return local_token; }
        virtual SourceLocation lastSourceLocation() const {
            if(body)
                return body->lastSourceLocation();
            if(name)
                return name->lastSourceLocation();
            return function_token;
        }
        
        virtual LocalFunctionStatementAST * asLocalFunction() { return this; }
        
        virtual StatementAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
   };
    
    class LocalStatementAST : public StatementAST
    {
    public:
        SourceLocation local_token;
        ComaSeparatedIdentifierListAST * names;
        SourceLocation equal_token;
        ComaSeparatedExpressionListAST * explist;
    
        LocalStatementAST() : names(NULL), explist(NULL) { }
        virtual ~LocalStatementAST();
        
        virtual SourceLocation firstSourceLocation() const { return local_token; }
        virtual SourceLocation lastSourceLocation() const {
            if(explist)
                return explist->lastSourceLocation();
            if(equal_token.isValid())
                return equal_token;
            if(names)
                return names->lastSourceLocation();
            return local_token;
        }
        
        virtual LocalStatementAST * asLocal() { return this; }

        virtual StatementAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
   };
    
    class LabelStatementAST : public StatementAST
    {
    public:
        SourceLocation ldb_token;
        IdentifierAST * name;
        SourceLocation rdb_token;
        
        LabelStatementAST() : name(NULL) { }
        virtual ~LabelStatementAST();
        
        virtual SourceLocation firstSourceLocation() const { return ldb_token; }
        virtual SourceLocation lastSourceLocation() const { return rdb_token; }
        
        virtual LabelStatementAST * asLabel() { return this; }

        virtual StatementAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
   };
    
    class ReturnStatementAST : public StatementAST
    {
    public:
        SourceLocation return_token;
        ComaSeparatedExpressionListAST * explist;
        SourceLocation semicolon_token;
        
        ReturnStatementAST() : explist(NULL) { }
        virtual ~ReturnStatementAST();
        
        virtual SourceLocation firstSourceLocation() const { return return_token; }
        virtual SourceLocation lastSourceLocation() const {
            if(semicolon_token.isValid())
                return semicolon_token;
            if(explist)
                return explist->lastSourceLocation();
            return return_token;
        }
        
        virtual ReturnStatementAST * asReturn() { return this; }
        
        virtual StatementAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
    };
    
    class BreakStatementAST : public StatementAST
    {
    public:
        SourceLocation break_token;
        
        BreakStatementAST() { }
        virtual ~BreakStatementAST();
        
        virtual SourceLocation firstSourceLocation() const { return break_token; }
        virtual SourceLocation lastSourceLocation() const { return break_token; }
        
        virtual BreakStatementAST * asBreak() { return this; }
        
        virtual StatementAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
   };
    
    class GotoStatementAST : public StatementAST
    {
    public:
        SourceLocation goto_token;
        IdentifierAST * label;
        
        GotoStatementAST() : label(NULL) { }
        virtual ~GotoStatementAST();
        
        virtual SourceLocation firstSourceLocation() const { return goto_token; }
        virtual SourceLocation lastSourceLocation() const { return label->lastSourceLocation(); }
        
        virtual GotoStatementAST * asGoto() { return this; }
        
        virtual StatementAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
  };
    
    class ExpressionStatementAST : public StatementAST
    {
    public:
        ExpressionAST * expr;
        
        ExpressionStatementAST() : expr(NULL) { }
        virtual ~ExpressionStatementAST();
        
        virtual SourceLocation firstSourceLocation() const { return expr->firstSourceLocation(); }
        virtual SourceLocation lastSourceLocation() const { return expr->lastSourceLocation(); }
        virtual ExpressionStatementAST * asExpressionStatement() { return this; }
        
        virtual StatementAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
    };
    
    class BracketExpressionAST : public ExpressionAST
    {
    public:
        SourceLocation lbracket_token;
        ExpressionAST * expr;
        SourceLocation rbracket_token;
        
        BracketExpressionAST() : expr(NULL) { }
        virtual ~BracketExpressionAST();
        
        virtual SourceLocation firstSourceLocation() const { return lbracket_token; }
        virtual SourceLocation lastSourceLocation() const { return rbracket_token; }
        virtual BracketExpressionAST * asBracketExpression() { return this; }
        
        virtual ExpressionAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
    };
    
    class IndexSelectorAST : public SelectorAST
    {
    public:
        SourceLocation lsquare_token;
        ExpressionAST * expr;
        SourceLocation rsquare_token;
        
        IndexSelectorAST() : expr(NULL) { }
        virtual ~IndexSelectorAST();
        
        virtual SourceLocation firstSourceLocation() const { return lsquare_token; }
        virtual SourceLocation lastSourceLocation() const { return rsquare_token; }
        virtual IndexSelectorAST * asIndexSelector() { return this; }
   
        virtual SelectorAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
    };
    
    class NameSelectorAST : public SelectorAST
    {
    public:
        IdentifierAST * name;
        
        NameSelectorAST() : name(NULL) { }
        virtual ~NameSelectorAST();
        
        virtual SourceLocation firstSourceLocation() const { return name->firstSourceLocation(); }
        virtual SourceLocation lastSourceLocation() const { return name->lastSourceLocation(); }
        virtual NameSelectorAST * asNameSelector() { return this; }
        
        virtual SelectorAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
    };
    
    class MethodCallSelectorAST : public SelectorAST
    {
    public:
        SourceLocation colon_token;
        IdentifierAST * method;
        ExpressionAST * arguments;
        
        MethodCallSelectorAST() : method(NULL), arguments(NULL) { }
        virtual ~MethodCallSelectorAST();
        
        virtual SourceLocation firstSourceLocation() const { return colon_token; }
        
        virtual SourceLocation lastSourceLocation() const {
            if(arguments)
                return arguments->lastSourceLocation();
            if(method)
                return method->lastSourceLocation();
            return colon_token;
        }
        
        virtual MethodCallSelectorAST * asMethodCallSelector() { return this; }
        
        virtual SelectorAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
    };
    
    class FunctionCallSelectorAST : public SelectorAST
    {
    public:
        ExpressionAST * arguments;
        
        FunctionCallSelectorAST() : arguments(NULL) { }
        virtual ~FunctionCallSelectorAST();
        
        virtual SourceLocation firstSourceLocation() const { return arguments->firstSourceLocation(); }
        virtual SourceLocation lastSourceLocation() const { return arguments->lastSourceLocation(); }
        virtual FunctionCallSelectorAST * asFunctionCallSelector() { return this; }

        virtual SelectorAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
   };
    
    class RecordFieldAST : public FieldAST
    {
    public:
        SelectorAST * selector;
        SourceLocation equal_token;
        ExpressionAST * value;
        SourceLocation separator_token;
        
        RecordFieldAST() : selector(NULL), value(NULL) { }
        virtual ~RecordFieldAST();
        
        virtual SourceLocation firstSourceLocation() const { return selector->firstSourceLocation(); }
        virtual SourceLocation lastSourceLocation() const { return separator_token.isValid() ? separator_token : value->lastSourceLocation(); }
        virtual RecordFieldAST * asRecordField() { return this; }

        virtual FieldAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
    };
    
    class ListFieldAST : public FieldAST
    {
    public:
        ExpressionAST * expr;
        SourceLocation separator_token;
        
        ListFieldAST() : expr(NULL) { }
        virtual ~ListFieldAST();
        
        virtual SourceLocation firstSourceLocation() const { return expr->firstSourceLocation(); }
        virtual SourceLocation lastSourceLocation() const { return separator_token.isValid() ? separator_token : expr->lastSourceLocation(); }
        virtual ListFieldAST * asListField() { return this; }
  
        virtual FieldAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
    };
    
    class ConstructorAST : public ExpressionAST
    {
    public:
        SourceLocation lcurl_token;
        FieldListAST * fields;
        SourceLocation rcurl_token;
        
        ConstructorAST() : fields(NULL) { }
        virtual ~ConstructorAST();
        
        virtual SourceLocation firstSourceLocation() const { return lcurl_token; }
        virtual SourceLocation lastSourceLocation() const { return rcurl_token; }
        virtual ConstructorAST * asConstructor() { return this; }
        
        virtual ExpressionAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
    };
    
    class FunctionExpressionAST : public ExpressionAST
    {
    public:
        SourceLocation function_token;
        FunctionBodyAST * body;
        
        FunctionExpressionAST() : body(NULL) { }
        virtual ~FunctionExpressionAST();
        
        virtual SourceLocation firstSourceLocation() const { return function_token; }
        virtual SourceLocation lastSourceLocation() const { return body ? body->lastSourceLocation() : function_token; }
        virtual FunctionExpressionAST * asFunctionExpression() { return this; }

        virtual ExpressionAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
    };
    
    class SelectorExpressionAST : public ExpressionAST
    {
    public:
        ExpressionAST * prefixexp;
        SelectorListAST * chain;
        
        SelectorExpressionAST() : prefixexp(NULL), chain(NULL) { }
        virtual ~SelectorExpressionAST();
        
        virtual SourceLocation firstSourceLocation() const { return prefixexp->firstSourceLocation(); }
        virtual SourceLocation lastSourceLocation() const { return chain ? chain->lastSourceLocation() : prefixexp->lastSourceLocation(); }
        virtual SelectorExpressionAST * asSelectorExpression() { return this; }
        
        virtual ExpressionAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
    };
    
    class FunctionArgumentsExpressionAST : public ExpressionAST
    {
    public:
        SourceLocation lbracket;
        ComaSeparatedExpressionListAST * args;
        SourceLocation rbracket;
        
        FunctionArgumentsExpressionAST() : args(NULL) { }
        virtual ~FunctionArgumentsExpressionAST();
        
        virtual SourceLocation firstSourceLocation() const { return lbracket; }
        virtual SourceLocation lastSourceLocation() const { return rbracket; }
        virtual FunctionArgumentsExpressionAST * asFunctionArgumentsExpression() { return this; }

        virtual ExpressionAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
    };
    
    class AssignmentExpressionAST : public ExpressionAST
    {
    public:
        ComaSeparatedExpressionListAST * lhs;
        SourceLocation equal_token;
        ComaSeparatedExpressionListAST * rhs;
        
        AssignmentExpressionAST() : lhs(NULL), rhs(NULL) { }
        virtual ~AssignmentExpressionAST();
        
        virtual SourceLocation firstSourceLocation() const { return lhs->firstSourceLocation(); }
        virtual SourceLocation lastSourceLocation() const { return rhs->lastSourceLocation(); }
        virtual AssignmentExpressionAST * asAssignmentExpression() { return this; }

        virtual ExpressionAST * clone(MemoryPool *) const;
    protected:
        virtual void accept0(ASTVisitor *);
        virtual bool match0(AST *, ASTMatcher *);
    };
    
}

#endif /* defined(__libluaast__LuaAST__) */

// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include "LuaASTBind.h"
#include "LuaASt.h"
#include "LuaValue.h"
#include "LuaValueOwner.h"
#include "LuaASTPathBuilder.h"
#include "LuaScopeChain.h"
#include <algorithm>
#ifdef QT_DEBUG
#include <QDebug>
#endif

namespace LuaAST {
    
    ASTBind::ASTBind(const LuaDocument * doc) :
        m_doc(doc)
    {
        m_valueOwner = new ValueOwner();
        m_rootObject = m_valueOwner->newTable(NULL);
        m_currentObject = m_rootObject;
        m_scopeMap[m_doc->ast()] = m_rootObject;
        accept(m_doc->ast());        
    }
    
    ASTBind::~ASTBind()
    {
        delete m_valueOwner;
    }
    
    ObjectValue * ASTBind::rootObjectValue() const
    {
        return m_rootObject;
    }
    
    ObjectValue * ASTBind::switchObjectValue(ObjectValue * value)
    {
#ifdef QT_DEBUG
        qDebug() << "Change scope" << value;
#endif
        std::swap(value, m_currentObject);
        return value;
    }

    ObjectValue * ASTBind::findObject(AST * node) const
    {
#ifdef LUAAST_QT
        return m_scopeMap.value(node);
#else
        ScopeMap::const_iterator it = m_scopeMap.find(node);
        if(it != m_scopeMap.end())
            return it->second;
        return NULL;
#endif
    }
    
    ObjectValue * ASTBind::bindObject(ConstructorAST * node)
    {
        ASTTableValue * table = new ASTTableValue(node, m_doc, m_valueOwner);
        m_scopeMap[node] = table;
        ObjectValue * parentObject = switchObjectValue(table);
        accept(node->fields);
        return switchObjectValue(parentObject);
    }

    bool ASTBind::visit(LocalStatementAST * ast)
    {
        ComaSeparatedExpressionListAST * exprNode = ast->explist;
        for(ComaSeparatedIdentifierListAST * node = ast->names ; node ; node = node->next, exprNode = exprNode ? exprNode->next : NULL) {
            IdentifierAST * identifier = node->value->identifier;
            ExpressionAST * expr = exprNode ? exprNode->value->expr : NULL;

#ifdef QT_DEBUG
            qDebug() << "Create local var" << identifier->identifier_token.toString(m_doc).toString();
#endif

            handleAssignment(identifier, expr, false);
        }
        return false;
    }

    bool ASTBind::visit(ForNumStatementAST * stmt)
    {
        ObjectValue * forScope = m_valueOwner->newTable(NULL);
        m_scopeMap[stmt] = forScope;
        ObjectValue * parentScope = switchObjectValue(forScope);

        ASTVariableReference * ref = new ASTVariableReference(stmt->name, stmt->index, m_doc, m_valueOwner);
#ifdef QT_DEBUG
        qDebug() << "Create for num var" << stmt->name->identifier_token.toString(m_doc).toString();
#endif
        forScope->setMember(stmt->name->identifier_token.toString(m_doc).toString(), ref);

        accept(stmt->index);
        accept(stmt->limit);
        accept(stmt->step);
        accept(stmt->block);

        switchObjectValue(parentScope);
        return false;
    }

    bool ASTBind::visit(ForListStatementAST * stmt)
    {
        ComaSeparatedIdentifierListAST * identifiers = stmt->forlist;
        ComaSeparatedExpressionListAST * expressions = stmt->explist;

        ObjectValue * forScope = m_valueOwner->newTable(NULL);
        m_scopeMap[stmt] = forScope;
        ObjectValue * parentScope = switchObjectValue(forScope);

#ifdef QT_DEBUG
        qDebug() << "Create for list identifiers";
#endif

        ExpressionAST * expr = expressions->value->expr;

        for( ; identifiers ; identifiers = identifiers->next) {
            IdentifierAST * identifier = identifiers->value->identifier;
            ASTVariableReference * ref = new ASTVariableReference(identifier, expr, m_doc, m_valueOwner);
#ifdef QT_DEBUG
            qDebug() << "Create for list var" << identifier->identifier_token.toString(m_doc).toString();
#endif
            forScope->setMember(identifier->identifier_token.toString(m_doc).toString(), ref);
        }
        accept(stmt->explist);
        accept(stmt->block);
        switchObjectValue(parentScope);

        return false;
    }

    bool ASTBind::visit(LabelStatementAST * stmt)
    {
        ASTLabelReference * ref = new ASTLabelReference(stmt, m_doc, m_valueOwner);
#ifdef QT_DEBUG
        qDebug() << "Create for label" << stmt->name->identifier_token.toString(m_doc).toString();
#endif
        m_currentObject->setMember(String("::") + stmt->name->identifier_token.toString(m_doc).toString() + String("::"), ref);
        return false;
    }

    bool ASTBind::visit(AssignmentExpressionAST * stmt)
    {
        ComaSeparatedExpressionListAST * lhs = stmt->lhs;
        ComaSeparatedExpressionListAST * rhs = stmt->rhs;
        for( ; lhs ; lhs = lhs->next, rhs = rhs ? rhs->next : NULL) {
            ExpressionAST * lhsExpr = lhs->value->expr;
            ExpressionAST * rhsExpr = rhs ? rhs->value->expr : NULL;

            if(IdentifierAST * lhsIdentifier = lhsExpr->asIdentifier()) {
#ifdef QT_DEBUG
                qDebug() << "Handle label assignment" << lhsIdentifier->identifier_token.toString(m_doc).toString();
#endif
                handleAssignment(lhsIdentifier, rhsExpr, true);
            } else {
                accept(lhsExpr);
                accept(rhsExpr);
            }
        }
        return false;
    }

    bool ASTBind::visit(BlockAST * block)
    {
        ObjectValue * blockScope = m_valueOwner->newTable(NULL);
        m_scopeMap[block] = blockScope;
        ObjectValue * parentScope = switchObjectValue(blockScope);
        accept(block->list);
        switchObjectValue(parentScope);
        return false;
    }

    void ASTBind::handleAssignment(IdentifierAST * identifier, ExpressionAST * expr, bool global)
    {
        String memberName = identifier->identifier_token.toString(m_doc).toString();

        ASTVariableReference * ref = new ASTVariableReference(identifier, expr, m_doc, m_valueOwner);
        m_currentObject->setMember(memberName, ref);

        accept(expr);
    }

    bool ASTBind::visit(ConstructorAST * expr)
    {
        bindObject(expr);
        return false;
    }

    bool ASTBind::visit(RecordFieldAST * field)
    {
        if(NameSelectorAST * name = field->selector->asNameSelector()) {
            ASTVariableReference * ref = new ASTVariableReference(name->name, field->value, m_doc, m_valueOwner);
            m_currentObject->setMember(name->name->identifier_token.toString(m_doc).toString(), ref);
        }
        return true;
    }
    
    bool ASTBind::visit(FunctionExpressionAST * expr)
    {
        return true;
    }
    
    bool ASTBind::visit(FunctionBodyAST * body)
    {
        ASTFunctionValue * functionValue = new ASTFunctionValue(body, m_doc, m_valueOwner);
        m_scopeMap[body] = functionValue;

#ifdef LUAAST_QT
        functionValue->setClassName(QString("LuaFunction@%1_%2").arg(functionValue).arg(body->firstSourceLocation().startLine));
#else
        char buf[64];
        sprintf(buf, "LuaFunction@%p_%d", (void *)functionValue, body->firstSourceLocation().startLine);
        functionValue->setClassName(buf);
#endif
        
        for(ComaSeparatedExpressionListAST * expList = body->parameters ;
            expList ;
            expList = expList->next)
        {
            if(IdentifierAST * identifier = expList->value->expr->asIdentifier()) {
                ASTVariableReference * ref = new ASTVariableReference(identifier, NULL, m_doc, m_valueOwner);
                functionValue->addArgument(identifier->identifier_token.toString(m_doc).toString(), ref);
            } else if(expList->value->expr->asDots()) {
                functionValue->setVariadic(true);
            }
        }
        
        ObjectValue * parentScope = switchObjectValue(functionValue);
        
        accept(body->block);
        
        switchObjectValue(parentScope);
        
        return false;
    }
    
    bool ASTBind::visit(LocalFunctionStatementAST * stmt)
    {
        ObjectValue * scope = NULL;
        accept(stmt->body);
#ifdef LUAAST_QT
        scope = m_scopeMap.value(stmt->body);
#else
        ScopeMap::const_iterator it = m_scopeMap.find(stmt->body);
        if(it != m_scopeMap.end())
            scope = it->second;
#endif
        
        if(scope) {
            m_currentObject->setMember(stmt->name->identifier_token.toString(m_doc).toString(), scope);
        }
        
        return false;
    }
    
    bool ASTBind::visit(FunctionStatementAST * stmt)
    {
        ObjectValue * scope = NULL;
        ObjectValue * initialScope = NULL;
        ObjectValue * functionBlock = NULL;
        
        accept(stmt->body);
        
        FunctionNameAST * fname = stmt->name;
        
#ifdef LUAAST_QT
        scope = m_scopeMap.value(stmt->body);
#else
        ScopeMap::const_iterator it = m_scopeMap.find(stmt->body);
        if(it != m_scopeMap.end())
            scope = it->second;
#endif
        
        if(!scope)
            return false;

        if(!fname->fieldsel && !fname->method_name) {
            m_rootObject->setMember(fname->name0->identifier_token.toString(m_doc).toString(), scope);
            return false;
        }
        
        ASTPathBuilder builder(m_doc->ptr());
        ASTList chain(builder((uint)fname->name0->firstSourceLocation().offset));

        String name0 = fname->name0->identifier_token.toString(m_doc).toString();
        
        StringList identifiers;
        
        for(FieldSelectorListAST * selectorQueue = fname->fieldsel ; selectorQueue ; selectorQueue = selectorQueue->next) {
            FieldSelectorAST * selector = selectorQueue->value;
            identifiers.push_back(selector->name->identifier_token.toString(m_doc).toString());
        }
        
        identifiers.push_back(fname->method_name->identifier_token.toString(m_doc).toString());
        
        for(size_t i = chain.size() ; i > 0 ; --i) {
            AST * ast = chain.at(i - 1);

#ifdef LUAAST_QT
            initialScope = m_scopeMap.value(ast);
            if(!initialScope)
                continue;
#else
            ScopeMap::const_iterator it = m_scopeMap.find(ast);
            if(it != m_scopeMap.end())
                initialScope = it->second;
            else
                continue;
#endif
            
            const Value * object = initialScope->lookupMember(name0, NULL, NULL);
            
            if(object) {
                if(identifiers.size() > 1) {
                    if(const ObjectValue * o = object->asObjectValue()) {
                        functionBlock = const_cast<ObjectValue *>(o);
                        break;
                    } else if(const ASTVariableReference * ref = object->asASTVariableReference()) {
                        if(const ObjectValue * o = findObject(ref->asExpression())) {
                            functionBlock = const_cast<ObjectValue *>(o);
                            break;
                        }
                    }
                } else if(object) {
                    if(const ASTVariableReference * ref = object->asASTVariableReference()) {
                        if(const ObjectValue * o = findObject(ref->asExpression())) {
                            if(const TableValue * table = o->asTableValue()) {
                                functionBlock = const_cast<TableValue *>(table);
                                break;
                            }
                        }
                    }
                }
            }

        }
        
        if(!functionBlock)
            return false;
 
        for(size_t i = 0 ; i < size_t(identifiers.size() - 1) ; ++i) {
            const Value * object = functionBlock->lookupMember(identifiers[i], NULL, NULL);
            if(!object)
                return false;
            if(identifiers.size() > 2 && i < size_t(identifiers.size() - 2)) {
                if(const ObjectValue * o = object->asObjectValue()) {
                    functionBlock = const_cast<ObjectValue *>(o);
                } else if(const ASTVariableReference * ref = object->asASTVariableReference()) {
                    if(const ObjectValue * o = findObject(ref->asExpression())) {
                        functionBlock = const_cast<ObjectValue *>(o);
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                if(const ASTVariableReference * ref = object->asASTVariableReference()) {
                    if(const ObjectValue * o = findObject(ref->ast())) {
                        if(const TableValue * table = o->asTableValue()) {
                            functionBlock = const_cast<TableValue *>(table);
                            break;
                        }
                    }
                }
            }
        }
        
        if(!functionBlock)
            return false;
        
        static_cast<TableValue *>(functionBlock)->setMember(identifiers[identifiers.size() - 1], scope);
        
        if(fname->method_name) {
            if(const TableValue * table = functionBlock->asTableValue()) {
                const_cast<TableValue *>(table)->setMember(Literal("self"), m_currentObject);
            }
        }
        
        return false;
    }

}

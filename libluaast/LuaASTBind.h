// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#ifndef __libluaast__LuaASTBind__
#define __libluaast__LuaASTBind__

#include "LuaASTFwd.h"
#include "LuaASTVisitor.h"
#ifdef LUAAST_QT
#include <QHash>
#else
#include <map>
#endif

namespace LuaAST {
    
    class ValueOwner;
    class ObjectValue;
    class LuaDocument;
    
    class ASTBind : protected ASTVisitor
    {
    public:
        ASTBind(const LuaDocument * doc);
        ~ASTBind();
        
        ObjectValue * rootObjectValue() const;
        ObjectValue * findObject(AST * node) const;
        
        ObjectValue * bindObject(ConstructorAST * node);
        
    private:
        ObjectValue * switchObjectValue(ObjectValue *);

        virtual bool visit(LocalStatementAST *);
        virtual bool visit(ForNumStatementAST *);
        virtual bool visit(ForListStatementAST *);
        virtual bool visit(LabelStatementAST *);
        virtual bool visit(AssignmentExpressionAST *);
        virtual bool visit(BlockAST *);
        virtual bool visit(ConstructorAST *);
        virtual bool visit(RecordFieldAST *);
        virtual bool visit(FunctionExpressionAST *);
        virtual bool visit(FunctionBodyAST *);
        virtual bool visit(LocalFunctionStatementAST *);
        virtual bool visit(FunctionStatementAST *);
        
    private:
        void handleAssignment(IdentifierAST * identifier, ExpressionAST * expr, bool global);

    private:
        const LuaDocument * m_doc;
        ValueOwner * m_valueOwner;
        ObjectValue * m_rootObject;
        ObjectValue * m_currentObject;
#ifdef LUAAST_QT
        typedef QHash<AST *, ObjectValue *> ScopeMap;
#else
        typedef std::map<AST *, ObjectValue *> ScopeMap;
#endif
        
        ScopeMap m_scopeMap;
    };
    
}

#endif /* defined(__libluaast__LuaASTBind__) */

// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


#ifndef libluaast_LuaASTFwd_h
#define libluaast_LuaASTFwd_h

#ifdef LUAAST_QT
#include <QString>
#include <QStringList>
#else
#include <string>
#include <vector>
#endif

#include <sys/types.h>

namespace LuaAST {
    
    template <typename Tptr> class List;
    
    class AST;
    class ASTVisitor;
    class ASTMatcher;

#ifndef LUAAST_QT
    class StringRef;
    typedef std::string String;
    typedef unsigned char char_type;
    typedef const char * Literal;
    typedef std::vector<std::string> StringList;
    typedef std::vector<AST *> ASTList;
#else
    typedef QStringRef StringRef;
    typedef QString String;
    typedef QChar char_type;
    typedef QLatin1String Literal;
    typedef QStringList StringList;
    typedef QList<AST *> ASTList;
#endif

    class MemoryPool;
    class Managed;
    
    class StatementAST;
    class EmptyStatementAST;
    class ExpressionAST;
    class SelectorAST;
    class FieldAST;

    class UnaryExpressionAST;
    class BinaryExpressionAST;
    class NumberAST;
    class StringAST;
    class NilAST;
    class BooleanAST;
    class DotsAST;
    class IdentifierAST;
    class IfStatementAST;
    class BlockAST;
    class WhileStatementAST;
    class DoStatementAST;
    class ForNumStatementAST;
    class ForListStatementAST;
    class RepeatStatementAST;
    class FunctionStatementAST;
    class LocalFunctionStatementAST;
    class LocalStatementAST;
    class LabelStatementAST;
    class ReturnStatementAST;
    class BreakStatementAST;
    class GotoStatementAST;
    class ExpressionStatementAST;
    class ComaSeparatedIdentifierAST;
    class ComaSeparatedExpressionAST;
    class FieldSelectorAST;
    class FunctionNameAST;
    class FunctionBodyAST;
    class BracketExpressionAST;
    class IndexSelectorAST;
    class MethodCallSelectorAST;
    class FunctionCallSelectorAST;
    class ConstructorAST;
    class RecordFieldAST;
    class ListFieldAST;
    class FunctionExpressionAST;
    class NameSelectorAST;
    class SelectorExpressionAST;
    class FunctionArgumentsExpressionAST;
    class AssignmentExpressionAST;
    
    class LuaDocument;

    typedef List<ExpressionAST *> ExpressionListAST;
    typedef List<StatementAST *> StatementListAST;
    typedef List<ComaSeparatedIdentifierAST *> ComaSeparatedIdentifierListAST;
    typedef List<ComaSeparatedExpressionAST *> ComaSeparatedExpressionListAST;
    typedef List<FieldSelectorAST *> FieldSelectorListAST;
    typedef List<FieldAST *> FieldListAST;
    typedef List<SelectorAST *> SelectorListAST;
    typedef List<IfStatementAST *> ElseIfListAST;

    typedef double lua_Number;
    
    class SourceLocation
    {
    public:
        SourceLocation() :
            offset(0),
            length(0),
            startLine(0),
            startColumn(0)
        {
        }
        
        SourceLocation(off_t _offset, size_t _length, uint _line, uint _column) :
            offset(_offset),
            length(_length),
            startLine(_line),
            startColumn(_column)
        {
        }
        
        inline bool isValid() const {
            return offset && length;
        }
        
        StringRef toString(const LuaDocument * doc) const;
        
        off_t offset;
        size_t length;
        uint startLine;
        uint startColumn;
    };
    
}

#endif

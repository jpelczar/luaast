// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include "LuaASTMatcher.h"
#include "LuaAST.h"

namespace LuaAST {
    
    ASTMatcher::ASTMatcher()
    {
    }
    
    ASTMatcher::~ASTMatcher()
    {
    }
    
    bool ASTMatcher::match(EmptyStatementAST * node, EmptyStatementAST * pattern)
    {
        pattern->semicolon_token = node->semicolon_token;
        return true;
    }
    
    bool ASTMatcher::match(UnaryExpressionAST * node, UnaryExpressionAST * pattern)
    {
        pattern->unop_token = node->unop_token;
        pattern->op = node->op;
        if(!pattern->subexpr)
            pattern->subexpr = node->subexpr;
        else if(!AST::match(node->subexpr, pattern->subexpr, this))
            return false;
        return true;
    }
    
    bool ASTMatcher::match(BinaryExpressionAST * node, BinaryExpressionAST * pattern)
    {
        if(!pattern->lhs)
            pattern->lhs = node->lhs;
        else if(!AST::match(node->lhs, pattern->lhs, this))
            return false;
        
        pattern->binop_token = node->binop_token;
        pattern->op = node->op;
        
        if(!pattern->rhs)
            pattern->rhs = node->rhs;
        else if(!AST::match(node->rhs, pattern->rhs, this))
            return false;
        
        return true;
    }
    
    bool ASTMatcher::match(NumberAST * node, NumberAST * pattern)
    {
        pattern->number_token = node->number_token;
        pattern->value = node->value;
        return true;
    }
    
    bool ASTMatcher::match(StringAST * node, StringAST * pattern)
    {
        pattern->lquot_token = node->lquot_token;
        pattern->rquot_token = node->rquot_token;
        pattern->value = node->value;
        pattern->full_string_token = node->full_string_token;
        return true;
    }
    
    bool ASTMatcher::match(NilAST * node, NilAST * pattern)
    {
        pattern->nil_token = node->nil_token;
        return true;
    }
    
    bool ASTMatcher::match(BooleanAST * node, BooleanAST * pattern)
    {
        pattern->bool_token = node->bool_token;
        pattern->value = node->value;
        return true;
    }
    
    bool ASTMatcher::match(DotsAST * node, DotsAST * pattern)
    {
        pattern->dots_token = node->dots_token;
        return true;
    }
    
    bool ASTMatcher::match(IfStatementAST * node, IfStatementAST * pattern)
    {
        pattern->if_token = node->if_token;
        
        if(!pattern->cond)
            pattern->cond = node->cond;
        else if(!AST::match(node->cond, pattern->cond, this))
            return false;
        
        pattern->then_token = node->then_token;
        
        if(!pattern->block)
            pattern->block = node->block;
        else if(!AST::match(node->block, pattern->block, this))
            return false;
        
        if(!pattern->elseif_list)
            pattern->elseif_list = node->elseif_list;
        else if(!AST::match(node->elseif_list, pattern->elseif_list, this))
            return false;
        
        pattern->else_token = node->else_token;
        
        if(!pattern->else_block)
            pattern->else_block = node->else_block;
        else if(!AST::match(node->else_block, pattern->else_block, this))
            return false;
        
        pattern->end_token = node->end_token;
        return true;
    }
    
    bool ASTMatcher::match(BlockAST * node, BlockAST * pattern)
    {
        if(!pattern->list)
            pattern->list = node->list;
        else if(!AST::match(node->list, pattern->list, this))
            return false;
        return true;
    }
    
    bool ASTMatcher::match(WhileStatementAST * node, WhileStatementAST * pattern)
    {
        pattern->while_token = node->while_token;
        
        if(!pattern->cond)
            pattern->cond = node->cond;
        else if(!AST::match(node->cond, pattern->cond, this))
            return false;
        
        pattern->do_token = node->do_token;
        
        if(!pattern->block)
            pattern->block = node->block;
        else if(!AST::match(node->block, pattern->block, this))
            return false;
        
        pattern->end_token = node->end_token;
        return true;
    }
    
    bool ASTMatcher::match(DoStatementAST * node, DoStatementAST * pattern)
    {
        pattern->do_token = node->do_token;
        
        if(!pattern->block)
            pattern->block = node->block;
        else if(!AST::match(node->block, pattern->block, this))
            return false;
        
        pattern->end_token = node->end_token;
        return true;
    }
    
    bool ASTMatcher::match(RepeatStatementAST * node, RepeatStatementAST * pattern)
    {
        pattern->repeat_token = node->repeat_token;
        
        if(!pattern->block)
            pattern->block = node->block;
        else if(!AST::match(node->block, pattern->block, this))
            return false;
        
        pattern->until_token = node->until_token;
        
        if(!pattern->cond)
            pattern->cond = node->cond;
        else if(!AST::match(node->cond, pattern->cond, this))
            return false;
        
        return true;
    }
    
    bool ASTMatcher::match(IdentifierAST * node, IdentifierAST * pattern)
    {
        pattern->identifier_token = node->identifier_token;
        return true;
    }
    
    bool ASTMatcher::match(ComaSeparatedIdentifierAST * node, ComaSeparatedIdentifierAST * pattern)
    {
        if(!pattern->identifier)
            pattern->identifier = node->identifier;
        else if(!AST::match(node->identifier, pattern->identifier, this))
            return false;
        
        pattern->coma_token = node->coma_token;
        return true;
    }
    
    bool ASTMatcher::match(ComaSeparatedExpressionAST * node, ComaSeparatedExpressionAST * pattern)
    {
        if(!pattern->expr)
            pattern->expr = node->expr;
        else if(!AST::match(node->expr, pattern->expr, this))
            return false;
        
        pattern->coma_token = node->coma_token;
        return true;
    }
    
    bool ASTMatcher::match(ForNumStatementAST * node, ForNumStatementAST * pattern)
    {
        pattern->for_token = node->for_token;
        
        if(!pattern->name)
            pattern->name = node->name;
        else if(!AST::match(node->name, pattern->name, this))
            return false;
        
        pattern->equal_token = node->equal_token;
        
        if(!pattern->index)
            pattern->index = node->index;
        else if(!AST::match(node->index, pattern->index, this))
            return false;
        
        pattern->coma1 = node->coma1;
        
        if(!pattern->limit)
            pattern->limit = node->limit;
        else if(!AST::match(node->limit, pattern->limit, this))
            return false;
        
        pattern->coma2 = node->coma2;
        
        if(!pattern->step)
            pattern->step = node->step;
        else if(!AST::match(node->step, pattern->step, this))
            return false;
        
        pattern->do_token = node->do_token;
        
        if(!pattern->block)
            pattern->block = node->block;
        else if(!AST::match(node->block, pattern->block, this))
            return false;
        
        pattern->end_token = node->end_token;
        
        return true;
    }
    
    bool ASTMatcher::match(ForListStatementAST * node, ForListStatementAST * pattern)
    {
        pattern->for_token = node->for_token;
        
        if(!pattern->forlist)
            pattern->forlist = node->forlist;
        else if(!AST::match(node->forlist, pattern->forlist, this))
            return false;
        
        pattern->in_token = node->in_token;
        
        if(!pattern->explist)
            pattern->explist = node->explist;
        else if(!AST::match(node->explist, pattern->explist, this))
            return false;
        
        pattern->do_token = node->do_token;
        
        if(!pattern->block)
            pattern->block = node->block;
        else if(!AST::match(node->block, pattern->block, this))
            return false;
        
        pattern->end_token = node->end_token;
        
        return true;
    }
    
    bool ASTMatcher::match(FieldSelectorAST * node, FieldSelectorAST * pattern)
    {
        pattern->selector_token = node->selector_token;
        
        if(!pattern->name)
            pattern->name = node->name;
        else if(!AST::match(node->name, pattern->name, this))
            return false;
        
        return true;
    }
    
    bool ASTMatcher::match(FunctionNameAST * node, FunctionNameAST * pattern)
    {
        if(!pattern->name0)
            pattern->name0 = node->name0;
        else if(!AST::match(node->name0, pattern->name0, this))
            return false;
        
        if(!pattern->fieldsel)
            pattern->fieldsel = node->fieldsel;
        else if(!AST::match(node->fieldsel, pattern->fieldsel, this))
            return false;
        
        pattern->method_token = node->method_token;
        
        if(!pattern->method_name)
            pattern->method_name = node->method_name;
        else if(!AST::match(node->method_name, pattern->method_name, this))
            return false;
        
        return true;
    }
    
    bool ASTMatcher::match(FunctionBodyAST * node, FunctionBodyAST * pattern)
    {
        pattern->lparen_token = node->lparen_token;
        
        if(!pattern->parameters)
            pattern->parameters = node->parameters;
        else if(!AST::match(node->parameters, pattern->parameters, this))
            return false;
        
        pattern->rparen_token = node->rparen_token;
        
        if(!pattern->block)
            pattern->block = node->block;
        else if(!AST::match(node->block, pattern->block, this))
            return false;
        
        pattern->end_token = node->end_token;
        
        return true;
    }
    
    bool ASTMatcher::match(FunctionStatementAST * node, FunctionStatementAST * pattern)
    {
        pattern->function_token = node->function_token;
        
        if(!pattern->name)
            pattern->name = node->name;
        else if(!AST::match(node->name, pattern->name, this))
            return false;
        
        if(!pattern->body)
            pattern->body = node->body;
        else if(!AST::match(node->body, pattern->body, this))
            return false;
        
        return true;
    }
    
    bool ASTMatcher::match(LocalFunctionStatementAST * node, LocalFunctionStatementAST * pattern)
    {
        pattern->local_token = node->local_token;
        pattern->function_token = node->function_token;
        
        if(!pattern->name)
            pattern->name = node->name;
        else if(!AST::match(node->name, pattern->name, this))
            return false;
        
        if(!pattern->body)
            pattern->body = node->body;
        else if(!AST::match(node->body, pattern->body, this))
            return false;
        
        return true;
    }
    
    bool ASTMatcher::match(LocalStatementAST * node, LocalStatementAST * pattern)
    {
        pattern->local_token = node->local_token;
        
        if(!pattern->names)
            pattern->names = node->names;
        else if(!AST::match(node->names, pattern->names, this))
            return false;
        
        pattern->equal_token = node->equal_token;
        
        if(!pattern->explist)
            pattern->explist = node->explist;
        else if(!AST::match(node->explist, pattern->explist, this))
            return false;
        
        return true;
    }
    
    bool ASTMatcher::match(LabelStatementAST * node, LabelStatementAST * pattern)
    {
        pattern->ldb_token = node->ldb_token;
        
        if(!pattern->name)
            pattern->name = node->name;
        else if(!AST::match(node->name, pattern->name, this))
            return false;
        
        pattern->rdb_token = node->rdb_token;
        
        return true;
    }
    
    bool ASTMatcher::match(ReturnStatementAST * node, ReturnStatementAST * pattern)
    {
        pattern->return_token = node->return_token;
        
        if(!pattern->explist)
            pattern->explist = node->explist;
        else if(!AST::match(node->explist, pattern->explist, this))
            return false;
        
        pattern->semicolon_token = node->semicolon_token;
        
        return true;
    }
    
    bool ASTMatcher::match(BreakStatementAST * node, BreakStatementAST * pattern)
    {
        pattern->break_token = node->break_token;
        return true;
    }
    
    bool ASTMatcher::match(GotoStatementAST * node, GotoStatementAST * pattern)
    {
        pattern->goto_token = node->goto_token;
        
        if(!pattern->label)
            pattern->label = node->label;
        else if(!AST::match(node->label, pattern->label, this))
            return false;
        
        return true;
    }
    
    bool ASTMatcher::match(ExpressionStatementAST * node, ExpressionStatementAST * pattern)
    {
        if(!pattern->expr)
            pattern->expr = node->expr;
        else if(!AST::match(node->expr, pattern->expr, this))
            return false;
        
        return true;
    }
    
    bool ASTMatcher::match(BracketExpressionAST * node, BracketExpressionAST * pattern)
    {
        pattern->lbracket_token = node->lbracket_token;
        
        if(!pattern->expr)
            pattern->expr = node->expr;
        else if(!AST::match(node->expr, pattern->expr, this))
            return false;
        
        pattern->rbracket_token = node->rbracket_token;
        return true;
    }
    
    bool ASTMatcher::match(IndexSelectorAST * node, IndexSelectorAST * pattern)
    {
        pattern->lsquare_token = node->lsquare_token;
        
        if(!pattern->expr)
            pattern->expr = node->expr;
        else if(!AST::match(node->expr, pattern->expr, this))
            return false;
        
        pattern->rsquare_token = node->rsquare_token;
        return true;
    }
    
    bool ASTMatcher::match(MethodCallSelectorAST * node, MethodCallSelectorAST * pattern)
    {
        pattern->colon_token = node->colon_token;
        
        if(!pattern->method)
            pattern->method = node->method;
        else if(!AST::match(node->method, pattern->method, this))
            return false;
        
        if(!pattern->arguments)
            pattern->arguments = node->arguments;
        else if(!AST::match(node->arguments, pattern->arguments, this))
            return false;
        
        return true;
    }
    
    bool ASTMatcher::match(FunctionCallSelectorAST * node, FunctionCallSelectorAST * pattern)
    {
        if(!pattern->arguments)
            pattern->arguments = node->arguments;
        else if(!AST::match(node->arguments, pattern->arguments, this))
            return false;
        
        return true;
    }
    
    bool ASTMatcher::match(RecordFieldAST * node, RecordFieldAST * pattern)
    {
        if(!pattern->selector)
            pattern->selector = node->selector;
        else if(!AST::match(node->selector, pattern->selector, this))
            return false;
        
        pattern->equal_token = node->equal_token;
        
        if(!pattern->value)
            pattern->value = node->value;
        else if(!AST::match(node->value, pattern->value, this))
            return false;
        
        pattern->separator_token = node->separator_token;
        return true;
    }
    
    bool ASTMatcher::match(ListFieldAST * node, ListFieldAST * pattern)
    {
        if(!pattern->expr)
            pattern->expr = node->expr;
        else if(!AST::match(node->expr, pattern->expr, this))
            return false;
        
        pattern->separator_token = node->separator_token;
        return true;
    }
    
    bool ASTMatcher::match(ConstructorAST * node, ConstructorAST * pattern)
    {
        pattern->lcurl_token = node->lcurl_token;
        
        if(!pattern->fields)
            pattern->fields = node->fields;
        else if(!AST::match(node->fields, pattern->fields, this))
            return false;
        
        pattern->rcurl_token = node->rcurl_token;
        return true;
    }
    
    bool ASTMatcher::match(FunctionExpressionAST * node, FunctionExpressionAST * pattern)
    {
        pattern->function_token = node->function_token;
        
        if(!pattern->body)
            pattern->body = node->body;
        else if(!AST::match(node->body, pattern->body, this))
            return false;
        return true;
    }
    
    bool ASTMatcher::match(NameSelectorAST * node, NameSelectorAST * pattern)
    {
        if(!pattern->name)
            pattern->name = node->name;
        else if(!AST::match(node->name, pattern->name, this))
            return false;
        return true;
    }
    
    bool ASTMatcher::match(SelectorExpressionAST * node, SelectorExpressionAST * pattern)
    {
        if(!pattern->prefixexp)
            pattern->prefixexp = node->prefixexp;
        else if(!AST::match(node->prefixexp, pattern->prefixexp, this))
            return false;
        
        if(!pattern->chain)
            pattern->chain = node->chain;
        else if(!AST::match(node->chain, pattern->chain, this))
            return false;
        
        return true;
    }
    
    bool ASTMatcher::match(FunctionArgumentsExpressionAST * node, FunctionArgumentsExpressionAST * pattern)
    {
        pattern->lbracket = node->lbracket;
        
        if(!pattern->args)
            pattern->args = node->args;
        else if(!AST::match(node->args, pattern->args, this))
            return false;
        
        pattern->rbracket = node->rbracket;
        
        return true;
    }
    
    bool ASTMatcher::match(AssignmentExpressionAST * node, AssignmentExpressionAST * pattern)
    {
        if(!pattern->lhs)
            pattern->lhs = node->lhs;
        else if(!AST::match(node->lhs, pattern->lhs, this))
            return false;
        
        pattern->equal_token = node->equal_token;
        
        if(!pattern->rhs)
            pattern->rhs = node->rhs;
        else if(!AST::match(node->rhs, pattern->rhs, this))
            return false;
        
        return true;
    }
    
}

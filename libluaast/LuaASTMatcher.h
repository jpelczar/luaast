// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


#ifndef __libluaast__LuaASTMatcher__
#define __libluaast__LuaASTMatcher__

#include "LuaASTFwd.h"

namespace LuaAST {
    
    class ASTMatcher
    {
    public:
        ASTMatcher();
        virtual ~ASTMatcher();
        
        virtual bool match(EmptyStatementAST * node, EmptyStatementAST * pattern);
        virtual bool match(UnaryExpressionAST * node, UnaryExpressionAST * pattern);
        virtual bool match(BinaryExpressionAST * node, BinaryExpressionAST * pattern);
        virtual bool match(NumberAST * node, NumberAST * pattern);
        virtual bool match(StringAST * node, StringAST * pattern);
        virtual bool match(NilAST * node, NilAST * pattern);
        virtual bool match(BooleanAST * node, BooleanAST * pattern);
        virtual bool match(DotsAST * node, DotsAST * pattern);
        virtual bool match(IfStatementAST * node, IfStatementAST * pattern);
        virtual bool match(BlockAST * node, BlockAST * pattern);
        virtual bool match(WhileStatementAST * node, WhileStatementAST * pattern);
        virtual bool match(DoStatementAST * node, DoStatementAST * pattern);
        virtual bool match(RepeatStatementAST * node, RepeatStatementAST * pattern);
        virtual bool match(IdentifierAST * node, IdentifierAST * pattern);
        virtual bool match(ComaSeparatedIdentifierAST * node, ComaSeparatedIdentifierAST * pattern);
        virtual bool match(ComaSeparatedExpressionAST * node, ComaSeparatedExpressionAST * pattern);
        virtual bool match(ForNumStatementAST * node, ForNumStatementAST * pattern);
        virtual bool match(ForListStatementAST * node, ForListStatementAST * pattern);
        virtual bool match(FieldSelectorAST * node, FieldSelectorAST * pattern);
        virtual bool match(FunctionNameAST * node, FunctionNameAST * pattern);
        virtual bool match(FunctionBodyAST * node, FunctionBodyAST * pattern);
        virtual bool match(FunctionStatementAST * node, FunctionStatementAST * pattern);
        virtual bool match(LocalFunctionStatementAST * node, LocalFunctionStatementAST * pattern);
        virtual bool match(LocalStatementAST * node, LocalStatementAST * pattern);
        virtual bool match(LabelStatementAST * node, LabelStatementAST * pattern);
        virtual bool match(ReturnStatementAST * node, ReturnStatementAST * pattern);
        virtual bool match(BreakStatementAST * node, BreakStatementAST * pattern);
        virtual bool match(GotoStatementAST * node, GotoStatementAST * pattern);
        virtual bool match(ExpressionStatementAST * node, ExpressionStatementAST * pattern);
        virtual bool match(BracketExpressionAST * node, BracketExpressionAST * pattern);
        virtual bool match(IndexSelectorAST * node, IndexSelectorAST * pattern);
        virtual bool match(MethodCallSelectorAST * node, MethodCallSelectorAST * pattern);
        virtual bool match(FunctionCallSelectorAST * node, FunctionCallSelectorAST * pattern);
        virtual bool match(RecordFieldAST * node, RecordFieldAST * pattern);
        virtual bool match(ListFieldAST * node, ListFieldAST * pattern);
        virtual bool match(ConstructorAST * node, ConstructorAST * pattern);
        virtual bool match(FunctionExpressionAST * node, FunctionExpressionAST * pattern);
        virtual bool match(NameSelectorAST * node, NameSelectorAST * pattern);
        virtual bool match(SelectorExpressionAST * node, SelectorExpressionAST * pattern);
        virtual bool match(FunctionArgumentsExpressionAST * node, FunctionArgumentsExpressionAST * pattern);
        virtual bool match(AssignmentExpressionAST * node, AssignmentExpressionAST * pattern);
    };
    
}

#endif /* defined(__libluaast__LuaASTMatcher__) */

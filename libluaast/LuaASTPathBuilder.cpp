// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include "LuaASTPathBuilder.h"
#include "LuaAST.h"

namespace LuaAST {
    
    ASTPathBuilder::ASTPathBuilder(const LuaDocument::Ptr& doc) :
        m_doc(doc)
    {
    }
    
    ASTList ASTPathBuilder::operator()(uint offset)
    {
        m_result.clear();
        m_offset = offset;
        accept(m_doc->ast());
        return m_result;
    }
    
    bool ASTPathBuilder::containsOffset(const SourceLocation& from, const SourceLocation& to) const
    {
        return m_offset >= from.offset && m_offset < (to.offset + to.length);
    }
    
    bool ASTPathBuilder::preVisit(AST * ast)
    {
        if(BlockAST * block = ast->asBlock())
            return containsOffset(block->firstSourceLocation(), block->lastSourceLocation());
        if(StatementAST * stmt = ast->asStatement())
            return containsOffset(stmt->firstSourceLocation(), stmt->lastSourceLocation());
        if(ExpressionAST * expr = ast->asExpression())
            return containsOffset(expr->firstSourceLocation(), expr->lastSourceLocation());
        if(FunctionBodyAST * body = ast->asFunctionBody())
            return containsOffset(body->firstSourceLocation(), body->lastSourceLocation());
        return true;
    }
    
    bool ASTPathBuilder::visit(FunctionBodyAST * ast)
    {
        m_result.push_back(ast);
        return true;
    }
    
    bool ASTPathBuilder::visit(ConstructorAST * expr)
    {
        m_result.push_back(expr);
        return true;
    }
    
    bool ASTPathBuilder::visit(ForNumStatementAST * stmt)
    {
        m_result.push_back(stmt);
        return true;
    }
    
    bool ASTPathBuilder::visit(ForListStatementAST * stmt)
    {
        m_result.push_back(stmt);
        return true;
    }
    
    bool ASTPathBuilder::visit(BlockAST * block)
    {
        m_result.push_back(block);
        return true;
    }
    
}

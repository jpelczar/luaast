// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#ifndef __libluaast__LuaASTPathBuilder__
#define __libluaast__LuaASTPathBuilder__

#include "LuaASTFwd.h"
#include "LuaASTVisitor.h"
#include "LuaDocument.h"

namespace LuaAST {
    
    class ASTPathBuilder : protected ASTVisitor
    {
    public:
        ASTPathBuilder(const LuaDocument::Ptr& doc);
        
        ASTList operator()(uint offset);
        
    private:
        bool containsOffset(const SourceLocation& from, const SourceLocation& to) const;
        
        virtual bool preVisit(AST *);
        virtual bool visit(FunctionBodyAST *);
        virtual bool visit(ConstructorAST *);
        virtual bool visit(ForNumStatementAST * stmt);
        virtual bool visit(ForListStatementAST * stmt);
        virtual bool visit(BlockAST * block);
        
    private:
        ASTList m_result;
        LuaDocument::Ptr m_doc;
        uint m_offset;
    };
    
}

#endif /* defined(__libluaast__LuaASTPathBuilder__) */

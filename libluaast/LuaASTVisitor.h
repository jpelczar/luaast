// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


#ifndef __libluaast__LuaASTVisitor__
#define __libluaast__LuaASTVisitor__

#include "LuaASTFwd.h"

namespace LuaAST {
    
    class ASTVisitor
    {
    public:
        ASTVisitor();
        virtual ~ASTVisitor();
        
        void accept(AST *ast);
        
        template <typename Tptr>
        void accept(List<Tptr> *it)
        {
            for (; it; it = it->next)
                accept(it->value);
        }
        
        virtual bool preVisit(AST *) { return true; }
        virtual void postVisit(AST *) { }
        
        virtual bool visit(EmptyStatementAST *) { return true; }
        virtual bool visit(UnaryExpressionAST *) { return true; }
        virtual bool visit(BinaryExpressionAST *) { return true; }
        virtual bool visit(NumberAST *) { return true; }
        virtual bool visit(StringAST *) { return true; }
        virtual bool visit(NilAST *) { return true; }
        virtual bool visit(BooleanAST *) { return true; }
        virtual bool visit(DotsAST *) { return true; }
        virtual bool visit(IfStatementAST *) { return true; }
        virtual bool visit(BlockAST *) { return true; }
        virtual bool visit(WhileStatementAST *) { return true; }
        virtual bool visit(DoStatementAST *) { return true; }
        virtual bool visit(ForNumStatementAST *) { return true; }
        virtual bool visit(ForListStatementAST *) { return true; }
        virtual bool visit(RepeatStatementAST *) { return true; }
        virtual bool visit(FunctionStatementAST *) { return true; }
        virtual bool visit(LocalStatementAST *) { return true; }
        virtual bool visit(LabelStatementAST *) { return true; }
        virtual bool visit(ReturnStatementAST *) { return true; }
        virtual bool visit(BreakStatementAST *) { return true; }
        virtual bool visit(GotoStatementAST *) { return true; }
        virtual bool visit(ExpressionStatementAST *) { return true; }
        virtual bool visit(IdentifierAST *) { return true; }
        virtual bool visit(ComaSeparatedIdentifierAST *) { return true; }
        virtual bool visit(ComaSeparatedExpressionAST *) { return true; }
        virtual bool visit(FieldSelectorAST *) { return true; }
        virtual bool visit(FunctionNameAST *) { return true; }
        virtual bool visit(FunctionBodyAST *) { return true; }
        virtual bool visit(LocalFunctionStatementAST *) { return true; }
        virtual bool visit(BracketExpressionAST *) { return true; }
        virtual bool visit(IndexSelectorAST *) { return true; }
        virtual bool visit(MethodCallSelectorAST *) { return true; }
        virtual bool visit(FunctionCallSelectorAST *) { return true; }
        virtual bool visit(ConstructorAST *) { return true; }
        virtual bool visit(RecordFieldAST *) { return true; }
        virtual bool visit(ListFieldAST *) { return true; }
        virtual bool visit(FunctionExpressionAST *) { return true; }
        virtual bool visit(NameSelectorAST *) { return true; }
        virtual bool visit(SelectorExpressionAST *) { return true; }
        virtual bool visit(FunctionArgumentsExpressionAST *) { return true; }
        virtual bool visit(AssignmentExpressionAST *) { return true; }
        
        virtual void endVisit(EmptyStatementAST *) { }
        virtual void endVisit(UnaryExpressionAST *) { }
        virtual void endVisit(BinaryExpressionAST *) { }
        virtual void endVisit(NumberAST *) { }
        virtual void endVisit(StringAST *) { }
        virtual void endVisit(NilAST *) { }
        virtual void endVisit(BooleanAST *) { }
        virtual void endVisit(DotsAST *) { }
        virtual void endVisit(IfStatementAST *) { }
        virtual void endVisit(BlockAST *) { }
        virtual void endVisit(WhileStatementAST *) { }
        virtual void endVisit(DoStatementAST *) { }
        virtual void endVisit(ForNumStatementAST *) { }
        virtual void endVisit(ForListStatementAST *) { }
        virtual void endVisit(RepeatStatementAST *) { }
        virtual void endVisit(FunctionStatementAST *) { }
        virtual void endVisit(LocalStatementAST *) { }
        virtual void endVisit(LabelStatementAST *) { }
        virtual void endVisit(ReturnStatementAST *) { }
        virtual void endVisit(BreakStatementAST *) { }
        virtual void endVisit(GotoStatementAST *) { }
        virtual void endVisit(ExpressionStatementAST *) { }
        virtual void endVisit(IdentifierAST *) { }
        virtual void endVisit(ComaSeparatedIdentifierAST *) { }
        virtual void endVisit(ComaSeparatedExpressionAST *) { }
        virtual void endVisit(FieldSelectorAST *) { }
        virtual void endVisit(FunctionNameAST *) { }
        virtual void endVisit(FunctionBodyAST *) { }
        virtual void endVisit(LocalFunctionStatementAST *) { }
        virtual void endVisit(BracketExpressionAST *) { }
        virtual void endVisit(IndexSelectorAST *) { }
        virtual void endVisit(MethodCallSelectorAST *) { }
        virtual void endVisit(FunctionCallSelectorAST *) { }
        virtual void endVisit(ConstructorAST *) { }
        virtual void endVisit(RecordFieldAST *) { }
        virtual void endVisit(ListFieldAST *) { }
        virtual void endVisit(FunctionExpressionAST *) { }
        virtual void endVisit(NameSelectorAST *) { }
        virtual void endVisit(SelectorExpressionAST *) { }
        virtual void endVisit(FunctionArgumentsExpressionAST *) { }
        virtual void endVisit(AssignmentExpressionAST *) { }
    };
    
}

#endif /* defined(__libluaast__LuaASTVisitor__) */

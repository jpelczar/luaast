// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include "LuaContext.h"
#include "LuaValueOwner.h"
#include "LuaValue.h"

namespace LuaAST {
    
    Context::ContextPtr Context::create(const Snapshot::Ptr& snapshot, ValueOwner * valueOwner)
    {
        ContextPtr ptr(new Context(snapshot, valueOwner));
#ifdef LUAAST_QT
        ptr->m_weak_self = ptr.toWeakRef();
#else
        ptr->m_weak_self = ptr;
#endif
        return ptr;
    }
    
    Context::~Context()
    {
    }
    
    Context::Context(const Snapshot::Ptr& snapshot, ValueOwner * valueOwner) :
        m_snapshot(snapshot),
        m_valueOwner(valueOwner)
    {
    }
    
    Context::ContextPtr Context::ptr() const
    {
#ifdef LUAAST_QT
        return m_weak_self.toStrongRef();
#else
        return m_weak_self.lock();
#endif
    }
    
    ValueOwner * Context::valueOwner() const
    {
#ifdef LUAAST_QT
        return m_valueOwner.data();
#else
        return m_valueOwner.get();
#endif
    }
    
    Snapshot::Ptr Context::snapshot() const
    {
        return m_snapshot;
    }
    
    const Value * Context::lookupReference(const Value * value) const
    {
        ReferenceContext refContext(ptr());
        return refContext.lookupReference(value);
    }
    
    ReferenceContext::ReferenceContext(const Context::ContextPtr& context) :
        m_context(context)
    {
    }
    
    const Context::ContextPtr& ReferenceContext::context() const
    {
        return m_context;
    }
    
    const Value * ReferenceContext::lookupReference(const Value * value)
    {
        const Reference * ref = value_cast<Reference>(value);
        if(!ref)
            return value;
        
#ifdef LUAAST_QT
        if(m_references.contains(ref))
            return value;
#else
        std::set<const Reference *>::const_iterator it = std::find(std::begin(m_references), std::end(m_references), ref);
        if(it != m_references.end())
            return value;
#endif
        
        m_references.insert(ref);
        const Value * v = ref->value(this);
        m_references.erase(m_references.find(ref));
        
        return v;
    }
    
}

// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#ifndef __libluaast__LuaContext__
#define __libluaast__LuaContext__

#include "LuaDocument.h"
#ifdef LUAAST_QT
#include <QSet>
#else
#include <set>
#endif

namespace LuaAST {

    class ValueOwner;
    class Reference;
    class Value;
    
    class Context
    {
    public:
#ifdef LUAAST_QT
        typedef QSharedPointer<Context> ContextPtr;
#else
        typedef std::shared_ptr<Context> ContextPtr;
#endif
        
        static ContextPtr create(const Snapshot::Ptr& snapshot, ValueOwner * valueOwner);
        ~Context();
        
        ContextPtr ptr() const;
        
        ValueOwner * valueOwner() const;
        Snapshot::Ptr snapshot() const;
        
        const Value * lookupReference(const Value * value) const;
        
    private:
        Context(const Snapshot::Ptr& snapshot, ValueOwner * valueOwner);
        
        Snapshot::Ptr m_snapshot;
#ifdef LUAAST_QT
        QSharedPointer<ValueOwner> m_valueOwner;
        QWeakPointer<Context> m_weak_self;
#else
        std::shared_ptr<ValueOwner> m_valueOwner;
        std::weak_ptr<Context> m_weak_self;
#endif
    };
    
    class ReferenceContext
    {
    public:
        ReferenceContext(const Context::ContextPtr& context);
        
        const Context::ContextPtr& context() const;
        
        const Value * lookupReference(const Value * value);
        
    private:
        Context::ContextPtr m_context;
#ifdef LUAAST_QT
        QSet<const Reference *> m_references;
#else
        std::set<const Reference *> m_references;
#endif
    };
    
}

#endif /* defined(__libluaast__LuaContext__) */

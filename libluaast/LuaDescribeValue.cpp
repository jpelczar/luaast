// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include "LuaDescribeValue.h"
#include "LuaStringRef.h"
#include "LuaAST.h"

namespace LuaAST {
    
    String DescribeValue::describe(const Value * value, int depth, Context::ContextPtr ptr)
    {
        DescribeValue o(depth, 0, 2, ptr);
        return o(value);
    }
    
    DescribeValue::DescribeValue(int detailDepth, int startIndent, int indentIncrement, Context::ContextPtr ptr) :
        m_context(ptr),
        m_depth(detailDepth),
        m_indent(startIndent),
        m_indentIncrement(indentIncrement),
        m_newLine(true)
    {
    }
    
    DescribeValue::~DescribeValue()
    {
    }
    
    String DescribeValue::operator()(const Value * value)
    {
        if(value)
            value->accept(this);
        else
            dump("**NULL**");
        return description();
    }

    void DescribeValue::dump(const char * s)
    {
        m_description.append(s);
        m_newLine = false;
    }
    
    void DescribeValue::dump(const String& s)
    {
        m_description.append(s);
        m_newLine = false;
    }
    
    void DescribeValue::dump(const StringRef& s)
    {
        m_description.append(s.constData(), s.length());
        m_newLine = false;
    }
    
    void DescribeValue::dumpNewLine()
    {
        dump("\n");
        static String indent_block("          ");
        int indent = m_indent;
        while(indent > 10) {
            m_description.append(indent_block);
            indent -= 10;
        }
#ifdef LUAAST_QT
        if(indent > 0)
            m_description.append(indent_block.left(indent));
#else
        if(indent > 0)
            m_description.append(indent_block.c_str(), indent);
#endif
    }
    
    void DescribeValue::openContext(const char * str)
    {
        dump(str);
        m_indent += m_indentIncrement;
        m_newLine = true;
    }
    
    void DescribeValue::closeContext(const char * str)
    {
        m_indent -= m_indentIncrement;
        if(m_newLine)
            dump(" ");
        else if(str[0]) {
            dumpNewLine();
        }
        dump(str);
    }

    void DescribeValue::dumpValue(const char * name, const Value * value, bool opensContext)
    {
        char temp[128];
        dump(name);
        snprintf(temp, sizeof(temp) - 1, "@%p", (const void *)value);
        dump(temp);
        String fileName;
        int line, column;
        if(value && value->getSourceLocation(&fileName, &line, &column)) {
            dump(" - ");
            dump(fileName);
            snprintf(temp, sizeof(temp) - 1, ":%d:%d", line, column);
            dump(temp);
        }
        if(opensContext)
            openContext();
    }
    
    void DescribeValue::visit(const UndefinedValue * value)
    {
        dumpValue("Undefined", value, false);
        m_newLine = false;
    }
    
    void DescribeValue::visit(const NilValue * value)
    {
        dumpValue("Nil", value, false);
        m_newLine = false;
    }
    
    void DescribeValue::visit(const BooleanValue * value)
    {
        dumpValue("Boolean", value, false);
        m_newLine = false;
    }
    
    void DescribeValue::visit(const NumberValue * value)
    {
        dumpValue("Number", value, false);
        m_newLine = false;
    }
    
    void DescribeValue::visit(const StringValue * value)
    {
        dumpValue("String", value, false);
        m_newLine = false;
    }
    
    class ShowTableMembers : public MemberVisitor
    {
    public:
        DescribeValue& d;
        
        ShowTableMembers(DescribeValue& d) : d(d) { }
        
        virtual bool visitMember(const String& name, const Value * value)
        {
            d.dumpNewLine();
            d.dump(name);
            d.openContext(":");
            value->accept(&d);
            d.closeContext(" ");
            return true;
        }
    };
    
    void DescribeValue::visit(const TableValue * table)
    {
        bool print_detail = (m_visited.find(table) == m_visited.end() && m_depth > 0);
        --m_depth;
        m_visited.insert(table);
        
        dumpValue("Table", table, print_detail);
        if(print_detail) {
            if(table->className().size() > 0) {
                dumpNewLine();
                dump("className:");
                dump(table->className());
            }
            if(table->metaTable()) {
                dumpNewLine();
                dump("metaTable:");
                (*this)(table->metaTable());
            }
            ShowTableMembers show(*this);
            table->visitMembers(&show);
            dumpNewLine();
            closeContext();
        }
        ++m_depth;
    }
    
    void DescribeValue::visit(const FunctionValue * func)
    {
        bool print_detail = (m_visited.find(func) == m_visited.end() && m_depth > 0);
        m_visited.insert(func);
        --m_depth;
        
        dumpValue("Function", func, print_detail);
        if(print_detail) {
            if(func->className().size() > 0) {
                dumpNewLine();
                dump("className:");
                dump(func->className());
            }
            if(func->metaTable()) {
                dumpNewLine();
                dump("metaTable:");
                (*this)(func->metaTable());
            }
            dumpNewLine();
            dump("returnValues:");
            openContext("[");
            for(int i = 0 ; i < func->returnValueCount() ; ++i) {
                dumpNewLine();
                (*this)(func->returnValue(i));
            }
            closeContext("]");
            dumpNewLine();
            dump("arguments:");
            openContext("[");
            for(int i = 0 ; i < func->namedArgumentCount() ; ++i) {
                dumpNewLine();
                dump(func->argumentName(i));
                dump(" ");
                (*this)(func->argument(i));
            }
            closeContext("]");
            dumpNewLine();
            if(func->isVariadic())
                dump("isVariadic:true");
            closeContext();
        }
        
        ++m_depth;
    }
    
    void DescribeValue::visit(const UserDataValue * value)
    {
        dumpValue("UserData", value, false);
        m_newLine = false;
    }
    
    void DescribeValue::visit(const Reference * ref)
    {
        bool print_detail = (m_visited.find(ref) == m_visited.end() && m_depth > 0);
        --m_depth;
        m_visited.insert(ref);
        if(const ASTVariableReference * var = ref->asASTVariableReference()) {
            dumpValue("ASTVariableReference", var, print_detail);
            if(print_detail) {
                if(IdentifierAST * ast = var->ast()->asIdentifier()) {
                    dumpNewLine();
                    dump("Variable: ");
                    dump(ast->identifier_token.toString(var->document()));
                }
                if(m_context) {
                    const Value * v = m_context->lookupReference(var);
                    if(v) {
                        dumpNewLine();
                        dump("ref:");
                        openContext();
                        dumpNewLine();
                        (*this)(v);
                        closeContext();
                    }
                }
            }
        } else if(const ASTLabelReference * label = ref->asASTLabelReference()) {
            dumpValue("ASTLabelReference", label, print_detail);
            if(print_detail) {
                if(LabelStatementAST * ast = label->ast()->asLabel()) {
                    dumpNewLine();
                    dump("Label: ::");
                    dump(ast->name->identifier_token.toString(label->document()));
                    dump("::");
                }
            }
        }
        
        if(print_detail) {
            char tmp[128];
            dumpNewLine();
            const ValueOwner * owner = ref->valueOwner();
            snprintf(tmp, sizeof(tmp) - 1, "valueOwner@%p", (const void *)owner);
            dump(tmp);
            if(m_context) {
                dumpNewLine();
                dump("referenceObject:");
                const Value * ref2 = m_context->lookupReference(ref);
                openContext();
                dumpNewLine();
                (*this)(ref2);
                closeContext();
            }
            closeContext();
        }
        ++m_depth;
    }
    
}
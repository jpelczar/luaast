// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


#ifndef __libluaast__LuaDescribeValue__
#define __libluaast__LuaDescribeValue__

#include "LuaValue.h"
#ifdef LUAAST_QT
#include <QSet>
#else
#include <set>
#endif

namespace LuaAST {
    
    class DescribeValue : public ValueVisitor
    {
    public:
        static String describe(const Value * value, int depth = 1, Context::ContextPtr ptr = Context::ContextPtr());
        
        DescribeValue(int detailDepth = 1, int startIndent = 0, int indentIncrement = 2, Context::ContextPtr ptr = Context::ContextPtr());
        ~DescribeValue();
        
        String operator()(const Value * value);
        inline String description() const { return m_description; }
        
        void dump(const char * s);
        void dump(const String& s);
        void dump(const StringRef& s);
        void dumpNewLine();
        void openContext(const char * str = "{");
        void closeContext(const char * str = "}");
        void dumpValue(const char * name, const Value * value, bool opensContext);
        
    protected:
        virtual void visit(const UndefinedValue *);
        virtual void visit(const NilValue *);
        virtual void visit(const BooleanValue *);
        virtual void visit(const NumberValue *);
        virtual void visit(const StringValue *);
        virtual void visit(const TableValue *);
        virtual void visit(const FunctionValue *);
        virtual void visit(const UserDataValue *);
        virtual void visit(const Reference *);
        
    private:
#ifdef LUAAST_QT
        QSet<const Value *> m_visited;
#else
        std::set<const Value *> m_visited;
#endif
        String m_description;
        Context::ContextPtr m_context;
        int m_depth;
        int m_indent;
        int m_indentIncrement;
        bool m_newLine;
        
        DescribeValue(const DescribeValue&);
        DescribeValue& operator = (const DescribeValue&);
    };
    
}

#endif /* defined(__libluaast__LuaDescribeValue__) */

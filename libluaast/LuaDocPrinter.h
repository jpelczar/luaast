// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#ifndef __libluaast__LuaDocPrinter__
#define __libluaast__LuaDocPrinter__

#include "LuaASTVisitor.h"
#include "LuaDocument.h"
#include "LuaAST.h"
#include <ostream>
#include <stack>
#include <typeinfo>

namespace LuaAST {
    
    class DocPrinter : public ASTVisitor
    {
        LuaDocument::Ptr m_doc;
        std::ostream& m_os;
        int m_indentLevel;
        bool m_had_newline;
        
        inline std::string token(const SourceLocation& loc) const {
#ifdef LUAAST_QT
            return  m_doc->midRef(loc.offset, loc.length).toUtf8().constData();
#else
            return  m_doc->midRef(loc.offset, loc.length).toString();
#endif
        }
        
        inline std::ostream& os() {
            if(m_had_newline) {
                m_had_newline = false;
                for(int i = 1 ; i < m_indentLevel ; ++i) {
                    m_os << "\t";
                }
            }
            return m_os;
        }
        
        inline void newLine() {
            if(!m_had_newline) {
                m_had_newline = true;
                m_os << std::endl;
            }
        }
        
        std::stack<bool> m_new_line_sep;
    public:
        DocPrinter(LuaDocument::Ptr doc, std::ostream& os) :
            m_doc(doc),
            m_os(os),
            m_indentLevel(0)
        {
            m_new_line_sep.push(false);
        }
        
        ~DocPrinter();
        
        virtual bool preVisit(AST * ast) {
//            os() << " [" << typeid(*ast).name() << " ]";
            return true;
        }
        
        virtual bool visit(EmptyStatementAST * stmt) {
            os() << token(stmt->semicolon_token);
            return true;
        }
        
        virtual bool visit(UnaryExpressionAST * expr) {
            os() << token(expr->unop_token) << " ";
            return true;
        }
        
        virtual bool visit(BinaryExpressionAST * expr) {
            AST::accept(expr->lhs, this);
            os() << " " << token(expr->binop_token) << " ";
            AST::accept(expr->rhs, this);
            return false;
        }
        
        virtual bool visit(NumberAST * expr) {
            os() << expr->value;
            return true;
        }
        
        virtual bool visit(StringAST * expr) {
            os() << token(expr->full_string_token);
            return true;
        }
        
        virtual bool visit(NilAST * expr) {
            os() << token(expr->nil_token);
            return true;
        }
        
        virtual bool visit(BooleanAST * expr) {
            os() << token(expr->bool_token);
            return true;
        }
        
        virtual bool visit(DotsAST * dots) {
            os() << token(dots->dots_token);
            return true;
        }
        
        virtual bool visit(IfStatementAST * stmt) {
            os() << token(stmt->if_token) << " ";
            AST::accept(stmt->cond, this);
            os() << " " << token(stmt->then_token);
            newLine();
            AST::accept(stmt->block, this);
            AST::accept(stmt->elseif_list, this);
            if(stmt->else_token.isValid()) {
                os() << token(stmt->else_token);
                AST::accept(stmt->else_block, this);
            }
            
            os() << token(stmt->end_token);
            newLine();
            return false;
        }
        
        virtual bool visit(BlockAST *) {
            ++m_indentLevel;
             return true;
        }
        
        virtual bool visit(WhileStatementAST * stmt) {
            os() << token(stmt->while_token) << " ";
            AST::accept(stmt->cond, this);
            os() << " " << token(stmt->do_token);
            newLine();
            AST::accept(stmt->block, this);
            os() << token(stmt->end_token);
            newLine();
            return false;
        }
        
        virtual bool visit(DoStatementAST * stmt) {
            os() << token(stmt->do_token);
            newLine();
            return true;
        }
        
        virtual bool visit(ForNumStatementAST * stmt) {
            os() << token(stmt->for_token) << " ";
            AST::accept(stmt->name, this);
            os() << " " << token(stmt->equal_token) << " ";
            AST::accept(stmt->index, this);
            os() << token(stmt->coma1) << " ";
            AST::accept(stmt->limit, this);
            if(stmt->coma2.isValid()) {
                os() << token(stmt->coma2) << " ";
                AST::accept(stmt->step, this);
            }
            os() << " " << token(stmt->do_token);
            newLine();
            AST::accept(stmt->block, this);
            os() << token(stmt->end_token);
            newLine();
            return false;
        }
        
        virtual bool visit(ForListStatementAST * stmt) {
            os() << token(stmt->for_token) << " ";
            AST::accept(stmt->forlist, this);
            os() << " " << token(stmt->in_token) << " ";
            AST::accept(stmt->explist, this);
            os() << " " << token(stmt->do_token);
            newLine();
            AST::accept(stmt->block, this);
            os() << token(stmt->end_token);
            newLine();
            return false;
        }
        
        virtual bool visit(RepeatStatementAST * stmt) {
            os() << token(stmt->repeat_token);
            newLine();
            AST::accept(stmt->block, this);
            os() << token(stmt->until_token) << " ";
            AST::accept(stmt->cond, this);
            newLine();
            return false;
        }
        
        virtual bool visit(FunctionStatementAST * stmt) {
            os() << token(stmt->function_token) << " ";
            AST::accept(stmt->name, this);
            os() << " ";
            AST::accept(stmt->body, this);
            newLine();
            return false;
        }
        
        virtual bool visit(LocalStatementAST * stmt) {
            os() << token(stmt->local_token) << " ";
            AST::accept(stmt->names, this);
            os() << " " << token(stmt->equal_token) << " ";
            AST::accept(stmt->explist, this);
            newLine();
            return false;
        }
        
        virtual bool visit(LabelStatementAST * stmt) {
            os() << token(stmt->ldb_token);
            AST::accept(stmt->name, this);
            os() << token(stmt->rdb_token);
            newLine();
            return false;
        }
        
        virtual bool visit(ReturnStatementAST * stmt) {
            os() << token(stmt->return_token) << " ";
            AST::accept(stmt->explist, this);
            os() << token(stmt->semicolon_token);
            newLine();
            return false;
        }
        
        virtual bool visit(BreakStatementAST * stmt) {
            os() << token(stmt->break_token);
            newLine();
            return false;
        }
        
        virtual bool visit(GotoStatementAST * stmt) {
            os() << token(stmt->goto_token) << " ";
            AST::accept(stmt->label, this);
            newLine();
            return false;
        }
        
        virtual bool visit(ExpressionStatementAST * expr)
        {
            /* endVisit will handle newline */
            return true;
        }
        
        virtual bool visit(IdentifierAST * expr) {
            os() << token(expr->identifier_token);
            return true;
        }
        
        virtual bool visit(ComaSeparatedIdentifierAST *) {
            return true;
        }
        
        virtual bool visit(ComaSeparatedExpressionAST *) {
            return true;
        }
        
        virtual bool visit(FieldSelectorAST * sel) {
            os() << token(sel->selector_token);
            return true;
        }
        
        virtual bool visit(FunctionNameAST * ast) {
            AST::accept(ast->name0, this);
            AST::accept(ast->fieldsel, this);
            os() << token(ast->method_token);
            AST::accept(ast->method_name, this);
            return false;
        }
        
        virtual bool visit(FunctionBodyAST * ast) {
            os() << token(ast->lparen_token);
            AST::accept(ast->parameters, this);
            os() << token(ast->rparen_token);
            newLine();
            AST::accept(ast->block, this);
            os() << token(ast->end_token);
            newLine();
            return false;
        }
        
        virtual bool visit(LocalFunctionStatementAST * stmt) {
            os() << token(stmt->local_token) << " " << token(stmt->function_token) << " ";
            AST::accept(stmt->name, this);
            os() << " ";
            AST::accept(stmt->body, this);
            return false;
        }
        
        virtual bool visit(BracketExpressionAST * expr) {
            os() << token(expr->lbracket_token);
            return true;
        }
        
        virtual bool visit(IndexSelectorAST * sel) {
            os() << token(sel->lsquare_token);
            return true;
        }
        
        virtual bool visit(MethodCallSelectorAST * sel) {
            os() << token(sel->colon_token);
            AST::accept(sel->method, this);
            os() << " ";
            AST::accept(sel->arguments, this);
            return false;
        }
        
        virtual bool visit(FunctionCallSelectorAST *) {
            return true;
        }
        
        virtual bool visit(ConstructorAST * expr) {
            os() << token(expr->lcurl_token);
            if(expr->lcurl_token.startLine != expr->rcurl_token.startLine) {
                newLine();
                ++m_indentLevel;
                m_new_line_sep.push(true);
            }
            return true;
        }
        
        virtual bool visit(RecordFieldAST * field) {
            AST::accept(field->selector, this);
            os() << " " << token(field->equal_token) << " ";
            AST::accept(field->value, this);
            if(field->separator_token.isValid()) {
                os() << token(field->separator_token) << " ";
            }
            return false;
        }
        
        virtual bool visit(ListFieldAST *) {
            return true;
        }
        
        virtual bool visit(FunctionExpressionAST * expr) {
            os() << token(expr->function_token) << " ";
            return true;
        }
        
        virtual bool visit(NameSelectorAST *) {
            return true;
        }
        
        virtual bool visit(SelectorExpressionAST *) {
            return true;
        }
        
        virtual bool visit(FunctionArgumentsExpressionAST * expr) {
            os() << token(expr->lbracket);
            return true;
        }
        
        virtual bool visit(AssignmentExpressionAST * expr) {
            AST::accept(expr->lhs, this);
            os() << " " << token(expr->equal_token) << " ";
            AST::accept(expr->rhs, this);
            return false;
        }
        
        virtual void endVisit(EmptyStatementAST *) {
            newLine();
        }
        
        virtual void endVisit(UnaryExpressionAST *) { }
        virtual void endVisit(BinaryExpressionAST *) { }
        virtual void endVisit(NumberAST *) { }
        virtual void endVisit(StringAST *) { }
        virtual void endVisit(NilAST *) { }
        virtual void endVisit(BooleanAST *) { }
        virtual void endVisit(DotsAST *) { }
        virtual void endVisit(IfStatementAST *) { }
        
        virtual void endVisit(BlockAST *)
        {
            --m_indentLevel;
            newLine();
        }
        
        virtual void endVisit(WhileStatementAST *) { }
        
        virtual void endVisit(DoStatementAST * stmt) {
            os() << token(stmt->end_token);
            newLine();
        }
        
        virtual void endVisit(ForNumStatementAST *) { }
        virtual void endVisit(ForListStatementAST *) { }
        virtual void endVisit(RepeatStatementAST *) { }
        virtual void endVisit(FunctionStatementAST *) { }
        virtual void endVisit(LocalStatementAST *) { }
        virtual void endVisit(LabelStatementAST *) { }
        virtual void endVisit(ReturnStatementAST *) { }
        virtual void endVisit(BreakStatementAST *) { }
        virtual void endVisit(GotoStatementAST *) { }
        
        virtual void endVisit(ExpressionStatementAST *)
        {
            newLine();
        }
        
        virtual void endVisit(IdentifierAST *) { }
        
        virtual void endVisit(ComaSeparatedIdentifierAST * expr) {
            if(expr->coma_token.isValid()) {
                os() << token(expr->coma_token);
                if(!m_new_line_sep.top())
                    os() << " ";
                else
                    newLine();
            } else {
                if(m_new_line_sep.top())
                    newLine();
            }
        }
        
        virtual void endVisit(ComaSeparatedExpressionAST * expr) {
            if(expr->coma_token.isValid()) {
                os() << token(expr->coma_token);
                if(!m_new_line_sep.top())
                    os() << " ";
                else
                    newLine();
            } else {
                if(m_new_line_sep.top())
                    newLine();
            }
        }
        
        virtual void endVisit(FieldSelectorAST *) { }
        virtual void endVisit(FunctionNameAST *) { }
        virtual void endVisit(FunctionBodyAST *) { }
        virtual void endVisit(LocalFunctionStatementAST *) { }
        
        virtual void endVisit(BracketExpressionAST * expr)
        {
            os() << token(expr->rbracket_token);
        }
        
        virtual void endVisit(IndexSelectorAST * sel)
        {
            os() << token(sel->rsquare_token);
        }
        
        virtual void endVisit(MethodCallSelectorAST *) { }
        virtual void endVisit(FunctionCallSelectorAST *) { }
        
        virtual void endVisit(ConstructorAST * expr) {
            if(expr->lcurl_token.startLine != expr->rcurl_token.startLine) {
                newLine();
                --m_indentLevel;
                m_new_line_sep.pop();
            }
            os() << token(expr->rcurl_token);
        }
        
        virtual void endVisit(RecordFieldAST *) { }
       
        virtual void endVisit(ListFieldAST * field) {
            if(field->separator_token.isValid()) {
                os() << token(field->separator_token) << " ";
            }
        }
        
        virtual void endVisit(FunctionExpressionAST *) { }
        virtual void endVisit(NameSelectorAST *) { }
        virtual void endVisit(SelectorExpressionAST *) { }
        
        virtual void endVisit(FunctionArgumentsExpressionAST * expr)
        {
            os() << token(expr->rbracket);
        }
        
        virtual void endVisit(AssignmentExpressionAST *) { }
    };
    
}

#endif /* defined(__libluaast__LuaDocPrinter__) */

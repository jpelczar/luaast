// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


#include "LuaDocument.h"
#include "LuaASTManaged.h"
#include "LuaLexer.h"
#include "LuaParser.h"
#include "LuaASTBind.h"
#ifdef LUAAST_QT
#include <QDebug>
#include <QFileInfo>
#include <QDir>
#else
#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include <libgen.h>
#include <algorithm>
#endif

namespace LuaAST {
    
    LuaDocument::LuaDocument() :
        m_pool(NULL),
        m_ast(NULL),
        m_version(0),
        m_bind(NULL)
    {
        m_pool = new MemoryPool();
    }
    
    LuaDocument::~LuaDocument()
    {
        delete m_bind;
        delete m_pool;
    }
    
    LuaDocument::Ptr LuaDocument::create(const String &file_name, const String &doc)
    {
#ifdef LUAAST_QT
        Ptr self(new LuaDocument());
        self->m_weak_self = self.toWeakRef();
#else
        Ptr self(std::make_shared<LuaDocument>());
        self->m_weak_self = self;
#endif

        self->m_code = doc;
        
#ifdef LUAAST_QT
        QFileInfo fileInfo(file_name);
        self->m_path = QDir::cleanPath(fileInfo.absolutePath());
        self->m_file_name = QDir::cleanPath(file_name);
#else
        char actualpath[PATH_MAX + 1];
        
        if(!realpath(file_name.c_str(), actualpath)) {
            self->m_file_name = file_name;
        } else {
            self->m_file_name = actualpath;
        }
        
        self->m_path = dirname(actualpath);
#endif

        return self;
    }
    
    void LuaDocument::addError(const LuaError& error)
    {
#ifdef LUAAST_QT
        qDebug() << "Error =" << error.message << "at line" << error.line;
#else
        std::cerr << "Error = " << error.message << " at line " << error.line<< std::endl;
#endif

        m_errors.push_back(error);
    }
    
    void LuaDocument::addComment(const SourceLocation& token)
    {
#ifdef LUAAST_QT
        qDebug() << "Comment = '" << m_code.mid(token.offset, token.length) << "'";
#else
        std::cout << "Comment = '" << m_code.substr(token.offset, token.length) << "'" << std::endl;
#endif

        m_comments.push_back(token);
    }
    
#ifdef LUAAST_QT
    QStringRef LuaDocument::newStringRef(const QString& s)
    {
        size_t pos = m_extra_code.size();
        m_extra_code.append(s);
        return m_extra_code.midRef(pos);
    }
#endif

    StringRef LuaDocument::newStringRef(const std::string& s)
    {
        size_t pos = m_extra_code.size();
#ifdef LUAAST_QT
        m_extra_code.append(QString::fromUtf8(s.c_str(), s.length()));
        return m_extra_code.midRef(pos);
#else
        m_extra_code.append(s);
        return StringRef(&m_extra_code, pos, s.length());
#endif
    }
    
    StringRef LuaDocument::newStringRef(const char * s, size_t length)
    {
        size_t pos = m_extra_code.size();
#ifdef LUAAST_QT
        m_extra_code.append(QString::fromUtf8(s, length));
        return m_extra_code.midRef(pos);
#else
        m_extra_code.append(s, length);
        return StringRef(&m_extra_code, pos, length);
#endif
    }
    
    bool LuaDocument::parse()
    {
        Lexer lexer(this);
        lexer.setCode(m_code);
        
        Parser parser(this, &lexer);
        parser.parse();
        
        m_bind = new ASTBind(this);
        
        return m_errors.empty();
    }

    Snapshot::Snapshot()
    {
    }
    
    Snapshot::Snapshot(const Snapshot& other) :
        m_documents(other.m_documents),
        m_documentsByPath(other.m_documentsByPath)
    {
    }
    
    Snapshot::~Snapshot()
    {
    }
    
    Snapshot::Ptr Snapshot::create()
    {
        Ptr ptr(new Snapshot());
#ifdef LUAAST_QT
        ptr->m_weak_self = ptr.toWeakRef();
#else
        ptr->m_weak_self = ptr;
#endif
        return ptr;
    }

    void Snapshot::insert(const LuaDocument::Ptr& doc, bool allowInvalid)
    {
        if(doc && (allowInvalid || doc->isValid())) {
            m_documents[doc->fileName()] = doc;
            m_documentsByPath[doc->fileName()].push_back(doc);
        }
    }
    
    void Snapshot::remove(const String& fileName)
    {
        LuaDocument::Ptr doc = m_documents[fileName];
        if(doc) {
            String path(doc->path());
#ifdef LUAAST_QT
            m_documents.remove(fileName);
            QList<LuaDocument::Ptr> docs = m_documentsByPath.value(path);
            docs.removeAll(doc);
            m_documentsByPath[path] = docs;
#else
            m_documents.erase(fileName);
            std::vector<LuaDocument::Ptr> docs = m_documentsByPath[path];
            std::remove(std::begin(docs), std::end(docs), doc);
            m_documentsByPath[path] = docs;
#endif
        }
    }
    
    LuaDocument::Ptr Snapshot::document(const String& fileName) const
    {
#ifdef LUAAST_QT
        return m_documents.value(QDir::cleanPath(fileName));
#else
        char actualpath[PATH_MAX + 1];
        std::map<std::string, LuaDocument::Ptr>::const_iterator it;
        
        if(!realpath(fileName.c_str(), actualpath)) {
            it = m_documents.find(fileName);
        } else {
            it = m_documents.find(actualpath);
        }
        
        return m_documents.end() == it ? LuaDocument::Ptr() : it->second;
#endif
    }
    
#ifndef LUAAST_QT
    std::vector<LuaDocument::Ptr> Snapshot::documentsInDirectory(const std::string& fileName) const
    {
        char actualpath[PATH_MAX + 1];
        std::map<std::string, std::vector<LuaDocument::Ptr> >::const_iterator it;
        
        if(!realpath(fileName.c_str(), actualpath)) {
            it = m_documentsByPath.find(fileName);
        } else {
            it = m_documentsByPath.find(actualpath);
        }
        
        return m_documentsByPath.end() == it ? std::vector<LuaDocument::Ptr>() : it->second;
    }
#else
    QList<LuaDocument::Ptr> Snapshot::documentsInDirectory(const QString& fileName) const
    {
        return m_documentsByPath.value(QDir::cleanPath(fileName));
    }
#endif
    
    LuaDocument::Ptr Snapshot::documentFromSource(const String& sourceCode,
                                        const String& fileName)
    {
        LuaDocument::Ptr doc = LuaDocument::create(fileName, sourceCode);
        if(LuaDocument::Ptr origDoc = document(fileName))
            doc->setDocumentVersion(origDoc->documentVersion());
        return doc;
    }
    
}

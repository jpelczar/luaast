// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


#ifndef __libluaast__LuaDocument__
#define __libluaast__LuaDocument__

#include "LuaASTFwd.h"
#include "LuaError.h"

#ifdef LUAAST_QT
#include <QSharedPointer>
#include <QList>
#include <QHash>
#else
#include "LuaStringRef.h"
#include <memory>
#include <vector>
#include <map>
#endif

namespace LuaAST {
    
    class Lexer;
    class Parser;
    class ASTBind;
    
    class LuaDocument
    {
    public:
        LuaDocument();
        ~LuaDocument();
        
#ifdef LUAAST_QT
        typedef QSharedPointer<LuaDocument> Ptr;
        Ptr ptr() const { return m_weak_self.toStrongRef(); }
#else
        typedef std::shared_ptr<LuaDocument> Ptr;
        Ptr ptr() const { return m_weak_self.lock(); }
#endif
        
        static Ptr create(const String& file_name, const String& doc);

        void addError(const LuaError& error);
        void addComment(const SourceLocation& token);
        
        inline const LuaDocument * self() const { return this; }
        
#ifdef LUAAST_QT
        inline QStringRef midRef(off_t pos, size_t size) const { return m_code.midRef(pos, size); }
#else
        inline StringRef midRef(off_t pos, size_t size) const { return StringRef(&m_code, pos, size); }
#endif

#ifdef LUAAST_QT
        QStringRef newStringRef(const QString& s);
#endif

        StringRef newStringRef(const std::string& s);
        StringRef newStringRef(const char * s, size_t length);

        bool parse();
        
        inline BlockAST * ast() const { return m_ast; }
        inline bool isValid() const { return m_errors.size() == 0; }
        inline const String& fileName() const { return m_file_name; }
        inline const String& path() const { return m_path; }
        inline int documentVersion() const { return m_version; }
        void setDocumentVersion(int ver) { m_version = ver; }
        
        inline ASTBind * bind() const { return m_bind; }
        
    private:
        friend class Parser;
        friend class Lexer;
        
#ifdef LUAAST_QT
        QWeakPointer<LuaDocument> m_weak_self;
#else
        std::weak_ptr<LuaDocument> m_weak_self;
#endif

        String m_file_name;
        MemoryPool * m_pool;
        BlockAST * m_ast;
#ifdef LUAAST_QT
        QList<LuaError> m_errors;
        QList<SourceLocation> m_comments;
#else
        std::vector<LuaError> m_errors;
        std::vector<SourceLocation> m_comments;
#endif
        String m_code;
        String m_extra_code;
        String m_path;
        int m_version;
        ASTBind * m_bind;
    };
    
    class Snapshot
    {
#ifdef LUAAST_QT
        typedef QHash<QString, LuaDocument::Ptr> Base;
        QHash<QString, LuaDocument::Ptr> m_documents;
        QHash<QString, QList<LuaDocument::Ptr> > m_documentsByPath;
        QWeakPointer<Snapshot> m_weak_self;
#else
        typedef std::map<std::string, LuaDocument::Ptr> Base;
        std::map<std::string, LuaDocument::Ptr> m_documents;
        std::map<std::string, std::vector<LuaDocument::Ptr> > m_documentsByPath;
        std::weak_ptr<Snapshot> m_weak_self;
#endif
        
        Snapshot();
        Snapshot(const Snapshot&);
    public:
#ifdef LUAAST_QT
        typedef QSharedPointer<Snapshot> Ptr;
#else
        typedef std::shared_ptr<Snapshot> Ptr;
#endif
        
        ~Snapshot();
        
        static Ptr create();
        
        typedef Base::iterator iterator;
        typedef Base::const_iterator const_iterator;
        
        const_iterator begin() const { return m_documents.begin(); }
        const_iterator end() const { return m_documents.end(); }
        
        void insert(const LuaDocument::Ptr& doc, bool allowInvalid = false);
        void remove(const String& fileName);
        
        LuaDocument::Ptr document(const String& fileName) const;
#ifndef LUAAST_QT
        std::vector<LuaDocument::Ptr> documentsInDirectory(const std::string& fileName) const;
#else
        QList<LuaDocument::Ptr> documentsInDirectory(const QString& fileName) const;
#endif
        
        LuaDocument::Ptr documentFromSource(const String& sourceCode,
                                            const String& fileName);
    };
    
}

#endif /* defined(__libluaast__LuaDocument__) */

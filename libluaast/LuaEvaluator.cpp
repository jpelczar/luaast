// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include "LuaEvaluator.h"
#include "LuaAST.h"
#include "LuaScopeChain.h"
#include "LuaValueOwner.h"
#include "LuaASTBind.h"
#include <algorithm>

namespace LuaAST {
    
    Evaluator::Evaluator(const ScopeChain * scopeChain, ReferenceContext * referenceContext) :
        m_valueOwner(scopeChain->context()->valueOwner()),
        m_context(scopeChain->context()),
        m_referenceContext(referenceContext),
        m_scopeChain(scopeChain),
        m_result(NULL)
    {
    }
    
    Evaluator::~Evaluator()
    {
    }
    
    const Value * Evaluator::operator()(AST * ast)
    {
        return value(ast);
    }
    
    const Value * Evaluator::value(AST * ast)
    {
        const Value * result = reference(ast);
        if(const Reference * ref = value_cast<Reference>(result)) {
            if(m_referenceContext) {
                result = m_referenceContext->lookupReference(ref);
            } else {
                result = m_context->lookupReference(ref);
            }
        }
        if(!result)
            result = m_valueOwner->undefinedValue();
        return result;
    }
    
    const Value * Evaluator::reference(AST * ast)
    {
        const Value * saved = NULL;
        std::swap(saved, m_result);
        accept(ast);
        std::swap(saved, m_result);
        return saved;
    }
    
    bool Evaluator::visit(EmptyStatementAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(UnaryExpressionAST * expr)
    {
        switch(expr->op)
        {
            case UnaryExpressionAST::OPR_LEN:
            case UnaryExpressionAST::OPR_MINUS:
            case UnaryExpressionAST::OPR_NOT:
                return m_valueOwner->numberValue();
            default:
                return value(expr->subexpr);
        }
    }
    
    bool Evaluator::visit(BinaryExpressionAST * expr)
    {
        const Value * lhs = 0;

        if(expr->lhs) {
            lhs = value(expr->lhs);
        }
        
        if(expr->op == BinaryExpressionAST::OPR_NOBINOPR) {
            m_result = lhs;
            return false;
        }
        
        if(expr->op == BinaryExpressionAST::OPR_EQ ||
           expr->op == BinaryExpressionAST::OPR_LT ||
           expr->op == BinaryExpressionAST::OPR_LE ||
           expr->op == BinaryExpressionAST::OPR_NE ||
           expr->op == BinaryExpressionAST::OPR_GT ||
           expr->op == BinaryExpressionAST::OPR_GE ||
           expr->op == BinaryExpressionAST::OPR_AND ||
           expr->op == BinaryExpressionAST::OPR_OR)
        {
            m_result = m_valueOwner->booleanValue();
            return false;
        }
        
        return false;
    }
    
    bool Evaluator::visit(NumberAST *)
    {
        m_result = m_valueOwner->numberValue();
        return false;
    }
    
    bool Evaluator::visit(StringAST *)
    {
        m_result = m_valueOwner->stringValue();
        return false;
    }
    
    bool Evaluator::visit(NilAST *)
    {
        m_result = m_valueOwner->nilValue();
        return false;
    }
    
    bool Evaluator::visit(BooleanAST *)
    {
        m_result = m_valueOwner->booleanValue();
        return false;
    }
    
    bool Evaluator::visit(DotsAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(IfStatementAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(BlockAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(WhileStatementAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(DoStatementAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(ForNumStatementAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(ForListStatementAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(RepeatStatementAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(FunctionStatementAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(LocalStatementAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(LabelStatementAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(ReturnStatementAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(BreakStatementAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(GotoStatementAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(ExpressionStatementAST *)
    {
        return true;
    }
    
    bool Evaluator::visit(IdentifierAST * ast)
    {
        StringRef nameRef(m_scopeChain->document()->midRef(ast->identifier_token.offset, ast->identifier_token.length));
        if(nameRef.length() == 0)
            return false;
        m_result = m_scopeChain->lookup(nameRef.toString(), NULL);
        return false;
    }

    bool Evaluator::visit(ComaSeparatedIdentifierAST *)
    {
        return true;
    }
    
    bool Evaluator::visit(ComaSeparatedExpressionAST *)
    {
        return true;
    }
    
    bool Evaluator::visit(FieldSelectorAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(FunctionNameAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(FunctionBodyAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(LocalFunctionStatementAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(BracketExpressionAST *)
    {
        return true;
    }
    
    bool Evaluator::visit(IndexSelectorAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(MethodCallSelectorAST * expr)
    {
        if(const Value * base = value(expr->method)) {
            if(const FunctionValue * func = base->asFunctionValue()) {
                m_result = func->returnValue(0);
            }
        }
        return false;
    }
    
    bool Evaluator::visit(FunctionCallSelectorAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(ConstructorAST * ast)
    {
        m_result = m_scopeChain->document()->bind()->findObject(ast);
        if(m_result == NULL)
            m_result = m_valueOwner->tableValue();
        return false;
    }
    
    bool Evaluator::visit(RecordFieldAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(ListFieldAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(FunctionExpressionAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(NameSelectorAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(SelectorExpressionAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(FunctionArgumentsExpressionAST *)
    {
        return false;
    }
    
    bool Evaluator::visit(AssignmentExpressionAST * expr)
    {
        m_result = value(expr->rhs->value);
        return false;
    }
    
}


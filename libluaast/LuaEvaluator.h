// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#ifndef __libluaast__LuaEvaluator__
#define __libluaast__LuaEvaluator__

#include "LuaContext.h"
#include "LuaASTVisitor.h"

namespace LuaAST {
    
    class ScopeChain;
    
    class Evaluator : public ASTVisitor
    {
    public:
        Evaluator(const ScopeChain * scopeChain, ReferenceContext * referenceContext);
        ~Evaluator();
        
        const Value * operator()(AST * ast);
        
        virtual bool visit(EmptyStatementAST *);
        virtual bool visit(UnaryExpressionAST *);
        virtual bool visit(BinaryExpressionAST *);
        virtual bool visit(NumberAST *);
        virtual bool visit(StringAST *);
        virtual bool visit(NilAST *);
        virtual bool visit(BooleanAST *);
        virtual bool visit(DotsAST *);
        virtual bool visit(IfStatementAST *);
        virtual bool visit(BlockAST *);
        virtual bool visit(WhileStatementAST *);
        virtual bool visit(DoStatementAST *);
        virtual bool visit(ForNumStatementAST *);
        virtual bool visit(ForListStatementAST *);
        virtual bool visit(RepeatStatementAST *);
        virtual bool visit(FunctionStatementAST *);
        virtual bool visit(LocalStatementAST *);
        virtual bool visit(LabelStatementAST *);
        virtual bool visit(ReturnStatementAST *);
        virtual bool visit(BreakStatementAST *);
        virtual bool visit(GotoStatementAST *);
        virtual bool visit(ExpressionStatementAST *);
        virtual bool visit(IdentifierAST *);
        virtual bool visit(ComaSeparatedIdentifierAST *);
        virtual bool visit(ComaSeparatedExpressionAST *);
        virtual bool visit(FieldSelectorAST *);
        virtual bool visit(FunctionNameAST *);
        virtual bool visit(FunctionBodyAST *);
        virtual bool visit(LocalFunctionStatementAST *);
        virtual bool visit(BracketExpressionAST *);
        virtual bool visit(IndexSelectorAST *);
        virtual bool visit(MethodCallSelectorAST *);
        virtual bool visit(FunctionCallSelectorAST *);
        virtual bool visit(ConstructorAST *);
        virtual bool visit(RecordFieldAST *);
        virtual bool visit(ListFieldAST *);
        virtual bool visit(FunctionExpressionAST *);
        virtual bool visit(NameSelectorAST *);
        virtual bool visit(SelectorExpressionAST *);
        virtual bool visit(FunctionArgumentsExpressionAST *);
        virtual bool visit(AssignmentExpressionAST *);
        
    private:
        const Value * value(AST * ast);
        const Value * reference(AST * ast);
        
    private:
        ValueOwner * m_valueOwner;
        Context::ContextPtr m_context;
        ReferenceContext * m_referenceContext;
        const ScopeChain * m_scopeChain;
        const Value * m_result;
    };
    
}

#endif /* defined(__libluaast__LuaEvaluator__) */

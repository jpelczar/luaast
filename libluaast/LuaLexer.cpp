// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


/******************************************************************************
 * Copyright (C) 1994-2011 Lua.org, PUC-Rio.  All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

#include "LuaLexer.h"
#include "LuaDocument.h"
#include "LuaASTFwd.h"
#include <cassert>
#include <cstring>
#include <cctype>
#include <math.h>
#ifdef LUAAST_QT
#include <QLocale>
#else
#include <clocale>
#endif

namespace LuaAST {

#ifndef lua_h
#ifndef llimits_h
#define cast(t, exp)    ((t)(exp))
    
#define cast_byte(i)    cast(lu_byte, (i))
#define cast_num(i)     cast(lua_Number, (i))
#define cast_int(i)     cast(int, (i))
#define cast_uchar(i)   cast(unsigned char, (i))
#endif
    
#ifndef lconfig_h
#define lua_str2number(s,p)     strtod((s), (p))
    
#if defined(LUA_USE_STRTODHEX)
#define lua_strx2number(s,p)    strtod((s), (p))
#endif
#endif

#if !defined(lua_strx2number)
    static int isneg (const char **s) {
        if (**s == '-') { (*s)++; return 1; }
        else if (**s == '+') (*s)++;
        return 0;
    }
    
    static inline int luaO_hexavalue (int c) {
        if (isdigit(c)) return c - '0';
        else return tolower(c) - 'a' + 10;
    }
    
    static lua_Number readhexa (const char **s, lua_Number r, int *count) {
        for (; isxdigit(cast_uchar(**s)); (*s)++) {  /* read integer part */
            r = (r * 16.0) + cast_num(luaO_hexavalue(cast_uchar(**s)));
            (*count)++;
        }
        return r;
    }
    
    /*
     ** convert an hexadecimal numeric string to a number, following
     ** C99 specification for 'strtod'
     */
    static lua_Number lua_strx2number (const char *s, char **endptr) {
        lua_Number r = 0.0;
        int e = 0, i = 0;
        int neg = 0;  /* 1 if number is negative */
        *endptr = cast(char *, s);  /* nothing is valid yet */
        while (isspace(cast_uchar(*s))) s++;  /* skip initial spaces */
        neg = isneg(&s);  /* check signal */
        if (!(*s == '0' && (*(s + 1) == 'x' || *(s + 1) == 'X')))  /* check '0x' */
            return 0.0;  /* invalid format (no '0x') */
        s += 2;  /* skip '0x' */
        r = readhexa(&s, r, &i);  /* read integer part */
        if (*s == '.') {
            s++;  /* skip dot */
            r = readhexa(&s, r, &e);  /* read fractional part */
        }
        if (i == 0 && e == 0)
            return 0.0;  /* invalid format (no digit) */
        e *= -4;  /* each fractional digit divides value by 2^-4 */
        *endptr = cast(char *, s);  /* valid up to here */
        if (*s == 'p' || *s == 'P') {  /* exponent part? */
            int exp1 = 0;
            int neg1;
            s++;  /* skip 'p' */
            neg1 = isneg(&s);  /* signal */
            if (!isdigit(cast_uchar(*s)))
                goto ret;  /* must have at least one digit */
            while (isdigit(cast_uchar(*s)))  /* read exponent */
                exp1 = exp1 * 10 + *(s++) - '0';
            if (neg1) exp1 = -exp1;
            e += exp1;
        }
        *endptr = cast(char *, s);  /* valid up to here */
    ret:
        if (neg) r = -r;
        return ldexp(r, e);
    }
#endif
    
    static int luaO_str2d (const char *s, size_t len, lua_Number *result) {
        char *endptr;
        if (strpbrk(s, "nN"))  /* reject 'inf' and 'nan' */
            return 0;
        else if (strpbrk(s, "xX"))  /* hexa? */
            *result = lua_strx2number(s, &endptr);
        else
            *result = lua_str2number(s, &endptr);
        if (endptr == s) return 0;  /* nothing recognized */
        while (isspace(cast_uchar(*endptr))) endptr++;
        return (endptr == s + len);  /* OK if no trailing characters */
    }
#endif
    
#define buff2d(b,e)     luaO_str2d((b).c_str(), (b).length(), e)

#if !defined(getlocaledecpoint)
#define getlocaledecpoint()     (localeconv()->decimal_point[0])
#endif
    
    static void buffreplace (std::string *ls, char from, char to) {
        size_t n = ls->length();
        char *p = const_cast<char *>(ls->c_str());
        while (n--)
            if (p[n] == from) p[n] = to;
    }
    
    Lexer::Lexer(LuaDocument * doc) :
        m_doc(doc),
        m_dec_point(getlocaledecpoint())
    {
    }
    
    Lexer::~Lexer()
    {
    }
    
    void Lexer::setCode(const String& code)
    {
        m_code = code;
        m_current = 0;
        m_linenumber = 1;
        m_lastline = 1;
        m_saved.resize(0);
        m_saved.reserve(128);
#ifdef LUAAST_QT
        m_doc_begin = reinterpret_cast<const char_type *>(m_code.constData());
#else
        m_doc_begin = reinterpret_cast<const char_type *>(m_code.c_str());
#endif
        m_first_char = m_doc_begin;
        m_last_char = m_doc_begin + m_code.size();
        m_line_start = m_first_char;
        m_cur_location = SourceLocation();
        m_last_location = SourceLocation();
        m_la_location = SourceLocation();
        m_lookahead.value = Token::T_EOS;
        next();
        nextToken();
    }
    
    void Lexer::inclinenumber()
    {
        int old = m_current;
        assert(currIsNewline());
        next();
        if(currIsNewline() && m_current != old)
            next();
        ++m_linenumber;
        m_line_start = m_first_char;
    }
    
    bool Lexer::check_next(const char * _set)
    {
        if(m_current == '\0' || !strchr(_set, m_current))
            return false;
        save_and_next();
        return true;
    }
    
    void Lexer::trydecpoint(__Token * sem)
    {
        char_type old = m_dec_point;
#ifndef LUAAST_QT
        m_dec_point = char_type(getlocaledecpoint());
        buffreplace(&m_saved, old, m_dec_point);
        if(!buff2d(m_saved, &sem->current_number)) {
            buffreplace(&m_saved, m_dec_point, '.');
            if(m_doc) {
                m_doc->addError(LuaError(LuaError::LexError, m_linenumber, m_first_char - m_line_start, "malformed number", T_NUMBER));
            }
        }
#else
        m_dec_point = QLocale::system().decimalPoint();
        QString str2 = m_saved;
        str2.replace(old, m_dec_point);
        bool ok = false;
        sem->current_number = QLocale::system().toDouble(str2, &ok);
        if(!ok) {
            str2.replace(m_dec_point, QChar('.'));
            sem->current_number = str2.toDouble(&ok);
            if(!ok) {
                if(m_doc) {
                    m_doc->addError(LuaError(LuaError::LexError, m_linenumber, m_first_char - m_line_start, QLatin1Literal("malformed number"), T_NUMBER));
                }
            }
        }
#endif
    }
    
    void Lexer::read_numeral(__Token * sem)
    {
        assert(isdigit(m_current));
        do {
            save_and_next();
            if(check_next("EePp"))
                check_next("+-");
        } while (isalnum(m_current) || m_current == '.');

#ifdef LUAAST_QT
        QString token = m_saved;
        token.replace(QChar('.'), m_dec_point);
        bool ok = false;
        sem->current_number = QLocale::system().toDouble(token, &ok);
        if(!ok)
            trydecpoint(sem);
#else
        buffreplace(&m_saved, '.', m_dec_point);
        if(!buff2d(m_saved, &sem->current_number))
            trydecpoint(sem);
#endif
    }
    
    int Lexer::skip_sep()
    {
        int count = 0;
        int s = m_current;
        assert(s == '[' || s ==']');
        save_and_next();
        while(m_current == '=') {
            save_and_next();
            ++count;
        }
        return m_current == s ? count : -(count) - 1;
    }
    
    void Lexer::read_long_string(int sep, bool comment, __Token * sem)
    {
        save_and_next();
        if(currIsNewline())
            inclinenumber();
        for(;;) {
            switch(m_current)
            {
                case 0:
                    if(m_doc)
                        m_doc->addError(LuaError(LuaError::LexError,
                                                 m_linenumber,
                                                 m_first_char - m_line_start,
                                                 comment ? "unfinished long comment" : "unfinished long string",
                                                 T_EOS));
                    return;
                case ']':
                    if(skip_sep() == sep) {
                        save_and_next();
                        if(!comment && sem) {
                            if(m_saved.length() >= 2 * (2 + sep)) {
#ifdef LUAAST_QT
                                sem->current_string = m_saved.mid(2 + sep, m_saved.length() - 2 * (2 + sep));
#else
                                sem->current_string = m_saved.substr(2 + sep, m_saved.length() - 2 * (2 + sep));
#endif
                            } else {
                                sem->current_string.clear();
                            }
                        }
                        return;
                    }
                    break;
                case '\n': case '\r':
                    save('\n');
                    inclinenumber();
                    if(comment)
                        m_saved.resize(0);
                    break;
                default:
                    if(!comment)
                        save_and_next();
                    else
                        next();
            }
        }
    }
    
    void Lexer::escerror(int *c, int n, const String& msg)
    {
        m_saved.resize(0);
        save('\\');
        for (int i = 0; i < n && c[i] != '\0'; i++)
            save(c[i]);
        if(m_doc) {
            m_doc->addError(LuaError(LuaError::LexError,
                                     m_linenumber,
                                     m_first_char - m_line_start,
                                     String(msg) + ": " + m_saved,
                                     T_STRING));
        }
    }
    
    int Lexer::readhexaesc()
    {
        int c[3], i;  /* keep input for error message */
        int r = 0;  /* result accumulator */
        c[0] = 'x';  /* for error message */
        for (i = 1; i < 3; i++) {  /* read two hexa digits */
            c[i] = next();
            if (!isxdigit(c[i])) {
                escerror(c, i + 1, "hexadecimal digit expected");
                return '?';
            }
            r = (r << 4) + luaO_hexavalue(c[i]);
        }
        return r;
    }
    
    int Lexer::readdecesc()
    {
        int c[3], i;
        int r = 0;  /* result accumulator */
        for (i = 0; i < 3 && isdigit(m_current); i++) {  /* read up to 3 digits */
            c[i] = m_current;
            r = 10*r + c[i] - '0';
            next();
        }
        if (r > UCHAR_MAX) {
            escerror(c, i, "decimal escape too large");
            return '?';
        }
        return r;
    }
    
    void Lexer::read_string(int del, __Token * sem)
    {
        save_and_next();  /* keep delimiter (for error messages) */
        while (m_current != del) {
            switch (m_current) {
                case '\0':
                    if(m_doc)
                        m_doc->addError(LuaError(LuaError::LexError,
                                                 m_linenumber,
                                                 m_first_char - m_line_start,
                                                 "unfinished string", T_EOS));
                    return;  /* to avoid warnings */
                case '\n':
                case '\r':
                    if(m_doc)
                        m_doc->addError(LuaError(LuaError::LexError,
                                                 m_linenumber,
                                                 m_first_char - m_line_start,
                                                 "unfinished string", T_STRING));
                    return;  /* to avoid warnings */
                case '\\': {  /* escape sequences */
                    int c;  /* final character to be saved */
                    next();  /* do not save the `\' */
                    switch (m_current) {
                        case 'a': c = '\a'; goto read_save;
                        case 'b': c = '\b'; goto read_save;
                        case 'f': c = '\f'; goto read_save;
                        case 'n': c = '\n'; goto read_save;
                        case 'r': c = '\r'; goto read_save;
                        case 't': c = '\t'; goto read_save;
                        case 'v': c = '\v'; goto read_save;
                        case 'x': c = readhexaesc(); goto read_save;
                        case '\n': case '\r':
                            inclinenumber(); c = '\n'; goto only_save;
                        case '\\': case '\"': case '\'':
                            c = m_current; goto read_save;
                        case '\0': goto no_save;  /* will raise an error next loop */
                        case 'z': {  /* zap following span of spaces */
                            next();  /* skip the 'z' */
                            while (isspace(m_current)) {
                                if (currIsNewline()) inclinenumber();
                                else next();
                            }
                            goto no_save;
                        }
                        default: {
                            if (!isdigit(m_current)) {
                                escerror(&m_current, 1, "invalid escape sequence");
                                goto no_save;
                            }
                            /* digital escape \ddd */
                            c = readdecesc();
                            goto only_save;
                        }
                    }
                read_save: next();  /* read next character */
                only_save: save(c);  /* save 'c' */
                no_save: break;
                }
                default:
                    save_and_next();
                    break;
            }
        }
        save_and_next();  /* skip delimiter */
        
        if(sem) {
#ifdef LUAAST_QT
            sem->current_string = m_saved.length() >= 2 ? m_saved.mid(1, m_saved.length() - 2) : QString();
#else
            sem->current_string = m_saved.length() >= 2 ? m_saved.substr(1, m_saved.length() - 2) : std::string();
#endif
        }
    }
    
    int Lexer::llex(__Token * tok)
    {
        for(;;) {
            SourceLocation * loc = &tok->loc;
            loc->startLine = m_linenumber;
            loc->startColumn = (uint)(m_first_char - m_line_start);
            loc->offset = m_first_char - m_doc_begin - 1;
            m_saved.resize(0);
            const char_type * token_start = m_first_char;
            
            switch(m_current) {
                case '\n': case '\r': {  /* line breaks */
                    inclinenumber();
                    break;
                }
                case ' ': case '\f': case '\t': case '\v': {  /* spaces */
                    next();
                    break;
                }
                case '-': {  /* '-' or '--' (comment) */
                    next();
                    if(m_current != '-') {
                        loc->length = 1;
                        return '-';
                    }
                    next();
                    if (m_current == '[') {  /* long comment? */
                        int sep = skip_sep();
                        m_saved.resize(0);
                        if(sep >= 0) {
                            read_long_string(sep, true, NULL);
                            if(m_doc)
                                m_doc->addComment(SourceLocation(token_start - m_doc_begin - 1,
                                                                 m_first_char - token_start + 1,
                                                                 loc->startLine,
                                                                 loc->startColumn));
                            
                            m_saved.resize(0);
                            break;
                        }
                    }
                    /* else short comment */
                    while (!currIsNewline() && m_current != '\0')
                        next();
                    if(m_doc)
                        m_doc->addComment(SourceLocation(token_start - m_doc_begin - 1,
                                                     m_first_char - token_start,
                                                     loc->startLine,
                                                     loc->startColumn));
                    break;
                }
                case '[': {  /* long string or simply '[' */
                    int sep = skip_sep();
                    if (sep >= 0) {
                        read_long_string(sep, false, tok);
                        loc->length = m_first_char - token_start;
                        return T_STRING;
                    }
                    else if(sep == -1) { loc->length = 1; return '['; }
                    else {
                        if(m_doc)
                            m_doc->addError(LuaError(LuaError::LexError,
                                                     loc->startLine,
                                                     loc->startColumn,
                                                     "invalid long string delimiter",
                                                     T_STRING));
                    }
                    break;
                }
                case '=': {
                    next();
                    loc->length = 1;
                    if(m_current != '=')
                        return '=';
                    else { next(); loc->length = 2; return T_EQ; }
                }
                case '<': {
                    next();
                    loc->length = 1;
                    if(m_current != '=')
                        return '<';
                    else { next(); loc->length = 2; return T_LE; }
                }
                case '>': {
                    next();
                    loc->length = 1;
                    if(m_current != '=')
                        return '>';
                    else { next(); loc->length = 2; return T_GE; }
                }
                case '~': {
                    next();
                    loc->length = 1;
                    if(m_current != '=')
                        return '~';
                    else { next(); loc->length = 2; return T_NE; }
                }
                case ':': {
                    next();
                    loc->length = 1;
                    if(m_current != ':')
                        return ':';
                    else { next(); loc->length = 2; return T_DBCOLON; }
                }
                case '"': case '\'': {  /* short literal strings */
                    read_string(m_current, tok);
                    loc->length = m_first_char - token_start;
                    return T_STRING;
                }
                case '.': {  /* '.', '..', '...', or number */
                    save_and_next();
                    if(check_next(".")) {
                        if(check_next(".")) {
                            loc->length = 3;
                            return T_DOTS;
                        }
                        loc->length = 2;
                        return T_CONCAT;
                    }
                    else if (!isdigit(m_current)) { loc->length = 1; return '.'; }
                    /* else go through */
                }
                case '0': case '1': case '2': case '3': case '4':
                case '5': case '6': case '7': case '8': case '9': {
                    read_numeral(tok);
                    loc->length = m_first_char - token_start;
                    return T_NUMBER;
                }
                case '\0':
                    loc->length = 0;
                    return T_EOS;
                default:
                    if(isalpha(m_current) || m_current == '_') {
                        do {
                            save_and_next();
                        } while(isalnum(m_current) || m_current == '_');
                        tok->current_string = m_saved;
                        loc->length = m_saved.size();
#ifdef LUAAST_QT
                        return name_to_token(tok->current_string.constData(), tok->current_string.size());
#else
                        return name_to_token(tok->current_string.c_str(), tok->current_string.size());
#endif
                    } else {
                        int c = m_current;
                        next();
                        loc->length = 1;
                        return c;
                    }
            }
        }
    }
    
    void Lexer::nextToken()
    {
        m_lastline = m_linenumber;
        m_last_location = m_cur_location;
        if(m_lookahead.value != T_EOS) {
            m_token = m_lookahead;
            m_lookahead.value = T_EOS;
        } else {
            m_token.value = llex(&m_token);
        }
    }
    
    int Lexer::lookAhead()
    {
        assert(m_lookahead.value == T_EOS);
        m_lookahead.value = llex(&m_lookahead);
        return m_lookahead.value;
    }
    
}

// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#ifndef __libluaast__LuaLexer__
#define __libluaast__LuaLexer__

#include "LuaToken.h"
#include "LuaError.h"
#include "LuaASTFwd.h"
#ifdef LUAAST_QT
#include <QString>
#include <QList>
#else
#include <string>
#include <vector>
#include <functional>
#endif

namespace LuaAST {
    
    class LuaDocument;
    
    class Lexer : Token
    {
    public:
        Lexer(LuaDocument * doc);
        ~Lexer();
        
        void setCode(const String& code);
        const String& code() const { return m_code; }
        
        void nextToken();
        int lookAhead();
        
        const SourceLocation& currentLocation() const { return m_token.loc; }
        const SourceLocation& lookAheadLocation() const { return m_lookahead.loc; }
        int token() const { return m_token.value; }
        lua_Number token_value() const { return m_token.current_number; }
        const String& token_string() const { return m_token.current_string; }

    private:
        struct __Token {
            SourceLocation loc;
            lua_Number current_number;
            String current_string;
            int value;
        };
        
        LuaDocument * m_doc;
        String m_code;
        int m_current;
        int m_linenumber;
        int m_lastline;
        __Token m_token;
        __Token m_lookahead;
        String m_saved;
        const char_type * m_doc_begin;
        const char_type * m_first_char;
        const char_type * m_last_char;
        const char_type * m_line_start;
        char_type m_dec_point;

        SourceLocation m_cur_location;
        SourceLocation m_last_location;
        SourceLocation m_la_location;
        
        void inclinenumber();
        bool check_next(const char * _set);
        void read_numeral(__Token * sem);
        void trydecpoint(__Token * sem);
        int skip_sep();
        void read_long_string(int sep, bool comment, __Token * sem);
        void escerror(int *c, int n, const String& msg);
        int readhexaesc();
        int readdecesc();
        void read_string(int del, __Token * sem);
        int llex(__Token * loc);
        
        inline int zgetc() {
            if(m_first_char == m_last_char)
                return 0;
#ifdef LUAAST_QT
            QChar c = *m_first_char++;
            return c.unicode();
#else
            return *m_first_char++;
#endif
        }
        
        inline int next() {
            m_current = zgetc();
            return m_current;
        }
        
        inline bool currIsNewline() const {
#ifdef LUAAST_QT
            return m_current == '\r' || m_current == '\n';
#else
            return m_current == '\r' || m_current == '\n';
#endif
        }
        
        inline void save(int c) {
            m_saved.push_back(c);
        }
        
        inline int save_and_next() {
            save(m_current);
            return next();
        }
    };
    
}

#endif /* defined(__libluaast__LuaLexer__) */

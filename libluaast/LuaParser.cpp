// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

/******************************************************************************
 * Copyright (C) 1994-2011 Lua.org, PUC-Rio.  All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

// #define _DEBUG_PARSER 1

#include "LuaParser.h"
#include "LuaDocument.h"
#include "LuaAST.h"
#ifdef _DEBUG_PARSER
#include <iostream>
#endif

namespace LuaAST {
    
#ifdef _DEBUG_PARSER
#define PARSER_DEBUG() \
    _parser_debug_logger __logger__(__PRETTY_FUNCTION__, m_lexer, m_doc)
    
    class _parser_debug_logger {
        const char * m_func;
        Lexer * m_lexer;
        LuaDocument * m_doc;
    public:
        _parser_debug_logger(const char * func, Lexer * lexer, LuaDocument * doc) :
            m_func(func),
            m_lexer(lexer),
            m_doc(doc)
        {
            std::cout << "--> Entering " << func << " because of token " << lexer->token() << " (" <<
                Token::name(lexer->token()) <<
                ") " << " at pos " << lexer->currentLocation().offset <<
                " and length " << lexer->currentLocation().length <<
                " with text '" << doc->midRef(lexer->currentLocation().offset, lexer->currentLocation().length).toString() <<
                "'" << std::endl;
        }
        
        ~_parser_debug_logger()
        {
            std::cout << "<-- Leaving " << m_func << " because of token " << m_lexer->token() << " (" <<
            Token::name(m_lexer->token()) <<
            ") " << " at pos " << m_lexer->currentLocation().offset <<
            " and length " << m_lexer->currentLocation().length <<
            " with text '" << m_doc->midRef(m_lexer->currentLocation().offset, m_lexer->currentLocation().length).toString() <<
            "'" << std::endl;
        }
    };
#else
#define PARSER_DEBUG() do { } while(0)
#endif
    
    Parser::Parser(LuaDocument * doc, Lexer * lexer) :
        m_doc(doc),
        m_lexer(lexer)
    {
    }
    
    Parser::~Parser()
    {
    }
    
    void Parser::parse()
    {
        BlockAST * ast = new(m_doc->m_pool) BlockAST();
        ast->list = statementList();
        m_doc->m_ast = ast;
        check(Token::T_EOS);
    }
    
    void Parser::errorExpected(int token)
    {
        const SourceLocation& loc(m_lexer->currentLocation());
        
        m_doc->addError(LuaError(LuaError::ParseError,
                                 loc.startLine,
                                 loc.startColumn,
                                 String(Literal("Expected token: ")) +
                                 Token::name(token), token));
    }
    
    bool Parser::check(int c)
    {
        if(m_lexer->token() != c) {
            errorExpected(c);
            return false;
        }
        
        return true;
    }
    
    void Parser::syntaxError(const String& msg)
    {
        const SourceLocation& loc(m_lexer->currentLocation());
        m_doc->addError(LuaError(LuaError::ParseError, loc.startLine, loc.startColumn, msg, m_lexer->token()));
    }
    
    bool Parser::testNext(int token)
    {
        if(m_lexer->token() == token) {
            m_lexer->nextToken();
            return true;
        } else return false;
    }
    
    bool Parser::checkNext(int c)
    {
        bool ok = check(c);
        m_lexer->nextToken();
        return ok;
    }
    
    bool Parser::checkMatch(int what, int who, int where)
    {
        if(!testNext(what)) {
            if(where == m_lexer->currentLocation().startLine)
                errorExpected(what);
            else {
                String msg;
                const SourceLocation& loc(m_lexer->currentLocation());
                msg.append(Token::name(what));
                msg.append(Literal(" expected (to close "));
                msg.append(Token::name(who));
                msg.append(Literal(" at line "));
#ifdef LUAAST_QT
                msg.append(QString::number(where));
#else
                char line[64];
                sprintf(line, "%d", where);
                msg.append(line);
#endif
                msg.append(Literal(")"));
                m_doc->addError(LuaError(LuaError::ParseError, loc.startLine, loc.startColumn, msg, m_lexer->token()));
            }
            return false;
        }
        return true;
    }
    
    bool Parser::blockFollow(bool withuntil)
    {
        switch(m_lexer->token())
        {
            case Token::T_ELSE:
            case Token::T_ELSEIF:
            case Token::T_END:
            case Token::T_EOS:
                return true;
            case Token::T_UNTIL:
                return withuntil;
            default:
                return false;
        }
    }
    
    template<typename _Tp, typename _U = List<_Tp> > class ListBuildHelper {
        _U * m_prev;
        _U * m_first;
        MemoryPool * m_pool;
    public:
        ListBuildHelper(MemoryPool * pool) :
            m_prev(NULL),
            m_first(NULL),
            m_pool(pool)
        {
        }
        
        _U * list() const {
            return m_first;
        }
        
        void append(_Tp t) {
            if(t) {
                _U * node = new(m_pool) _U(t);
                if(m_prev)
                    m_prev->next = node;
                if(m_first == NULL)
                    m_first = node;
                m_prev = node;
            }
        }
    };
    
    StatementListAST * Parser::statementList(int delimiting_token, int delimiting_token2)
    {
        ListBuildHelper<StatementAST *> builder(m_doc->m_pool);
        
        /* statlist -> { stat [`;'] } */
        while(!blockFollow(true)) {
            if(m_lexer->token() == Token::T_RETURN) {
                builder.append(statement());
                break;
            }
            builder.append(statement());
        }
        return builder.list();
    }
    
    StatementAST * Parser::statement()
    {
        PARSER_DEBUG();
        
        switch(m_lexer->token())
        {
            case ';': {
                EmptyStatementAST * stmt = new(m_doc->m_pool) EmptyStatementAST();
                stmt->semicolon_token = m_lexer->currentLocation();
                m_lexer->nextToken();
                return stmt;
            }
            case Token::T_IF:
                return ifStat();
            case Token::T_WHILE:
                return whileStat();
            case Token::T_DO:
                return doStat();
            case Token::T_FOR:
                return forStat();
            case Token::T_REPEAT:
                return repeatStat();
            case Token::T_FUNCTION:
                return funcStat();
            case Token::T_LOCAL: {
                SourceLocation localToken(m_lexer->currentLocation());
                m_lexer->nextToken();
                SourceLocation functionToken(m_lexer->currentLocation());
                if(testNext(Token::T_FUNCTION))
                    return localFunc(localToken, functionToken);
                return localStat(localToken);
            }
            case Token::T_DBCOLON:
                return labelStat();
            case Token::T_RETURN:
                return returnStat();
            case Token::T_BREAK:
                return breakStat();
            case Token::T_GOTO:
                return gotoStat();
            default:
                return exprStat();
        }
    }
    
    IfStatementAST * Parser::ifStat()
    {
        PARSER_DEBUG();
        
        uint line = m_lexer->currentLocation().startLine;
        IfStatementAST * stmt = new(m_doc->m_pool) IfStatementAST();
        stmt->if_token = m_lexer->currentLocation();
        m_lexer->nextToken();
        stmt->cond = expr();
        if(!stmt->cond)
            return NULL;
        stmt->then_token = m_lexer->currentLocation();
        if(!checkNext(Token::T_THEN))
            return NULL;
        stmt->block = block(Token::T_ELSEIF, Token::T_END);
        if(!stmt->block)
            return NULL;
        
        ListBuildHelper<IfStatementAST *> builder(m_doc->m_pool);
        
        
        while(m_lexer->token() == Token::T_ELSEIF) {
            IfStatementAST * else_stmt = new(m_doc->m_pool) IfStatementAST();
            else_stmt->if_token = m_lexer->currentLocation();
            m_lexer->nextToken();
            else_stmt->cond = expr();
            if(!else_stmt->cond)
                return NULL;
            else_stmt->then_token = m_lexer->currentLocation();
            if(!checkNext(Token::T_THEN))
                return NULL;
            else_stmt->block = block(Token::T_ELSEIF, Token::T_END);
            if(!else_stmt->block)
                return NULL;
            builder.append(else_stmt);
        }
        
        stmt->elseif_list = builder.list();
        
        if(m_lexer->token() == Token::T_ELSE) {
            stmt->else_token = m_lexer->currentLocation();
            m_lexer->nextToken();
            stmt->block = block(Token::T_END);
            if(!stmt->block)
                return NULL;
        }
        stmt->end_token = m_lexer->currentLocation();
        if(!checkMatch(Token::T_END, Token::T_IF, line))
            return NULL;
        return stmt;
    }
    
    WhileStatementAST * Parser::whileStat()
    {
        PARSER_DEBUG();
        
        /* whilestat -> WHILE cond DO block END */
        uint line = m_lexer->currentLocation().startLine;
        WhileStatementAST * stmt = new(m_doc->m_pool) WhileStatementAST();
        stmt->while_token = m_lexer->currentLocation();
        m_lexer->nextToken();
        stmt->cond = expr();
        if(!stmt->cond)
            return NULL;
        stmt->do_token = m_lexer->currentLocation();
        if(!checkNext(Token::T_DO))
            return NULL;
        stmt->block = block(Token::T_END);
        if(!stmt->block)
            return NULL;
        stmt->end_token = m_lexer->currentLocation();
        if(!checkMatch(Token::T_END, Token::T_WHILE, line))
            return NULL;
        return stmt;
    }
    
    DoStatementAST * Parser::doStat()
    {
        PARSER_DEBUG();
        
        /* stat -> DO block END */
        uint line = m_lexer->currentLocation().startLine;
        DoStatementAST * stmt = new(m_doc->m_pool) DoStatementAST();
        stmt->do_token = m_lexer->currentLocation();
        m_lexer->nextToken();
        stmt->block = block(Token::T_END);
        if(!stmt->block)
            return NULL;
        stmt->end_token = m_lexer->currentLocation();
        if(!checkMatch(Token::T_END, Token::T_DO, line))
            return NULL;
        return stmt;
    }
    
    StatementAST * Parser::forStat()
    {
        PARSER_DEBUG();
        
        /* forstat -> FOR (fornum | forlist) END */
        SourceLocation for_token(m_lexer->currentLocation());
        m_lexer->nextToken();
        SourceLocation first_var_token(m_lexer->currentLocation());
        if(!checkNext(Token::T_NAME))
            return NULL;
        if(m_lexer->token() == '=')
            return forNumStat(for_token, first_var_token);
        if(m_lexer->token() == ',' || m_lexer->token() == Token::T_IN)
            return forListStat(for_token, first_var_token);
        syntaxError("`=' or `in' expected");
        m_lexer->nextToken();
        return NULL;
    }
    
    IdentifierAST * Parser::nameFromToken(const SourceLocation& loc)
    {
        PARSER_DEBUG();
        
        IdentifierAST * node = new(m_doc->m_pool) IdentifierAST();
        node->identifier_token = loc;
        return node;
    }
    
    ForNumStatementAST * Parser::forNumStat(const SourceLocation& forToken, const SourceLocation& var1Token)
    {
        PARSER_DEBUG();
        
        /* fornum -> NAME = exp1,exp1[,exp1] forbody */
        ForNumStatementAST * stmt = new(m_doc->m_pool) ForNumStatementAST();
        stmt->for_token = forToken;
        stmt->name = nameFromToken(var1Token);
        stmt->equal_token = m_lexer->currentLocation();
        m_lexer->nextToken();
        stmt->index = expr();
        if(!stmt->index)
            return NULL;
        stmt->coma1 = m_lexer->currentLocation();
        if(!checkNext(','))
            return NULL;
        stmt->limit = expr();
        if(!stmt->limit)
            return NULL;
        stmt->coma2 = m_lexer->currentLocation();
        if(testNext(',')) {
            stmt->step = expr();
            if(!stmt->step)
                return NULL;
        } else {
            stmt->coma2 = SourceLocation();
        }
        stmt->do_token = m_lexer->currentLocation();
        if(!checkNext(Token::T_DO))
            return NULL;
        stmt->block = block(Token::T_END);
        if(!stmt->block)
            return NULL;
        stmt->end_token = m_lexer->currentLocation();
        if(!checkNext(Token::T_END))
            return NULL;
        return stmt;
    }
    
    ForListStatementAST * Parser::forListStat(const SourceLocation& forToken, const SourceLocation& var1Token)
    {
        PARSER_DEBUG();
        
        /* forlist -> NAME {,NAME} IN explist forbody */
        ForListStatementAST * stmt = new(m_doc->m_pool) ForListStatementAST();
        stmt->for_token = forToken;
        stmt->forlist = comaSeparatedLocalVars(var1Token);
        if(!stmt->forlist)
            return NULL;
        stmt->in_token = m_lexer->currentLocation();
        if(!checkNext(Token::T_IN))
            return NULL;
        stmt->explist = comaSeparatedExprs();
        if(!stmt->explist)
            return NULL;
        stmt->do_token = m_lexer->currentLocation();
        if(!checkNext(Token::T_DO))
            return NULL;
        stmt->block = block(Token::T_END);
        if(!stmt->block)
            return NULL;
        stmt->end_token = m_lexer->currentLocation();
        if(!checkNext(Token::T_END))
            return NULL;
        return stmt;
    }
    
    ComaSeparatedIdentifierListAST * Parser::comaSeparatedLocalVars(const SourceLocation& var1Token)
    {
        PARSER_DEBUG();
        
        ListBuildHelper<ComaSeparatedIdentifierAST *> builder(m_doc->m_pool);
        ComaSeparatedIdentifierAST * ast = new(m_doc->m_pool) ComaSeparatedIdentifierAST();
        ast->identifier = nameFromToken(var1Token);
        while(m_lexer->token() == ',') {
            builder.append(ast);
            ast->coma_token = m_lexer->currentLocation();
            m_lexer->nextToken();
            if(!check(Token::T_NAME))
                return NULL;
            ast = new(m_doc->m_pool) ComaSeparatedIdentifierAST();
            ast->identifier = nameFromToken(m_lexer->currentLocation());
            m_lexer->nextToken();
        }
        builder.append(ast);
        return builder.list();
    }
    
    ComaSeparatedIdentifierListAST * Parser::comaSeparatedIdentifierList()
    {
        PARSER_DEBUG();
        
        SourceLocation loc(m_lexer->currentLocation());
        if(!checkNext(Token::T_NAME))
            return NULL;
        return comaSeparatedLocalVars(loc);
    }
    
    ComaSeparatedExpressionListAST * Parser::comaSeparatedExprs()
    {
        PARSER_DEBUG();
        
        ListBuildHelper<ComaSeparatedExpressionAST *> builder(m_doc->m_pool);
        ComaSeparatedExpressionAST * ast = new(m_doc->m_pool) ComaSeparatedExpressionAST();
        ast->expr = expr();
        if(!ast->expr)
            return NULL;
        while(m_lexer->token() == ',') {
            builder.append(ast);
            ast->coma_token = m_lexer->currentLocation();
            m_lexer->nextToken();
            ast = new(m_doc->m_pool) ComaSeparatedExpressionAST();
            ast->expr = expr();
            if(!ast->expr)
                return NULL;
        }
        builder.append(ast);
        return builder.list();
    }
    
    ComaSeparatedExpressionListAST * Parser::parameterList()
    {
        PARSER_DEBUG();
        
        ListBuildHelper<ComaSeparatedExpressionAST *> builder(m_doc->m_pool);
        if(m_lexer->token() != ')') {
            for(;;) {
                ComaSeparatedExpressionAST * ast = new(m_doc->m_pool) ComaSeparatedExpressionAST();
                switch(m_lexer->token())
                {
                    case Token::T_NAME: {
                        ast->expr = nameFromToken(m_lexer->currentLocation());
                        builder.append(ast);
                        m_lexer->nextToken();
                        if(m_lexer->token() != ',')
                            return builder.list();
                        ast->coma_token = m_lexer->currentLocation();
                        m_lexer->nextToken();
                        break;
                    }
                    case Token::T_DOTS: {
                        DotsAST * dots = new(m_doc->m_pool) DotsAST();
                        dots->dots_token = m_lexer->currentLocation();
                        ast->expr = dots;
                        m_lexer->nextToken();
                        builder.append(ast);
                        return builder.list();
                    }
                    default: {
                        syntaxError("Expected <name> or `...'");
                        m_lexer->nextToken();
                        return NULL;
                    }
                }
            }
        }
        return builder.list();
    }
    
    /* repeatstat -> REPEAT block UNTIL cond */
    RepeatStatementAST * Parser::repeatStat()
    {
        PARSER_DEBUG();
        
        RepeatStatementAST * stmt = new(m_doc->m_pool) RepeatStatementAST();
        stmt->repeat_token = m_lexer->currentLocation();
        m_lexer->nextToken();
        stmt->block = block(Token::T_UNTIL);
        if(!stmt->block)
            return NULL;
        stmt->until_token = m_lexer->currentLocation();
        if(!checkNext(Token::T_UNTIL))
            return NULL;
        stmt->cond = expr();
        if(!stmt->cond)
            return NULL;
        return stmt;
    }
    
    FunctionStatementAST * Parser::funcStat()
    {
        FunctionStatementAST * stmt = new(m_doc->m_pool) FunctionStatementAST();
        stmt->function_token = m_lexer->currentLocation();
        m_lexer->nextToken();
        stmt->name = funcName();
        if(!stmt->name)
            return NULL;
        stmt->body = funcBody();
        if(!stmt->body)
            return NULL;
        return stmt;
    }
    
    /* stat -> LOCAL NAME {`,' NAME} [`=' explist] */
    LocalStatementAST * Parser::localStat(const SourceLocation& localToken)
    {
        PARSER_DEBUG();
        
        LocalStatementAST * stmt = new(m_doc->m_pool) LocalStatementAST();
        stmt->local_token = localToken;
        stmt->names = comaSeparatedIdentifierList();
        if(!stmt->names)
            return NULL;
        stmt->equal_token = m_lexer->currentLocation();
        if(testNext('=')) {
            stmt->explist = comaSeparatedExprs();
            if(!stmt->explist)
                return NULL;
        } else {
            stmt->equal_token = SourceLocation();
        }
        return stmt;
    }
    
    LocalFunctionStatementAST * Parser::localFunc(const SourceLocation& localToken, const SourceLocation& functionToken)
    {
        PARSER_DEBUG();
        
        LocalFunctionStatementAST * stmt = new(m_doc->m_pool) LocalFunctionStatementAST();
        stmt->local_token = localToken;
        stmt->function_token = functionToken;
        SourceLocation name_loc(m_lexer->currentLocation());
        if(!checkNext(Token::T_NAME))
            return NULL;
        stmt->name = nameFromToken(name_loc);
        stmt->body = funcBody();
        if(!stmt->body)
            return NULL;
        return stmt;
    }
    
    LabelStatementAST * Parser::labelStat()
    {
        PARSER_DEBUG();
        
        LabelStatementAST * stmt = new(m_doc->m_pool) LabelStatementAST();
        stmt->ldb_token = m_lexer->currentLocation();
        m_lexer->nextToken();
        SourceLocation name_loc(m_lexer->currentLocation());
        if(!checkNext(Token::T_NAME))
            return NULL;
        stmt->name = nameFromToken(name_loc);
        stmt->rdb_token = m_lexer->currentLocation();
        if(!checkMatch(Token::T_DBCOLON, Token::T_DBCOLON, stmt->ldb_token.startLine))
            return NULL;
        return stmt;
    }
    
    ReturnStatementAST * Parser::returnStat()
    {
        PARSER_DEBUG();
        
        ReturnStatementAST * stmt = new(m_doc->m_pool) ReturnStatementAST();
        stmt->return_token = m_lexer->currentLocation();
        m_lexer->nextToken();
        if(blockFollow(true))
            return stmt;
        if(m_lexer->token() == ';') {
            stmt->return_token = m_lexer->currentLocation();
            m_lexer->nextToken();
            return stmt;
        }
        stmt->explist = comaSeparatedExprs();
        if(!stmt->explist)
            return NULL;
        if(m_lexer->token() == ';') {
            stmt->return_token = m_lexer->currentLocation();
            m_lexer->nextToken();
        }
        return stmt;
    }
    
    BreakStatementAST * Parser::breakStat()
    {
        PARSER_DEBUG();
        
        BreakStatementAST * stmt = new(m_doc->m_pool) BreakStatementAST();
        stmt->break_token = m_lexer->currentLocation();
        m_lexer->nextToken();
        return stmt;
    }
    
    GotoStatementAST * Parser::gotoStat()
    {
        PARSER_DEBUG();
        
        GotoStatementAST * stmt = new(m_doc->m_pool) GotoStatementAST();
        stmt->goto_token = m_lexer->currentLocation();
        m_lexer->nextToken();
        SourceLocation name_loc(m_lexer->currentLocation());
        if(!checkNext(Token::T_NAME))
            return NULL;
        stmt->label = nameFromToken(name_loc);
        return stmt;
    }
    
    BlockAST * Parser::block(int delimiting_token, int delimiting_token2)
    {
        PARSER_DEBUG();
        
        BlockAST * stmt = new(m_doc->m_pool) BlockAST();
        stmt->list = statementList(delimiting_token, delimiting_token2);
        return stmt;
    }
    
    ExpressionAST * Parser::expr()
    {
        PARSER_DEBUG();
        return subExpr(0);
    }
    
    /* funcname -> NAME {fieldsel} [`:' NAME] */
    FunctionNameAST * Parser::funcName()
    {
        PARSER_DEBUG();
        
        FunctionNameAST * ast = new(m_doc->m_pool) FunctionNameAST();
        ast->name0 = nameFromToken(m_lexer->currentLocation());
        if(!checkNext(Token::T_NAME))
            return NULL;
        ListBuildHelper<FieldSelectorAST *> helper(m_doc->m_pool);
        while(m_lexer->token() == '.') {
            FieldSelectorAST * sel = new(m_doc->m_pool) FieldSelectorAST();
            sel->selector_token = m_lexer->currentLocation();
            m_lexer->nextToken();
            sel->name = nameFromToken(m_lexer->currentLocation());
            if(!checkNext(Token::T_NAME))
                return NULL;
            helper.append(sel);
        }
        ast->fieldsel = helper.list();
        if(m_lexer->token() == ':') {
            ast->method_token = m_lexer->currentLocation();
            m_lexer->nextToken();
            ast->method_name = nameFromToken(m_lexer->currentLocation());
            if(!checkNext(Token::T_NAME))
                return NULL;
        }
        return ast;
    }
    
    /* body ->  `(' parlist `)' block END */
    FunctionBodyAST * Parser::funcBody()
    {
        PARSER_DEBUG();
        
        uint line = m_lexer->currentLocation().startLine;
        FunctionBodyAST * ast = new(m_doc->m_pool) FunctionBodyAST();
        ast->lparen_token = m_lexer->currentLocation();
        if(!checkNext('('))
            return NULL;
        ast->parameters = parameterList();
        ast->rparen_token = m_lexer->currentLocation();
        if(!checkNext(')'))
            return NULL;
        ast->block = block(Token::T_END);
        ast->end_token = m_lexer->currentLocation();
        if(!checkMatch(Token::T_END, Token::T_FUNCTION, line))
            return NULL;
        return ast;
    }
    
    static UnaryExpressionAST::UnOpr getunopr (int op) {
        switch (op) {
            case Token::T_NOT: return UnaryExpressionAST::OPR_NOT;
            case '-': return UnaryExpressionAST::OPR_MINUS;
            case '#': return UnaryExpressionAST::OPR_LEN;
            default: return UnaryExpressionAST::OPR_NOUNOPR;
        }
    }
    
    
    static BinaryExpressionAST::BinOpr getbinopr (int op) {
        switch (op) {
            case '+': return BinaryExpressionAST::OPR_ADD;
            case '-': return BinaryExpressionAST::OPR_SUB;
            case '*': return BinaryExpressionAST::OPR_MUL;
            case '/': return BinaryExpressionAST::OPR_DIV;
            case '%': return BinaryExpressionAST::OPR_MOD;
            case '^': return BinaryExpressionAST::OPR_POW;
            case Token::T_CONCAT: return BinaryExpressionAST::OPR_CONCAT;
            case Token::T_NE: return BinaryExpressionAST::OPR_NE;
            case Token::T_EQ: return BinaryExpressionAST::OPR_EQ;
            case '<': return BinaryExpressionAST::OPR_LT;
            case Token::T_LE: return BinaryExpressionAST::OPR_LE;
            case '>': return BinaryExpressionAST::OPR_GT;
            case Token::T_GE: return BinaryExpressionAST::OPR_GE;
            case Token::T_AND: return BinaryExpressionAST::OPR_AND;
            case Token::T_OR: return BinaryExpressionAST::OPR_OR;
            default: return BinaryExpressionAST::OPR_NOBINOPR;
        }
    }
    
    
    static const struct {
        uint8_t left;  /* left priority for each binary operator */
        uint8_t right; /* right priority */
    } priority[] = {  /* ORDER OPR */
        {6, 6}, {6, 6}, {7, 7}, {7, 7}, {7, 7},  /* `+' `-' `*' `/' `%' */
        {10, 9}, {5, 4},                 /* ^, .. (right associative) */
        {3, 3}, {3, 3}, {3, 3},          /* ==, <, <= */
        {3, 3}, {3, 3}, {3, 3},          /* ~=, >, >= */
        {2, 2}, {1, 1}                   /* and, or */
    };
    
#define UNARY_PRIORITY  8  /* priority for unary operators */

    ExpressionAST * Parser::subExpr(int limit, int * p_nextop)
    {
        PARSER_DEBUG();
        
        UnaryExpressionAST::UnOpr uop;
        BinaryExpressionAST::BinOpr op;
        
        ExpressionAST * lhs;
        
        uop = getunopr(m_lexer->token());
        if(uop != UnaryExpressionAST::OPR_NOUNOPR) {
            UnaryExpressionAST * unexp = new(m_doc->m_pool) UnaryExpressionAST();
            unexp->unop_token = m_lexer->currentLocation();
            unexp->op = uop;
            m_lexer->nextToken();
            unexp->subexpr = subExpr(UNARY_PRIORITY);
            if(!unexp->subexpr)
                return NULL;
            lhs = unexp;
        } else {
            lhs = simpleExp();
        }
        
        if(!lhs)
            return NULL;
        
        op = getbinopr(m_lexer->token());
        while(op != BinaryExpressionAST::OPR_NOBINOPR && priority[op].left > limit) {
            BinaryExpressionAST * binexp = new(m_doc->m_pool) BinaryExpressionAST();
            int nextop;
            binexp->lhs = lhs;
            binexp->binop_token = m_lexer->currentLocation();
            m_lexer->nextToken();
            binexp->op = op;
            binexp->rhs = subExpr(priority[op].right, &nextop);
            if(!binexp->rhs)
                return NULL;
            lhs = binexp;
            op = static_cast<BinaryExpressionAST::BinOpr>(nextop);
        }
        if(p_nextop)
            *p_nextop = op;
        return lhs;
    }
    
    StringAST * Parser::stringExpr()
    {
        PARSER_DEBUG();
        
        StringAST * ast = new(m_doc->m_pool) StringAST();
        
        SourceLocation loc(m_lexer->currentLocation());
        StringRef raw_ref(&m_doc->m_code, loc.offset, loc.length);
        
        ast->value = m_doc->newStringRef(m_lexer->token_string());
        ast->full_string_token = m_lexer->currentLocation();
        
        m_lexer->nextToken();

#ifdef LUAAST_QT
#define R_CHAR(x, y)    ((x).unicode()[(y)])
#else
#define R_CHAR(x, y)    ((x)[(y)])
#endif

        if(R_CHAR(raw_ref, 0) == '\'' || R_CHAR(raw_ref, 0) == '"') {
            ast->lquot_token.offset = loc.offset;
            ast->lquot_token.length = 1;
            ast->lquot_token.startColumn = loc.startColumn;
            ast->lquot_token.startLine = loc.startLine;
            loc.startColumn++;
            loc.offset++;
            loc.length--;
            
    
            if(R_CHAR(raw_ref, raw_ref.length() - 1) == R_CHAR(raw_ref, 0) && loc.length > 0) {
                ast->rquot_token.offset = ast->lquot_token.offset + loc.length;
                ast->rquot_token.length = 1;
                ast->rquot_token.startLine = ast->lquot_token.startLine;
                ast->rquot_token.startColumn = (uint)(ast->lquot_token.startColumn + loc.length);
            }
        } else if(R_CHAR(raw_ref, 0) == '[') {
            ast->lquot_token.offset = loc.offset;
            ast->lquot_token.length = 2;
            ast->lquot_token.startColumn = loc.startColumn;
            ast->lquot_token.startLine = loc.startLine;
            int sep = 0;
            while(1 + sep < raw_ref.length() && R_CHAR(raw_ref, 1 + sep) == '=')
                ++sep;

            ast->lquot_token.length += sep;
            loc.offset += 2 + sep;
            loc.length -= 2 + sep;
            loc.startColumn += 2 + sep;
            
            if(raw_ref.length() >= (2 * (2 + sep))) {
                ast->rquot_token.offset = ast->lquot_token.offset + loc.length;
                ast->rquot_token.length = 2 + sep;
            }
        }
        
        return ast;
    }
    
    ExpressionAST * Parser::simpleExp()
    {
        PARSER_DEBUG();
        
        /* simpleexp -> NUMBER | STRING | NIL | TRUE | FALSE | ... |
         constructor | FUNCTION body | primaryexp */
        switch(m_lexer->token())
        {
            case Token::T_NUMBER: {
                NumberAST * ast = new(m_doc->m_pool) NumberAST();
                ast->number_token = m_lexer->currentLocation();
                ast->value = m_lexer->token_value();
                m_lexer->nextToken();
                return ast;
            }
            case Token::T_STRING: {
                return stringExpr();
            }
            case Token::T_NIL: {
                NilAST * ast = new(m_doc->m_pool) NilAST();
                ast->nil_token = m_lexer->currentLocation();
                m_lexer->nextToken();
                return ast;
            }
            case Token::T_TRUE: case Token::T_FALSE: {
                BooleanAST * ast = new(m_doc->m_pool) BooleanAST();
                ast->bool_token = m_lexer->currentLocation();
                ast->value = Token::T_TRUE == m_lexer->token();
                m_lexer->nextToken();
                return ast;
            }
            case Token::T_DOTS: {
                DotsAST * dots = new(m_doc->m_pool) DotsAST();
                dots->dots_token = m_lexer->currentLocation();
                m_lexer->nextToken();
                return dots;
            }
            case '{':
                return constructorExpr();
            case Token::T_FUNCTION:
                return funcExpr();
            default:
                return primaryExpr();
        }
    }
    
    FunctionExpressionAST * Parser::funcExpr()
    {
        PARSER_DEBUG();
        
        FunctionExpressionAST * ast = new(m_doc->m_pool) FunctionExpressionAST();
        ast->function_token = m_lexer->currentLocation();
        m_lexer->nextToken();
        ast->body = funcBody();
        if(!ast->body)
            return NULL;
        return ast;
    }

    ConstructorAST * Parser::constructorExpr()
    {
        PARSER_DEBUG();
        
        ConstructorAST * ast = new(m_doc->m_pool) ConstructorAST();
        ast->lcurl_token = m_lexer->currentLocation();
        m_lexer->nextToken();
        ListBuildHelper<FieldAST *> builder(m_doc->m_pool);
        do {
            if(m_lexer->token() == '}')
                break;
            FieldAST * f = field();
            if(!f)
                return NULL;
            builder.append(f);
        } while(testNext(',') || testNext(';'));
        ast->rcurl_token = m_lexer->currentLocation();
        if(!checkMatch('}', '{', ast->lcurl_token.startLine))
            return NULL;
        ast->fields = builder.list();
        return ast;
    }
    
    FieldAST * Parser::field()
    {
        PARSER_DEBUG();
        
        switch(m_lexer->token())
        {
            case Token::T_NAME: {
                if(m_lexer->lookAhead() != '=')
                    return listField();
                return recField();
            }
            case '[':
                return recField();
            default:
                return listField();
        }
    }
    
    ListFieldAST * Parser::listField()
    {
        PARSER_DEBUG();
        
        /* listfield -> exp */
        ListFieldAST * field = new(m_doc->m_pool) ListFieldAST();
        field->expr = expr();
        if(!field->expr)
            return NULL;
        if(m_lexer->token() == ',' || m_lexer->token() == ';')
            field->separator_token = m_lexer->currentLocation();
        return field;
    }
    
    RecordFieldAST * Parser::recField()
    {
        PARSER_DEBUG();
        
        /* recfield -> (NAME | `['exp1`]') = exp1 */
        RecordFieldAST * field = new(m_doc->m_pool) RecordFieldAST();
        if(m_lexer->token() == Token::T_NAME) {
            field->selector = nameSelector();
        } else {
            field->selector = indexSelector();
        }
        
        if(!field->selector)
            return NULL;
        
        field->equal_token = m_lexer->currentLocation();
        if(!checkNext('='))
            return NULL;
        
        field->value = expr();
        if(!field->value)
            return NULL;
        
        if(m_lexer->token() == ',' || m_lexer->token() == ';')
            field->separator_token = m_lexer->currentLocation();
        
        return field;
    }
    
    NameSelectorAST * Parser::nameSelector()
    {
        PARSER_DEBUG();
        
        if(!check(Token::T_NAME))
            return NULL;
        NameSelectorAST * sel = new(m_doc->m_pool) NameSelectorAST();
        sel->name = nameFromToken(m_lexer->currentLocation());
        m_lexer->nextToken();
        return sel;
    }
    
    IndexSelectorAST * Parser::indexSelector()
    {
        PARSER_DEBUG();
        
        if(!check('['))
            return NULL;
        IndexSelectorAST * sel = new(m_doc->m_pool) IndexSelectorAST();
        sel->lsquare_token = m_lexer->currentLocation();
        m_lexer->nextToken();
        sel->expr = expr();
        if(!sel->expr)
            return NULL;
        sel->rsquare_token = m_lexer->currentLocation();
        if(!checkNext(']'))
            return NULL;
        return sel;
    }
    
    /* primaryexp ->
     prefixexp { `.' NAME | `[' exp `]' | `:' NAME funcargs | funcargs } */
    ExpressionAST * Parser::primaryExpr()
    {
        PARSER_DEBUG();
        
        ExpressionAST * lhs = prefixExpr();
        if(!lhs)
            return NULL;
        
        // Don't create SelectorExpressionAST instances if not necessary
        if(m_lexer->token() != '.' && m_lexer->token() != '[' && m_lexer->token() != ':' &&
           m_lexer->token() != '(' && m_lexer->token() != '{' && m_lexer->token() != Token::T_STRING)
            return lhs;
        
        SelectorExpressionAST * selExpr = new(m_doc->m_pool) SelectorExpressionAST();
        
        selExpr->prefixexp = lhs;
        
        ListBuildHelper<SelectorAST *> builder(m_doc->m_pool);
        
        for(;;) {
            switch(m_lexer->token())
            {
                case '.': {
                    FieldSelectorAST * ast = fieldSel();
                    if(!ast)
                        return NULL;
                    builder.append(ast);
                    break;
                }
                case '[': {
                    IndexSelectorAST * ast = indexSelector();
                    if(!ast)
                        return NULL;
                    builder.append(ast);
                    break;
                }
                case ':': {
                    MethodCallSelectorAST * ast = methodSel();
                    if(!ast)
                        return NULL;
                    builder.append(ast);
                    break;
                }
                case '(':
                case '{':
                case Token::T_STRING: {
                    FunctionCallSelectorAST * ast = new(m_doc->m_pool) FunctionCallSelectorAST();
                    ast->arguments = funcArgsExpr();
                    if(!ast->arguments)
                        return NULL;
                    builder.append(ast);
                    break;
                }
                default: {
                    selExpr->chain = builder.list();
                    return selExpr;
                }
            }
        }
    }
    
    /* fieldsel -> ['.' | ':'] NAME */
    FieldSelectorAST * Parser::fieldSel()
    {
        PARSER_DEBUG();
        
        FieldSelectorAST * ast = new(m_doc->m_pool) FieldSelectorAST();
        ast->selector_token = m_lexer->currentLocation();
        m_lexer->nextToken();
        ast->name = new(m_doc->m_pool) IdentifierAST();
        ast->name->identifier_token = m_lexer->currentLocation();
        if(!checkNext(Token::T_NAME))
            return NULL;
        return ast;
    }
    
    /* prefixexp -> NAME | '(' expr ')' */
    ExpressionAST * Parser::prefixExpr()
    {
        PARSER_DEBUG();
        
        switch (m_lexer->token()) {
            case '(': {
                uint line = m_lexer->currentLocation().startLine;
                BracketExpressionAST * ast = new(m_doc->m_pool) BracketExpressionAST();
                ast->lbracket_token = m_lexer->currentLocation();
                m_lexer->nextToken();
                ast->expr = expr();
                if(!ast->expr)
                    return NULL;
                ast->rbracket_token = m_lexer->currentLocation();
                if(!checkMatch(')', '(', line))
                    return NULL;
                return ast;
                }
            case Token::T_NAME: {
                IdentifierAST * ast = new(m_doc->m_pool) IdentifierAST();
                ast->identifier_token = m_lexer->currentLocation();
                m_lexer->nextToken();
                return ast;
            }
            default:
                syntaxError("unexpected symbol in expression");
                m_lexer->nextToken();
                break;
        }
        return NULL;
    }
    
    MethodCallSelectorAST * Parser::methodSel()
    {
        PARSER_DEBUG();
        
        MethodCallSelectorAST * sel = new(m_doc->m_pool) MethodCallSelectorAST();
        sel->colon_token = m_lexer->currentLocation();
        m_lexer->nextToken();
        sel->method = new(m_doc->m_pool) IdentifierAST();
        sel->method->identifier_token = m_lexer->currentLocation();
        if(!checkNext(Token::T_NAME))
            return NULL;
        sel->arguments = funcArgsExpr();
        if(!sel->arguments)
            return NULL;
        return sel;
    }
    
    ExpressionAST * Parser::funcArgsExpr()
    {
        PARSER_DEBUG();
        
        switch(m_lexer->token())
        {
            case '(': {
                FunctionArgumentsExpressionAST * ast = new(m_doc->m_pool) FunctionArgumentsExpressionAST();
                ast->lbracket = m_lexer->currentLocation();
                m_lexer->nextToken();
                if(m_lexer->token() != ')') {
                    ast->args = comaSeparatedExprs();
                }
                ast->rbracket = m_lexer->currentLocation();
                if(!checkNext(')'))
                    return NULL;
                return ast;
            }
            case '{':
                return constructorExpr();
            case Token::T_STRING:
                return stringExpr();
            default:
                syntaxError("Function arguments expected");
                m_lexer->nextToken();
                return NULL;
        }
    }
    
    ExpressionStatementAST * Parser::exprStat()
    {
        PARSER_DEBUG();
        
        ExpressionStatementAST * stmt = new(m_doc->m_pool) ExpressionStatementAST();
        stmt->expr = primaryExpr();
        if(!stmt->expr)
            return NULL;
        
        // If this is function call, it's not assignment expression
        if(SelectorExpressionAST * _sel = stmt->expr->asSelectorExpression()) {
            if(_sel->chain) {
                SelectorAST * lastVal = _sel->chain->lastValue();
                if(lastVal->asFunctionCallSelector() || lastVal->asMethodCallSelector())
                    return stmt;
            }
        }
        
        stmt->expr = assignmentExpr(stmt->expr);
        
        return stmt;
    }
    
    AssignmentExpressionAST * Parser::assignmentExpr(ExpressionAST * expr)
    {
        PARSER_DEBUG();
        
        AssignmentExpressionAST * ast = new(m_doc->m_pool) AssignmentExpressionAST();
        
        ListBuildHelper<ComaSeparatedExpressionAST *> builder(m_doc->m_pool);
        
        ComaSeparatedExpressionAST * lhs_cse = new(m_doc->m_pool) ComaSeparatedExpressionAST();
        lhs_cse->expr = expr;
        
        while(m_lexer->token() == ',') {
            builder.append(lhs_cse);
            lhs_cse->coma_token = m_lexer->currentLocation();
            m_lexer->nextToken();
            lhs_cse = new(m_doc->m_pool) ComaSeparatedExpressionAST();
            lhs_cse->expr = primaryExpr();
            if(!lhs_cse->expr)
                return NULL;
        }

        builder.append(lhs_cse);
        ast->lhs = builder.list();
        
        ast->equal_token = m_lexer->currentLocation();
        if(!checkNext('='))
            return NULL;
        
        ast->rhs = comaSeparatedExprs();
        if(!ast->rhs)
            return NULL;
        
        return ast;
    }
    
}

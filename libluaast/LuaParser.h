// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#ifndef __libluaast__LuaParser__
#define __libluaast__LuaParser__

#include "LuaLexer.h"
#include "LuaASTFwd.h"

namespace LuaAST {
    
    class Parser
    {
    public:
        Parser(LuaDocument * doc, Lexer * lexer);
        ~Parser();
        
        void parse();

    private:
        void errorExpected(int token);
        bool check(int c);
        void syntaxError(const String& msg);
        bool testNext(int token);
        bool checkNext(int c);
        bool checkMatch(int what, int who, int where);
        bool blockFollow(bool withuntil);
        StatementListAST * statementList(int delimiting_token = -1, int delimiting_token2 = -1);
        StatementAST * statement();
        IfStatementAST * ifStat();
        WhileStatementAST * whileStat();
        DoStatementAST * doStat();
        StatementAST * forStat();
        RepeatStatementAST * repeatStat();
        FunctionStatementAST * funcStat();
        LocalStatementAST * localStat(const SourceLocation& localToken);
        LocalFunctionStatementAST * localFunc(const SourceLocation& localToken, const SourceLocation& functionToken);
        LabelStatementAST * labelStat();
        ReturnStatementAST * returnStat();
        BreakStatementAST * breakStat();
        GotoStatementAST * gotoStat();
        ExpressionStatementAST * exprStat();
        BlockAST * block(int delimiting_token, int delimiting_token2 = -1);
        ExpressionAST * expr();
        ExpressionAST * subExpr(int limit, int * p_nextop = NULL);
        ExpressionAST * simpleExp();
        
        ForNumStatementAST * forNumStat(const SourceLocation& forToken, const SourceLocation& var1Token);
        ForListStatementAST * forListStat(const SourceLocation& forToken, const SourceLocation& var1Token);
        
        IdentifierAST * nameFromToken(const SourceLocation& loc);
        ComaSeparatedIdentifierListAST * comaSeparatedIdentifierList();
        ComaSeparatedIdentifierListAST * comaSeparatedLocalVars(const SourceLocation& var1Token);
        ComaSeparatedExpressionListAST * comaSeparatedExprs();
        ComaSeparatedExpressionListAST * parameterList();
        
        FunctionNameAST * funcName();
        FunctionBodyAST * funcBody();
        
        ExpressionAST * primaryExpr();
        ConstructorAST * constructorExpr();
        FieldAST * field();
        ListFieldAST * listField();
        RecordFieldAST * recField();
        NameSelectorAST * nameSelector();
        IndexSelectorAST * indexSelector();
        FunctionExpressionAST * funcExpr();
        ExpressionAST * prefixExpr();
        FieldSelectorAST * fieldSel();
        MethodCallSelectorAST * methodSel();
        ExpressionAST * funcArgsExpr();
        StringAST * stringExpr();
        AssignmentExpressionAST * assignmentExpr(ExpressionAST * expr);
        
    private:
        LuaDocument * m_doc;
        Lexer * m_lexer;
    };
    
}

#endif /* defined(__libluaast__LuaParser__) */

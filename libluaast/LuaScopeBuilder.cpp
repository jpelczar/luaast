// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include "LuaScopeBuilder.h"
#include "LuaDocument.h"
#include "LuaASTBind.h"

namespace LuaAST {
    
    ScopeBuilder::ScopeBuilder(ScopeChain * chain) :
        m_chain(chain)
    {
    }
    
    ScopeBuilder::~ScopeBuilder()
    {
    }
    
    void ScopeBuilder::push(AST * node)
    {
        m_nodes.push_back(node);
        const ObjectValue * value = m_chain->document()->bind()->findObject(node);
        if(value)
            m_chain->appendScope(value);
    }
    
    void ScopeBuilder::push(const ASTList& list)
    {
        for(ASTList::const_iterator it = list.begin() ; it != list.end() ; ++it)
            push(*it);
    }
    
    void ScopeBuilder::pop()
    {
        m_nodes.pop_back();
    }
    
}

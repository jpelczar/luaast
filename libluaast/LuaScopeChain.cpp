// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include "LuaScopeChain.h"
#include "LuaValueOwner.h"
#include "LuaEvaluator.h"

namespace LuaAST {
    
    ScopeChain::Ptr ScopeChain::create(const LuaDocument::Ptr& doc, const Context::ContextPtr& context)
    {
        Ptr ptr(new ScopeChain(doc, context));
#ifdef LUAAST_QT
        ptr->m_weak_self = ptr.toWeakRef();
#else
        ptr->m_weak_self = ptr;
#endif
        return ptr;
    }
    
    ScopeChain::Ptr ScopeChain::clone()
    {
        Ptr ptr(new ScopeChain(*this));
#ifdef LUAAST_QT
        ptr->m_weak_self = ptr.toWeakRef();
#else
        ptr->m_weak_self = ptr;
#endif
        return ptr;
    }
    
    ScopeChain::ScopeChain(const LuaDocument::Ptr& doc, const Context::ContextPtr& context) :
        m_document(doc),
        m_context(context)
    {
        ValueOwner * valueOwner = context->valueOwner();
        m_globalScope = valueOwner->globalObject();
    }
    
    const Value * ScopeChain::lookup(const String& name, const ObjectValue ** foundInScope) const
    {
        ScopeList all(this->all());
        for(size_t i = all.size() ; i > 0 ; --i) {
            const ObjectValue * scope = all.at(i - 1);
            if(const Value * member = scope->lookupMember(name, m_context, NULL)) {
                if(foundInScope)
                    *foundInScope = scope;
                return member;
            }
        }
        if(foundInScope)
            *foundInScope = NULL;
        return m_context->valueOwner()->undefinedValue();
    }
    
    const Value * ScopeChain::evaluate(AST * node)
    {
        Evaluator evaluator(this, NULL);
        return evaluator(node);
    }

    void ScopeChain::appendScope(const ObjectValue * scope)
    {
        m_all.push_back(scope);
    }

}

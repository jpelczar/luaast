// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#ifndef __libluaast__LuaScopeChain__
#define __libluaast__LuaScopeChain__

#include "LuaDocument.h"
#include "LuaContext.h"
#include "LuaValue.h"
#ifdef LUAAST_QT
#include <QSharedPointer>
#else
#include <memory>
#endif

namespace LuaAST {
    
    class ScopeChain
    {
    public:
#ifdef LUAAST_QT
        typedef QSharedPointer<ScopeChain> Ptr;
#else
        typedef std::shared_ptr<ScopeChain> Ptr;
#endif
        
        static Ptr create(const LuaDocument::Ptr& doc, const Context::ContextPtr& context);
        Ptr clone();
        
        inline Context::ContextPtr context() const { return m_context; }
        
        const Value * lookup(const String& name, const ObjectValue ** scope) const;
        const Value * evaluate(AST * node);
        LuaDocument::Ptr document() const { return m_document; }

#ifdef LUAAST_QT
        typedef QList<const ObjectValue *> ScopeList;
#else
        typedef std::vector<const ObjectValue *> ScopeList;
#endif
        
        inline ScopeList all() const { return m_all; }

        void appendScope(const ObjectValue * scope);
        
        ScopeChain(const LuaDocument::Ptr& doc, const Context::ContextPtr& context);
    private:
        LuaDocument::Ptr m_document;
        Context::ContextPtr m_context;
        const ObjectValue * m_globalScope;
        ScopeList m_all;
#ifdef LUAAST_QT
        QWeakPointer<ScopeChain> m_weak_self;
#else
        std::weak_ptr<ScopeChain> m_weak_self;
#endif
    };
    
}

#endif /* defined(__libluaast__LuaScopeChain__) */

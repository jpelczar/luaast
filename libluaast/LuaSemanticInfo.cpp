// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include "LuaSemanticInfo.h"
#include "LuaScopeBuilder.h"
#include "LuaASTVisitor.h"
#include "LuaAST.h"

namespace LuaAST {
    
    class AstPath : public ASTVisitor
    {
        ASTList m_path;
        unsigned m_offset;
    public:
        ASTList operator ()(AST * node, unsigned offset)
        {
            m_offset = offset;
            m_path.clear();
            accept(node);
            return m_path;
        }
        
        bool containsOffset(const SourceLocation& from, const SourceLocation& to) const
        {
            return m_offset >= from.offset && m_offset < (to.offset + to.length);
        }
        
        bool preVisit(AST * ast)
        {
            if(BlockAST * block = ast->asBlock())
                return containsOffset(block->firstSourceLocation(), block->lastSourceLocation());
            if(StatementAST * stmt = ast->asStatement())
                return containsOffset(stmt->firstSourceLocation(), stmt->lastSourceLocation());
            if(ExpressionAST * expr = ast->asExpression())
                return containsOffset(expr->firstSourceLocation(), expr->lastSourceLocation());
            if(FunctionBodyAST * body = ast->asFunctionBody())
                return containsOffset(body->firstSourceLocation(), body->lastSourceLocation());
            return true;
        }
        
        virtual bool visit(ForNumStatementAST * stmt)
        {
            m_path.push_back(stmt);
            return true;
        }
        
        virtual bool visit(ForListStatementAST * stmt)
        {
            m_path.push_back(stmt);
            return true;
        }
        
        virtual bool visit(BlockAST * block)
        {
            m_path.push_back(block);
            return true;
        }
        
        virtual bool visit(ConstructorAST * expr)
        {
            m_path.push_back(expr);
            return true;
        }
        
        virtual bool visit(FunctionBodyAST * expr)
        {
            m_path.push_back(expr);
            return true;
        }
    };
    
    SemanticInfo::SemanticInfo()
    {
    }

    SemanticInfo::SemanticInfo(ScopeChain::Ptr scopeChain) :
        m_scopeChain(scopeChain)
    {
    }
    
    SemanticInfo::~SemanticInfo()
    {
    }
    
    ScopeChain::Ptr SemanticInfo::scopeChain(const ASTList& path) const
    {
        if(path.size() == 0)
            return m_scopeChain->clone();
        ScopeChain::Ptr ptr(m_scopeChain->clone());
#ifdef LUAAST_QT
        ScopeBuilder builder(ptr.data());
#else
        ScopeBuilder builder(ptr.get());
#endif
      
        builder.push(path);
        return ptr;
    }
    
    ASTList SemanticInfo::rangePath(int cursorPosition) const
    {
        ASTList path;
        for(size_t i = 0 ; i < ranges.size() ; ++i) {
            const Range& range = ranges[i];
            if(cursorPosition >= range.start && cursorPosition < range.end) {
                path.push_back(range.ast);
            }
        }
        return path;
    }
    
    ASTList SemanticInfo::astPath(int cursorPosition) const
    {
        AstPath pathBuilder;
        if(!document)
            return ASTList();
        return pathBuilder(document->ast(), cursorPosition);
    }
    
    AST * SemanticInfo::astNodeAt(int cursorPosition) const
    {
        ASTList path(astPath(cursorPosition));
        if(path.size() == 0)
            return NULL;
        return path[path.size() - 1];
    }
    
}

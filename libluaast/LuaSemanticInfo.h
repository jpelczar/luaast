// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#ifndef __libluaast__LuaSemanticInfo__
#define __libluaast__LuaSemanticInfo__

#include "LuaDocument.h"
#include "LuaContext.h"
#include "LuaScopeChain.h"

namespace LuaAST {
    
    class ScopeChain;
    
    class Range
    {
    public:
        AST * ast;
        unsigned start;
        unsigned end;
        
        Range() : ast(NULL)
        {
        }
    };
    
    class SemanticInfo
    {
    public:
        SemanticInfo();
        SemanticInfo(ScopeChain::Ptr scopeChain);
        ~SemanticInfo();
        
        ASTList astPath(int cursorPosition) const;
        AST * astNodeAt(int cursorPosition) const;
        ASTList rangePath(int cursorPosition) const;
        ScopeChain::Ptr scopeChain(const ASTList& path) const;
      
        LuaDocument::Ptr document;
        Snapshot::Ptr snapshot;
        Context::ContextPtr context;
#ifdef LUAAST_QT
        QList<Range> ranges;
        QList<LuaError> semanticMessages;
#else
        std::vector<Range> ranges;
        std::vector<LuaError> semanticMessages;
#endif
    private:
        ScopeChain::Ptr m_scopeChain;
    };
    
}

#endif /* defined(__libluaast__LuaSemanticInfo__) */

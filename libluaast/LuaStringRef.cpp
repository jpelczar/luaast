// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include "LuaStringRef.h"
#ifndef LUAAST_QT
#include <algorithm>
#include <cctype>

namespace LuaAST {
    
    char StringRef::c_ref[1];

    int StringRef::compare_helper(const char * s1, size_t l1, const char * s2, size_t l2)
    {
        if(s1 == s2) {
            if(l1 == l2)
                return 0;
            return l1 < l2 ? -1 : 1;
        }
        
        return strncmp(s1, s2, std::max(l1, l2));
    }
    
    StringRef StringRef::trimmed() const
    {
        if(!m_string)
            return StringRef();
        
        off_t _begin = this->m_pos;
        off_t _end = this->m_pos + this->m_length;
        
        while(_begin < _end) {
            if(!isspace(m_string->at(_begin)))
               break;
            ++_begin;
        }
               
        while(_end > _begin) {
            if(!isspace(m_string->at(_end - 1)))
                break;
            --_end;
        }
        
        return _begin == _end ? StringRef() : StringRef(m_string, _begin, _end - _begin);
    }
    
    StringRef StringRef::left(size_t n) const
    {
        if(n >= m_length)
            return *this;
        return StringRef(m_string, m_pos, n);
    }
    
    StringRef StringRef::right(size_t n) const
    {
        if(n >= m_length)
            return *this;
        return StringRef(m_string, m_pos + m_length - n, n);
    }
    
    StringRef StringRef::mid(off_t pos, ssize_t n) const
    {
        if(pos >= m_length)
            return StringRef();
        if(n < 0)
            return StringRef(m_string, m_pos + pos, m_length - (pos - m_pos));
        if(n > (m_length - pos))
            n = m_length - pos;
        return StringRef(m_string, m_pos + pos, n);
    }
    
}
#endif

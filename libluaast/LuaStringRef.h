// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#ifndef __libluaast__LuaStringRef__
#define __libluaast__LuaStringRef__

#ifndef LUAAST_QT
#include <string>
#include <cstring>
#include <cassert>

namespace LuaAST {
    
    class StringRef
    {
        const std::string * m_string;
        off_t m_pos;
        size_t m_length;
    public:
        inline StringRef() : m_string(0), m_pos(0), m_length(0) { }
        inline StringRef(const std::string * str, off_t pos, size_t len) : m_string(str), m_pos(pos), m_length(len) { }
        inline StringRef(const std::string * str) : m_string(str), m_pos(0), m_length(str ? str->length() : 0) { }
        
        inline const std::string * string() const { return m_string; }
        inline off_t position() const { return m_pos; }
        inline size_t size() const { return m_length; }
        inline size_t length() const { return m_length; }
      
        
        inline StringRef& operator = (const StringRef& o) {
            m_string = o.m_string;
            m_pos = o.m_pos;
            m_length = o.m_length;
            return *this;
        }
        
        inline std::string toString() const {
            return m_string ? m_string->substr(m_pos, m_length) : std::string();
        }
        
        inline void clear() {
            m_string = 0;
            m_pos = 0;
            m_length = 0;
        }
        
        inline const char * constData() const { return m_string ? m_string->c_str() + m_pos : c_ref; }
        inline const char * c_str() const { return m_string ? m_string->c_str() + m_pos : c_ref; }
        
        inline int compare(const std::string& s) const { return compare_helper(c_str(), length(), s.c_str(), s.length()); }
        inline int compare(const StringRef& s) const { return compare_helper(c_str(), length(), s.c_str(), s.length()); }
        inline int compare(const char * s) const { return compare_helper(c_str(), length(), s ? s : c_ref, s ? std::strlen(s) : 0); }
        
        inline bool operator == (const StringRef& o) const { return compare(o) == 0; }
        inline bool operator != (const StringRef& o) const { return compare(o) != 0; }
        inline bool operator < (const StringRef& o) const { return compare(o) < 0; }
        inline bool operator <= (const StringRef& o) const { return compare(o) <= 0; }
        inline bool operator > (const StringRef& o) const { return compare(o) > 0; }
        inline bool operator >= (const StringRef& o) const { return compare(o) >= 0; }
        
        inline bool operator == (const std::string& o) const { return compare(o) == 0; }
        inline bool operator != (const std::string& o) const { return compare(o) != 0; }
        inline bool operator < (const std::string& o) const { return compare(o) < 0; }
        inline bool operator <= (const std::string& o) const { return compare(o) <= 0; }
        inline bool operator > (const std::string& o) const { return compare(o) > 0; }
        inline bool operator >= (const std::string& o) const { return compare(o) >= 0; }
        
        StringRef trimmed() const;
        StringRef left(size_t n) const;
        StringRef right(size_t n) const;
        StringRef mid(off_t pos, ssize_t n = -1) const;
        
        inline const char& operator [] (off_t pos) const {
            assert(pos < m_length);
            return *(m_string->c_str() + m_pos + pos);
        }
        
    private:
        static int compare_helper(const char * s1, size_t l1, const char * s2, size_t l2);
        
    private:
        static char c_ref[1];
    };
    
}
#endif

#endif /* defined(__libluaast__LuaStringRef__) */

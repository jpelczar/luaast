// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


#include "LuaToken.h"
#include "LuaASTFwd.h"
#include <cstring>

namespace LuaAST {

#ifdef LUAAST_QT
static inline int _litcmp(const QChar * x1, const char * x2) {
    for ( ; *x1 == QChar(*x2); x1++, x2++)
        if(x1->unicode() == 0)
            return 0;
    return x1->unicode() < *x2 ? -1 : 1;
}
#else
static inline int _litcmp(const char * x1, const char * x2) { return strcmp(x1, x2); }
#endif

#ifdef LUAAST_QT
    QLatin1String Token::name(int kind)
#else
    const char * Token::name(int kind)
#endif
    {
        switch(kind)
        {
            case T_EOF_SYMBOL: return Literal("<eof>");
            case T_ERROR: return Literal("<error>");
            case '#': return Literal("#");
            case '+': return Literal("+");
            case '-': return Literal("-");
            case '*': return Literal("*");
            case '/': return Literal("/");
            case '%': return Literal("%");
            case '^': return Literal("^");
            case '=': return Literal("=");
            case '>': return Literal(">");
            case '<': return Literal("<");
            case '.': return Literal(".");
            case ':': return Literal(":");
            case '(': return Literal("(");
            case ')': return Literal(")");
            case '[': return Literal("[");
            case ']': return Literal("]");
            case '{': return Literal("{");
            case '}': return Literal("}");
            case ',': return Literal("???");
            case T_AND: return Literal("and"); // ok
            case T_BREAK: return Literal("break"); // ok
            case T_DO: return Literal("do"); // ok
            case T_ELSE: return Literal("else"); // ok
            case T_ELSEIF: return Literal("elseif"); // ok
            case T_END: return Literal("end"); // ok
            case T_FALSE: return Literal("false"); // ok
            case T_FOR: return Literal("for"); // ok
            case T_FUNCTION: return Literal("function");
            case T_GOTO: return Literal("goto"); // ok
            case T_IF: return Literal("if"); // ok
            case T_IN: return Literal("in"); // ok
            case T_LOCAL: return Literal("local"); // ok
            case T_NIL: return Literal("nil"); // ok
            case T_NOT: return Literal("not"); // ok
            case T_OR: return Literal("or"); // ok
            case T_REPEAT: return Literal("repeat"); // ok
            case T_RETURN: return Literal("return"); // ok
            case T_THEN: return Literal("then"); // ok
            case T_TRUE: return Literal("true"); // ok
            case T_UNTIL: return Literal("until"); // ok
            case T_WHILE: return Literal("while"); // ok
            case T_CONCAT: return Literal("..");
            case T_DOTS: return Literal("...");
            case T_EQ: return Literal("==");
            case T_GE: return Literal(">=");
            case T_LE: return Literal("<=");
            case T_NE: return Literal("~=");
            case T_DBCOLON: return Literal("::");
            case T_EOS: return Literal("<eos>");
            case T_NUMBER: return Literal("<number>");
            case T_NAME: return Literal("<name>");
            case T_STRING: return Literal("<string>");
            case T_COMMENT: return Literal("<comment>");
            default: return Literal("<???>");
        }
    }
    
#ifdef LUAAST_QT
    int Token::name_to_token(const QChar * s, size_t length)
#else
    int Token::name_to_token(const char * s, size_t length)
#endif
    {
        if(length == 2) {
            if(s[0] == 'd') {
                if(s[1] == 'o')
                    return T_DO;
            } else if(s[0] == 'i') {
                if(s[1] == 'f')
                    return T_IF;
                if(s[1] == 'n')
                    return T_IN;
            } else if(s[0] == 'o') {
                if(s[1] == 'r')
                    return T_OR;
            }
        }
        if(length == 3) {
            if(s[0] == 'a') {
                if(s[1] == 'n') {
                    if(s[2] == 'd')
                        return T_AND;
                }
            }
            if(s[0] == 'e') {
                if(s[1] == 'n') {
                    if(s[2] == 'd')
                        return T_END;
                }
            }
            if(s[0] == 'f') {
                if(s[1] == 'o') {
                    if(s[2] == 'r')
                        return T_FOR;
                }
            }
            if(s[0] == 'n') {
                if(s[1] == 'i') {
                    if(s[2] == 'l')
                        return T_NIL;
                }
                if(s[1] == 'o') {
                    if(s[2] == 't')
                        return T_NOT;
                }
            }
        }
        if(length == 4) {
            if(s[0] == 't') {
                if(s[1] == 'h') {
                    if(s[2] == 'e') {
                        if(s[3] == 'n')
                            return T_THEN;
                    }
                }
                if(s[1] == 'r') {
                    if(s[2] == 'u') {
                        if(s[3] == 'e')
                            return T_TRUE;
                    }
                }
            }
            if(s[0] == 'g') {
                if(s[1] == 'o') {
                    if(s[2] == 't') {
                        if(s[3] == 'o')
                            return T_GOTO;
                    }
                }
            }
            if(s[0] == 'e') {
                if(s[1] == 'l') {
                    if(s[2] == 's') {
                        if(s[3] == 'e')
                            return T_ELSE;
                    }
                }
            }
        }
        if(length == 5) {
            if(s[0] == 'f') {
                if(!_litcmp(s + 1, "alse"))
                    return T_FALSE;
            }
            if(s[0] == 'l') {
                if(!_litcmp(s + 1, "ocal"))
                    return T_LOCAL;
            }
            if(s[0] == 'u') {
                if(!_litcmp(s + 1, "ntil"))
                    return T_UNTIL;
            }
            if(s[0] == 'w') {
                if(!_litcmp(s + 1, "hile"))
                    return T_WHILE;
            }
            if(s[0] == 'b') {
                if(!_litcmp(s + 1, "reak"))
                    return T_BREAK;
            }
        }
        if(length == 6) {
            if(s[0] == 'e') {
                if(!_litcmp(s + 1, "lseif"))
                    return T_ELSEIF;
            }
            if(s[0] == 'r') {
                if(s[1] == 'e') {
                    if(!_litcmp(s + 2, "peat"))
                        return T_REPEAT;
                    if(!_litcmp(s + 2, "turn"))
                        return T_RETURN;
                }
            }
        }
        if(length == 8) {
            if(s[0] == 'f') {
                if(s[1] == 'u') {
                    if(s[2] == 'n') {
                        if(s[3] == 'c') {
                            if(s[4] == 't') {
                                if(s[5] == 'i') {
                                    if(s[6] == 'o') {
                                        if(s[7] == 'n')
                                            return T_FUNCTION;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        return T_NAME;
    }

}

// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


#ifndef __libluaast__LuaToken__
#define __libluaast__LuaToken__

#ifdef LUAAST_QT
#include <QString>
#else
#include <sys/types.h>
#endif

namespace LuaAST {

    class Token
    {
    public:
        
        enum Kind {
            T_EOF_SYMBOL = 0,
            T_ERROR,
            
            T_AND = 257,
            T_BREAK,
            T_DO,
            T_ELSE,
            T_ELSEIF,
            T_END,
            T_FALSE,
            T_FOR,
            T_FUNCTION,
            T_GOTO,
            T_IF,
            T_IN,
            T_LOCAL,
            T_NIL,
            T_NOT,
            T_OR,
            T_REPEAT,
            T_RETURN,
            T_THEN,
            T_TRUE,
            T_UNTIL,
            T_WHILE,
            T_CONCAT,
            T_DOTS,
            T_EQ,
            T_GE,
            T_LE,
            T_NE,
            T_DBCOLON,
            T_EOS,
            T_NUMBER,
            T_NAME,
            T_STRING,
            T_COMMENT_BEGIN,
            T_COMMENT,
            T_COMMENT_END
        };
        
#ifdef LUAAST_QT
        static QLatin1String name(int kind);
        static int name_to_token(const QChar * s, size_t length);
#else
        static const char *name(int kind);
        static int name_to_token(const char * s, size_t length);
#endif
    };
    
}

#endif /* defined(__libluaast__LuaToken__) */

// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include "LuaValue.h"
#include "LuaValueOwner.h"
#include "LuaAST.h"
#include "LuaASTVisitor.h"
#include "LuaScopeChain.h"
#include "LuaEvaluator.h"
#include "LuaASTPathBuilder.h"
#include "LuaASTBind.h"
#include "LuaScopeBuilder.h"

#ifdef LUAAST_QT
#include <QDebug>
#endif

#include <typeinfo>
#include <iostream>

namespace LuaAST {

    ValueVisitor::ValueVisitor()
    {
    }
    
    ValueVisitor::~ValueVisitor()
    {
    }
    
    Value::Value() :
        m_scope(NULL)
    {
    }
    
    Value::~Value()
    {
    }
    
    void UndefinedValue::accept(ValueVisitor * visitor) const
    {
        visitor->visit(this);
    }
    
    const UndefinedValue * UndefinedValue::asUndefinedValue() const
    {
        return this;
    }
    
    void NilValue::accept(ValueVisitor * visitor) const
    {
        visitor->visit(this);
    }
    
    const NilValue * NilValue::asNilValue() const
    {
        return this;
    }
    
    void BooleanValue::accept(ValueVisitor * visitor) const
    {
        visitor->visit(this);
    }
    
    const BooleanValue * BooleanValue::asBooleanValue() const
    {
        return this;
    }
    
    void StringValue::accept(ValueVisitor * visitor) const
    {
        visitor->visit(this);
    }
    
    const StringValue * StringValue::asStringValue() const
    {
        return this;
    }
    
    void NumberValue::accept(ValueVisitor * visitor) const
    {
        visitor->visit(this);
    }
    
    const NumberValue * NumberValue::asNumberValue() const
    {
        return this;
    }
    
    FunctionValue::FunctionValue(ValueOwner * valueOwner) :
        ObjectValue(valueOwner)
    {
    }
    
    FunctionValue::~FunctionValue()
    {
    }
    
    size_t FunctionValue::returnValueCount() const
    {
        return 0;
    }
    
    const Value * FunctionValue::returnValue(size_t) const
    {
        return m_valueOwner->nilValue();
    }
    
    size_t FunctionValue::namedArgumentCount() const
    {
        return 0;
    }
    
    String FunctionValue::argumentName(size_t index) const
    {
#ifdef LUAAST_QT
        return QString::fromLatin1("arg%1").arg(index + 1);
#else
        char temp[32];
        snprintf(temp, sizeof(temp) - 1, "arg%zd", index);
        return String(temp);
#endif
    }
    
    bool FunctionValue::isVariadic() const
    {
        return true;
    }
    
    const Value * FunctionValue::argument(size_t) const
    {
        return m_valueOwner->nilValue();
    }
    
    void FunctionValue::accept(ValueVisitor * visitor) const
    {
        visitor->visit(this);
    }
    
    const FunctionValue * FunctionValue::asFunctionValue() const
    {
        return this;
    }
    
    void FunctionValue::setMember(const String& name, const Value * value)
    {
    }
    
    void FunctionValue::removeMember(const String& name)
    {
    }
    
    const Value * FunctionValue::lookupMember(const String& name, const Context * context,
                                       const ObjectValue ** foundInObject,
                                       bool lookIntoMetatables) const
    {
        for(size_t i = 0 ; i < namedArgumentCount() ; ++i) {
            if(name == argumentName(i)) {
                if(foundInObject)
                    *foundInObject = this;
                return argument(i);
            }
        }
        return NULL;
    }
    
    ObjectValue::ObjectValue(ValueOwner * valueOwner) :
        m_valueOwner(valueOwner),
        m_metaTable(NULL)
    {
        valueOwner->registerValue(this);
    }   
    
    ObjectValue::~ObjectValue()
    {
    }
    
    ValueOwner * ObjectValue::valueOwner() const
    {
        return m_valueOwner;
    }
    
    const Value * ObjectValue::metaTable() const
    {
        return m_metaTable;
    }
    
    const ObjectValue * ObjectValue::asObjectValue() const
    {
        return this;
    }
    
    const Value * ObjectValue::lookupMember(const String& name, const Context::ContextPtr& context,
                               const ObjectValue ** foundInObject,
                               bool lookIntoMetatables) const
    {
        return lookupMember(name,
#ifdef LUAAST_QT
                            context.data(),
#else
                            context.get(),
#endif
                            foundInObject,
                            lookIntoMetatables);
    }
    
    void ObjectValue::setClassName(const String& className)
    {
        m_className = className;
    }
    
    void ObjectValue::visitMembers(MemberVisitor * visitor) const
    {
        
    }
    
    TableValue::TableValue(ValueOwner * valueOwner) :
        ObjectValue(valueOwner)
    {
    }
    
    TableValue::~TableValue()
    {
    }
    
    void TableValue::accept(ValueVisitor * visitor) const
    {
        visitor->visit(this);
    }
    
    const TableValue * TableValue::asTableValue() const
    {
        return this;
    }
    
    void TableValue::setMember(const String& name, const Value * value)
    {
#ifdef QT_DEBUG
        qDebug() << "Set member" << name << "of value" << value << "in" << this;
#else
        std::cout << "Set member " << name << " of value " << value << " in " << typeid(*this).name() << " " << this << std::endl;
#endif

        m_members[name] = value;
    }
    
    void TableValue::removeMember(const String& name)
    {
#ifdef LUAAST_QT
        m_members.remove(name);
#else
        m_members.erase(name);
#endif
    }
    
    const Value * TableValue::lookupMember(const String& name, const Context * context,
                               const ObjectValue ** foundInObject,
                               bool lookIntoMetatables) const
    {
#ifdef LUAAST_QT
        if(m_members.contains(name)) {
            if(foundInObject)
                *foundInObject = this;
            return m_members.value(name);
        }
#else
        Members::const_iterator it = m_members.find(name);
        if(it != m_members.end()) {
            if(foundInObject)
                *foundInObject = this;
            return it->second;
        }
#endif
        
        if(lookIntoMetatables && m_metaTable) {
            if(const ObjectValue * o = m_metaTable->asObjectValue()) {
                return o->lookupMember(name, context, foundInObject, lookIntoMetatables);
            }
        }
        
        if(foundInObject)
            *foundInObject = NULL;
        return NULL;
    }
    
    void TableValue::setMetaTable(const ObjectValue * value)
    {
        m_metaTable = value;
    }
    
    void TableValue::visitMembers(MemberVisitor * visitor) const
    {
        for(Members::const_iterator it = m_members.begin() ; it != m_members.end() ; ++it) {
            if(!visitor->visitMember(it->first, it->second))
                return;
        }
    }
    
    UserDataValue::UserDataValue(ValueOwner * valueOwner) :
        ObjectValue(valueOwner)
    {
    }
    
    UserDataValue::~UserDataValue()
    {
    }
    
    void UserDataValue::accept(ValueVisitor * visitor) const
    {
        visitor->visit(this);
    }
    
    const UserDataValue * UserDataValue::asUserDataValue() const
    {
        return this;
    }
    
    void UserDataValue::setMember(const String& name, const Value * value)
    {
    }
    
    void UserDataValue::removeMember(const String& name)
    {
    }
    
    const Value * UserDataValue::lookupMember(const String& name, const Context * context,
                               const ObjectValue ** foundInObject,
                               bool lookIntoMetatables) const
    {
        if(lookIntoMetatables && m_metaTable) {
            if(const ObjectValue * o = m_metaTable->asObjectValue()) {
                return o->lookupMember(name, context, foundInObject, lookIntoMetatables);
            }
        }
        
        if(foundInObject)
            *foundInObject = NULL;
        return NULL;
    }
    
    Function::Function(ValueOwner * valueOwner) :
        FunctionValue(valueOwner)
    {
    }
    
    Function::~Function()
    {
    }
    
    size_t Function::returnValueCount() const
    {
        return m_returnValues.size();
    }
    
    const Value * Function::returnValue(size_t index) const
    {
        return m_returnValues.at(index);
    }
    
    size_t Function::namedArgumentCount() const
    {
        return m_argumentNames.size();
    }
    
    String Function::argumentName(size_t index) const
    {
        return m_argumentNames.at(index);
    }
    
    bool Function::isVariadic() const
    {
        return m_isVariadic;
    }
    
    const Value * Function::argument(size_t index) const
    {
        return m_arguments.at(index);
    }
    
    void Function::addReturnValue(const Value * value)
    {
        m_returnValues.push_back(value);
    }
    
    void Function::addArgument(const Value * argument, const String& name)
    {
        if(name.size()) {
            while(m_argumentNames.size() < m_arguments.size()) {
                m_argumentNames.push_back(String());
            }
            m_argumentNames.push_back(name);
        }
        m_arguments.push_back(argument);
    }
    
    void Function::setVariadic(bool variadic)
    {
        m_isVariadic = variadic;
    }
    
    ConvertToString::ConvertToString(ValueOwner * valueOwner) :
        m_valueOwner(valueOwner),
        m_result(NULL)
    {
    }
    
    ConvertToString::~ConvertToString()
    {
    }
    
    const Value * ConvertToString::switchResult(const Value * result)
    {
        std::swap(m_result, result);
        return result;
    }
    
    const Value * ConvertToString::operator()(const Value * value)
    {
        const Value * prev = switchResult(NULL);
        if(value)
            value->accept(this);
        return switchResult(prev);
    }
    
    void ConvertToString::visit(const UndefinedValue *)
    {
        m_result = m_valueOwner->stringValue();
    }
    
    void ConvertToString::visit(const NilValue *)
    {
        m_result = m_valueOwner->stringValue();
    }
    
    void ConvertToString::visit(const BooleanValue *)
    {
        m_result = m_valueOwner->stringValue();
    }
    
    void ConvertToString::visit(const NumberValue *)
    {
        m_result = m_valueOwner->stringValue();
    }
    
    void ConvertToString::visit(const StringValue * value)
    {
        m_result = value;
    }
    
    void ConvertToString::visit(const TableValue * value)
    {
        if(const FunctionValue * toString = value_cast<FunctionValue>(value->lookupMember(Literal("__tostring"), NULL, NULL))) {
            m_result = value_cast<StringValue>(toString->returnValue(0));
        }
    }
    
    void ConvertToString::visit(const FunctionValue *)
    {
    }
    
    void ConvertToString::visit(const UserDataValue * value)
    {
        if(const FunctionValue * toString = value_cast<FunctionValue>(value->lookupMember(Literal("__tostring"), NULL, NULL))) {
            m_result = value_cast<StringValue>(toString->returnValue(0));
        }
    }
    
    ConvertToObject::ConvertToObject(ValueOwner * valueOwner) :
        m_valueOwner(valueOwner),
        m_result(NULL)
    {
    }
    
    ConvertToObject::~ConvertToObject()
    {
    }
    
    const Value * ConvertToObject::switchResult(const Value * result)
    {
        std::swap(m_result, result);
        return result;
    }
    
    const Value * ConvertToObject::operator()(const Value * value)
    {
        const Value * prev = switchResult(NULL);
        if(value)
            value->accept(this);
        return switchResult(prev);
    }
    
    void ConvertToObject::visit(const UndefinedValue *)
    {
        m_result = m_valueOwner->nilValue();
    }
    
    void ConvertToObject::visit(const NilValue * value)
    {
        m_result = value;
    }
    
    void ConvertToObject::visit(const BooleanValue *)
    {
    }
    
    void ConvertToObject::visit(const NumberValue *)
    {
    }
    
    void ConvertToObject::visit(const StringValue *)
    {
    }
    
    void ConvertToObject::visit(const TableValue * value)
    {
        m_result = value;
    }
    
    void ConvertToObject::visit(const FunctionValue * value)
    {
        m_result = value->returnValue(0);
    }
    
    void ConvertToObject::visit(const UserDataValue * value)
    {
        m_result = value;
    }
    
    String TypeId::operator()(const Value * value)
    {
        m_result = Literal("unknown");
        if(value)
            value->accept(this);
        return m_result;
    }
    
    void TypeId::visit(const UndefinedValue *)
    {
        m_result = Literal("undefined");
    }
    
    void TypeId::visit(const NilValue *)
    {
        m_result = Literal("nil");
    }
    
    void TypeId::visit(const BooleanValue *)
    {
        m_result = Literal("boolean");
    }
    
    void TypeId::visit(const NumberValue *)
    {
        m_result = Literal("number");
    }
    
    void TypeId::visit(const StringValue *)
    {
        m_result = Literal("string");
    }
    
    void TypeId::visit(const TableValue *)
    {
        m_result = Literal("table");
    }
    
    void TypeId::visit(const FunctionValue *)
    {
        m_result = Literal("function");
    }
    
    void TypeId::visit(const UserDataValue *)
    {
        m_result = Literal("userdata");
    }
    
    ASTFunctionValue::ASTFunctionValue(FunctionBodyAST * ast, const LuaDocument * doc, ValueOwner * valueOwner) :
        FunctionValue(valueOwner),
        m_body(ast),
        m_doc(doc),
        m_isVariadic(false)
    {
        
    }
    
    ASTFunctionValue::~ASTFunctionValue()
    {
    }
    
    FunctionBodyAST * ASTFunctionValue::ast() const
    {
        return m_body;
    }
    
    size_t ASTFunctionValue::namedArgumentCount() const
    {
        return m_argumentNames.size();
    }
    
    String ASTFunctionValue::argumentName(size_t index) const
    {
        return m_argumentNames.at(index);
    }
    
    bool ASTFunctionValue::isVariadic() const
    {
        return m_isVariadic;
    }
    
    const Value * ASTFunctionValue::returnValue(size_t index) const
    {
        if(index >= (size_t)m_argumentValues.size())
            return m_valueOwner->nilValue();
        return m_argumentValues.at(index);
    }
    
    bool ASTFunctionValue::getSourceLocation(String * fileName, int * line, int * column) const
    {
        *fileName = m_doc->fileName();
        *line = m_body->firstSourceLocation().startLine;
        *column = m_body->firstSourceLocation().startColumn;
        return true;
    }
    
    const ASTFunctionValue * ASTFunctionValue::asASTFunctionValue() const
    {
        return this;
    }
    
    void ASTFunctionValue::addArgument(const String& name, const Value * value)
    {
        m_argumentNames.push_back(name);
        m_argumentValues.push_back(value);
    }
    
    void ASTFunctionValue::visitMembers(MemberVisitor * visitor) const
    {
        for(size_t i = 0 ; i < m_argumentNames.size() ; ++i) {
            if(!visitor->visitMember(m_argumentNames[i], m_argumentValues[i]))
                return;
        }
    }
    
    Reference::Reference(ValueOwner * valueOwner) :
        m_valueOwner(valueOwner)
    {
        valueOwner->registerValue(this);
    }
    
    Reference::~Reference()
    {
    }
    
    const Reference * Reference::asReference() const
    {
        return this;
    }
    
    void Reference::accept(ValueVisitor * visitor) const
    {
        visitor->visit(this);
    }
    
    const Value * Reference::value(ReferenceContext *) const
    {
        return m_valueOwner->undefinedValue();
    }
    
    ASTVariableReference::ASTVariableReference(IdentifierAST * identifier, ExpressionAST * expr, const LuaDocument * doc, ValueOwner * valueOwner) :
        Reference(valueOwner),
        m_ast(identifier),
        m_expr(expr),
        m_doc(doc)
    {
    }
    
    ASTVariableReference::~ASTVariableReference()
    {
    }
    
    AST * ASTVariableReference::ast() const
    {
        return m_ast;
    }

    IdentifierAST * ASTVariableReference::asIdentifier() const
    {
        return m_ast;
    }
    
    ExpressionAST * ASTVariableReference::asExpression() const
    {
        return m_expr;
    }
    
    const ASTVariableReference * ASTVariableReference::asASTVariableReference() const
    {
        return this;
    }
    
    bool ASTVariableReference::getSourceLocation(String * fileName, int * line, int * column) const
    {
        IdentifierAST * ast = asIdentifier();
        if(!ast)
            return false;
        *fileName = m_doc->fileName();
        *line = ast->identifier_token.startLine;
        *column = ast->identifier_token.startColumn;
        return true;
    }

    const Value * ASTVariableReference::value(ReferenceContext * context) const
    {
        ExpressionAST * expr = asExpression();
        if(expr == NULL)
            return m_valueOwner->nilValue();
        
        LuaDocument::Ptr doc = m_doc->ptr();
        ScopeChain chain(doc, context->context());
        ASTPathBuilder astBuilder(doc);
        ScopeBuilder scopeBuilder(&chain);
        scopeBuilder.push(astBuilder((int)expr->firstSourceLocation().offset));
        Evaluator evalulator(&chain, context);
        
        return evalulator(expr);
    }
    
    ASTLabelReference::ASTLabelReference(LabelStatementAST * stmt, const LuaDocument * doc, ValueOwner * valueOwner) :
        Reference(valueOwner),
        m_label(stmt),
        m_doc(doc)
    {
    }

    ASTLabelReference::~ASTLabelReference()
    {
    }

    AST * ASTLabelReference::ast() const
    {
        return m_label;
    }

    const ASTLabelReference * ASTLabelReference::asASTLabelReference() const
    {
        return this;
    }

    bool ASTLabelReference::getSourceLocation(String * fileName, int * line, int * column) const
    {
        *fileName = m_doc->fileName();
        *line = m_label->ldb_token.startLine;
        *column = m_label->ldb_token.startColumn;
        return true;
    }

    const Value * ASTLabelReference::value(ReferenceContext *) const
    {
        return m_valueOwner->undefinedValue();
    }
    
    ASTTableValue::ASTTableValue(ConstructorAST * ast, const LuaDocument * doc, ValueOwner * valueOwner) :
        TableValue(valueOwner),
        m_ast(ast),
        m_doc(doc)
    {
    }
    
    ASTTableValue::~ASTTableValue()
    {
    }
    
    AST * ASTTableValue::ast() const
    {
        return m_ast;
    }
    
    const ASTTableValue * ASTTableValue::asASTTableValue() const
    {
        return this;
    }
    
    bool ASTTableValue::getSourceLocation(String * fileName, int * line, int * column) const
    {
        *fileName = m_doc->fileName();
        *line = m_ast->firstSourceLocation().startLine;
        *column = m_ast->firstSourceLocation().startColumn;
        return true;
    }
    
    MemberVisitor::~MemberVisitor()
    {
    }
    
    bool MemberVisitor::visitMember(const String& name, const Value * value)
    {
        return true;
    }

}

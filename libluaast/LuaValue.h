// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#ifndef __libluaast__LuaValue__
#define __libluaast__LuaValue__

#include "LuaASTFwd.h"
#include "LuaContext.h"

#ifdef LUAAST_QT
#include <QHash>
#include <QList>
#else
#include <map>
#include <vector>
#endif

namespace LuaAST {
    
    class UndefinedValue;
    class NilValue;
    class BooleanValue;
    class NumberValue;
    class StringValue;
    class TableValue;
    class FunctionValue;
    class UserDataValue;
    class ValueOwner;
    class ObjectValue;
    class ASTFunctionValue;
    class Reference;
    class ASTVariableReference;
    class ASTLabelReference;
    class ASTTableValue;

    class ValueVisitor
    {
    public:
        ValueVisitor();
        virtual ~ValueVisitor();
        
        virtual void visit(const UndefinedValue *) { }
        virtual void visit(const NilValue *) { }
        virtual void visit(const BooleanValue *) { }
        virtual void visit(const NumberValue *) { }
        virtual void visit(const StringValue *) { }
        virtual void visit(const TableValue *) { }
        virtual void visit(const FunctionValue *) { }
        virtual void visit(const UserDataValue *) { }
        virtual void visit(const Reference *) { }
    };
    
    class Value
    {
        Value(const Value&);
        Value& operator = (const Value&);
    public:
        Value();
        virtual ~Value();
        
        virtual void accept(ValueVisitor *) const = 0;

        virtual bool getSourceLocation(String * fileName, int * line, int * column) const { return false; }
        virtual const UndefinedValue * asUndefinedValue() const { return NULL; }
        virtual const NilValue * asNilValue() const { return NULL; }
        virtual const BooleanValue * asBooleanValue() const { return NULL; }
        virtual const NumberValue * asNumberValue() const { return NULL; }
        virtual const StringValue * asStringValue() const { return NULL; }
        virtual const TableValue * asTableValue() const { return NULL; }
        virtual const FunctionValue * asFunctionValue() const { return NULL; }
        virtual const UserDataValue * asUserDataValue() const { return NULL; }
        virtual const ObjectValue * asObjectValue() const { return NULL; }
        virtual const ASTFunctionValue * asASTFunctionValue() const { return NULL; }
        virtual const Reference * asReference() const { return NULL; }
        virtual const ASTVariableReference * asASTVariableReference() const { return NULL; }
        virtual const ASTLabelReference * asASTLabelReference() const { return NULL; }
        virtual const ASTTableValue * asASTTableValue() const { return NULL; }

    protected:
        ObjectValue * m_scope;
    };
    
#ifdef LUAAST_QT
    typedef QList<const Value *> ValueList;
#else
    typedef std::vector<const Value *> ValueList;
#endif
    
    template<typename _Ret> const _Ret * value_cast(const Value * value)
    {
        _Ret::MissingValueSpecialization();
        return 0;
    }
    
    template<> inline const UndefinedValue * value_cast(const Value * value)
    {
        if(value)
            return value->asUndefinedValue();
        return 0;
    }
    
    template<> inline const NilValue * value_cast(const Value * value)
    {
        if(value)
            return value->asNilValue();
        return 0;
    }
    
    template<> inline const BooleanValue * value_cast(const Value * value)
    {
        if(value)
            return value->asBooleanValue();
        return 0;
    }
    
    template<> inline const NumberValue * value_cast(const Value * value)
    {
        if(value)
            return value->asNumberValue();
        return 0;
    }
    
    template<> inline const StringValue * value_cast(const Value * value)
    {
        if(value)
            return value->asStringValue();
        return 0;
    }
    
    template<> inline const TableValue * value_cast(const Value * value)
    {
        if(value)
            return value->asTableValue();
        return 0;
    }
    
    template<> inline const FunctionValue * value_cast(const Value * value)
    {
        if(value)
            return value->asFunctionValue();
        return 0;
    }
    
    template<> inline const ObjectValue * value_cast(const Value * value)
    {
        if(value)
            return value->asObjectValue();
        return 0;
    }
    
    template<> inline const Reference * value_cast(const Value * value)
    {
        if(value)
            return value->asReference();
        return 0;
    }
    
    class UndefinedValue : public Value
    {
    public:
        virtual void accept(ValueVisitor *) const;
        virtual const UndefinedValue * asUndefinedValue() const;
    };
    
    class NilValue : public Value
    {
    public:
        virtual void accept(ValueVisitor *) const;
        virtual const NilValue * asNilValue() const;
    };
    
    class BooleanValue : public Value
    {
    public:
        virtual void accept(ValueVisitor *) const;
        virtual const BooleanValue * asBooleanValue() const;
    };
    
    class StringValue : public Value
    {
    public:
        virtual void accept(ValueVisitor *) const;
        virtual const StringValue * asStringValue() const;
    };
    
    class NumberValue : public Value
    {
    public:
        virtual void accept(ValueVisitor *) const;
        virtual const NumberValue * asNumberValue() const;
    };
    
    class MemberVisitor
    {
    public:
        virtual ~MemberVisitor();
        virtual bool visitMember(const String& name, const Value * value);
    };
    
    class ObjectValue : public Value
    {
    public:
        ObjectValue(ValueOwner * valueOwner);
        virtual ~ObjectValue();
        
        ValueOwner * valueOwner() const;
        const Value * metaTable() const;
        
        virtual void setMember(const String& name, const Value * value) = 0;
        virtual void removeMember(const String& name) = 0;
        
        virtual const Value * lookupMember(const String& name, const Context * context,
                                   const ObjectValue ** foundInObject,
                                   bool lookIntoMetatables = true) const = 0;
        
        const Value * lookupMember(const String& name, const Context::ContextPtr& context,
                                   const ObjectValue ** foundInObject,
                                   bool lookIntoMetatables = true) const;
        
        virtual const ObjectValue * asObjectValue() const;
        
        const String& className() const { return m_className; }
        void setClassName(const String& className);
        
        virtual void visitMembers(MemberVisitor * visitor) const;
        
    protected:
        ValueOwner * m_valueOwner;
    protected:
        const Value * m_metaTable;
        String m_className;
    };
    
    class FunctionValue : public ObjectValue
    {
    public:
        FunctionValue(ValueOwner * valueOwner);
        ~FunctionValue();
        
        virtual size_t returnValueCount() const;
        virtual const Value * returnValue(size_t index) const;
        virtual size_t namedArgumentCount() const;
        virtual String argumentName(size_t index) const;
        virtual bool isVariadic() const;
        virtual const Value * argument(size_t index) const;
        
        virtual void accept(ValueVisitor *) const;
        virtual const FunctionValue * asFunctionValue() const;
        
        virtual void setMember(const String& name, const Value * value);
        virtual void removeMember(const String& name);
        
        virtual const Value * lookupMember(const String& name, const Context * context,
                                           const ObjectValue ** foundInObject,
                                           bool lookIntoMetatables = true) const;
    };
    
    class TableValue : public ObjectValue
    {
    public:
        TableValue(ValueOwner * valueOwner);
        ~TableValue();
        
        virtual void accept(ValueVisitor *) const;
        virtual const TableValue * asTableValue() const;
        
        virtual void setMember(const String& name, const Value * value);
        virtual void removeMember(const String& name);
        
        virtual const Value * lookupMember(const String& name, const Context * context,
                                           const ObjectValue ** foundInObject,
                                           bool lookIntoMetatables = true) const;
        
        void setMetaTable(const ObjectValue * value);
        
        virtual void visitMembers(MemberVisitor * visitor) const;
    private:
#ifdef LUAAST_QT
        typedef QHash<QString, const Value *> Members;
        QHash<QString, const Value *> m_members;
#else
        typedef std::map<std::string, const Value *> Members;
        std::map<std::string, const Value *> m_members;
#endif
    };
    
    class UserDataValue : public ObjectValue
    {
    public:
        UserDataValue(ValueOwner * valueOwner);
        virtual ~UserDataValue();
        
        virtual void accept(ValueVisitor *) const;
        virtual const UserDataValue * asUserDataValue() const;
        
        virtual void setMember(const String& name, const Value * value);
        virtual void removeMember(const String& name);
        
        virtual const Value * lookupMember(const String& name, const Context * context,
                                           const ObjectValue ** foundInObject,
                                           bool lookIntoMetatables = true) const;
    };
    
    class Function : public FunctionValue
    {
    public:
        Function(ValueOwner * valueOwner);
        ~Function();
        
        virtual size_t returnValueCount() const;
        virtual const Value * returnValue(size_t index) const;
        virtual size_t namedArgumentCount() const;
        virtual String argumentName(size_t index) const;
        virtual bool isVariadic() const;
        virtual const Value * argument(size_t index) const;
        void addReturnValue(const Value * value);
        void addArgument(const Value * argument, const String& name = String());
        void setVariadic(bool variadic);
        
    private:
        ValueList m_arguments;
        StringList m_argumentNames;
        ValueList m_returnValues;
        bool m_isVariadic;
    };
    
    class ConvertToString : protected ValueVisitor
    {
    public:
        ConvertToString(ValueOwner * valueOwner);
        ~ConvertToString();
        
        const Value * operator()(const Value * value);
        
    protected:
        const Value * switchResult(const Value * result);
        
        virtual void visit(const UndefinedValue *);
        virtual void visit(const NilValue *);
        virtual void visit(const BooleanValue *);
        virtual void visit(const NumberValue *);
        virtual void visit(const StringValue *);
        virtual void visit(const TableValue *);
        virtual void visit(const FunctionValue *);
        virtual void visit(const UserDataValue *);
        
    private:
        ValueOwner * m_valueOwner;
        const Value * m_result;
    };
    
    class ConvertToObject : protected ValueVisitor
    {
    public:
        ConvertToObject(ValueOwner * valueOwner);
        ~ConvertToObject();
        
        const Value * operator()(const Value * value);
        
    protected:
        const Value * switchResult(const Value * result);
        
        virtual void visit(const UndefinedValue *);
        virtual void visit(const NilValue *);
        virtual void visit(const BooleanValue *);
        virtual void visit(const NumberValue *);
        virtual void visit(const StringValue *);
        virtual void visit(const TableValue *);
        virtual void visit(const FunctionValue *);
        virtual void visit(const UserDataValue *);
        
    private:
        ValueOwner * m_valueOwner;
        const Value * m_result;
    };
    
    class TypeId : protected ValueVisitor
    {
        String m_result;
    public:
        String operator()(const Value * value);
        
    protected:
        virtual void visit(const UndefinedValue *);
        virtual void visit(const NilValue *);
        virtual void visit(const BooleanValue *);
        virtual void visit(const NumberValue *);
        virtual void visit(const StringValue *);
        virtual void visit(const TableValue *);
        virtual void visit(const FunctionValue *);
        virtual void visit(const UserDataValue *);
    };
    
    class ASTFunctionValue : public FunctionValue
    {
        FunctionBodyAST * m_body;
        const LuaDocument * m_doc;
        StringList m_argumentNames;
        ValueList m_argumentValues;
        bool m_isVariadic;
    public:
        ASTFunctionValue(FunctionBodyAST * ast, const LuaDocument * doc, ValueOwner * valueOwner);
        ~ASTFunctionValue();
        
        FunctionBodyAST * ast() const;
        
        virtual size_t namedArgumentCount() const;
        virtual String argumentName(size_t index) const;
        virtual bool isVariadic() const;
        virtual const Value * returnValue(size_t index) const;
        
        virtual bool getSourceLocation(String * fileName, int * line, int * column) const;
        virtual const ASTFunctionValue * asASTFunctionValue() const;
        
        void setVariadic(bool variadic) { m_isVariadic = variadic; }
        void addArgument(const String& name, const Value * value);
        
        virtual void visitMembers(MemberVisitor * visitor) const;
    };
    
    class ReferenceContext;
    
    class Reference : public Value
    {
    public:
        Reference(ValueOwner * valueOwner);
        ~Reference();
        
        virtual AST * ast() const = 0;
        virtual const Reference * asReference() const;
        virtual void accept(ValueVisitor *) const;
        
        inline ValueOwner * valueOwner() const { return m_valueOwner; }
        
    protected:
        friend class ReferenceContext;
        virtual const Value * value(ReferenceContext * context) const;
        
    protected:
        ValueOwner * m_valueOwner;
    };
    
    class ASTVariableReference : public Reference
    {
        IdentifierAST * m_ast;
        ExpressionAST * m_expr;
        const LuaDocument * m_doc;
    public:
        ASTVariableReference(IdentifierAST *, ExpressionAST *, const LuaDocument * doc, ValueOwner * valueOwner);
        ~ASTVariableReference();
        
        AST * ast() const;
        IdentifierAST * asIdentifier() const;
        ExpressionAST * asExpression() const;

        const LuaDocument * document() const { return m_doc; }
        
        virtual const ASTVariableReference * asASTVariableReference() const;
        virtual bool getSourceLocation(String * fileName, int * line, int * column) const;
        
    protected:
        virtual const Value * value(ReferenceContext * context) const;
    };
    
    class ASTLabelReference : public Reference
    {
        LabelStatementAST * m_label;
        const LuaDocument * m_doc;
    public:
        ASTLabelReference(LabelStatementAST * stmt, const LuaDocument * doc, ValueOwner * valueOwner);
        ~ASTLabelReference();

        AST * ast() const;
        virtual const ASTLabelReference * asASTLabelReference() const;
        virtual bool getSourceLocation(String * fileName, int * line, int * column) const;

        const LuaDocument * document() const { return m_doc; }
        
    protected:
        virtual const Value * value(ReferenceContext * context) const;
    };
    
    class ASTTableValue : public TableValue
    {
        ConstructorAST * m_ast;
        const LuaDocument * m_doc;
    public:
        ASTTableValue(ConstructorAST * ast, const LuaDocument * doc, ValueOwner * valueOwner);
        ~ASTTableValue();
        
        AST * ast() const;
        virtual const ASTTableValue * asASTTableValue() const;
        virtual bool getSourceLocation(String * fileName, int * line, int * column) const;
        
        const LuaDocument * document() const { return m_doc; }
    };

}

#endif /* defined(__libluaast__LuaValue__) */

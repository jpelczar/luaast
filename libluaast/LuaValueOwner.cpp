// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include "LuaValueOwner.h"

namespace LuaAST {
    
    class SharedValueOwner : public ValueOwner
    {
    public:
        UndefinedValue m_undefinedValue;
        NilValue m_nilValue;
        NumberValue m_numberValue;
        StringValue m_stringValue;
        BooleanValue m_booleanValue;
        TableValue * m_globalObject;
        TableValue * m_objectMetaTable;
        TableValue * m_tableValue;
        
        SharedValueOwner() : ValueOwner(this) {
            Function * f;
            
            m_objectMetaTable = newTable(NULL);
            m_objectMetaTable->setClassName("MetaTable");
            
            m_globalObject = newTable();
            m_globalObject->setClassName(Literal("Global"));
            
            m_tableValue = newTable();
            m_tableValue->setClassName(Literal("Table"));
            
            addFunction(m_objectMetaTable, Literal("__index"), undefinedValue(), 2, false);
            addFunction(m_objectMetaTable, Literal("__newindex"), 3, false);
            m_objectMetaTable->setMember(Literal("__mode"), stringValue());
            addFunction(m_objectMetaTable, Literal("__call"), 1, true);
            m_objectMetaTable->setMember(Literal("__metatable"), undefinedValue());
            addFunction(m_objectMetaTable, Literal("__len"), numberValue(), 1, false);
            addFunction(m_objectMetaTable, Literal("__gc"), 1, false);
            addFunction(m_objectMetaTable, Literal("__unm"), undefinedValue(), 1, false);
            addFunction(m_objectMetaTable, Literal("__add"), undefinedValue(), 2, false);
            addFunction(m_objectMetaTable, Literal("__sub"), undefinedValue(), 2, false);
            addFunction(m_objectMetaTable, Literal("__mul"), undefinedValue(), 2, false);
            addFunction(m_objectMetaTable, Literal("__div"), undefinedValue(), 2, false);
            addFunction(m_objectMetaTable, Literal("__mod"), undefinedValue(), 2, false);
            addFunction(m_objectMetaTable, Literal("__pow"), undefinedValue(), 2, false);
            addFunction(m_objectMetaTable, Literal("__concat"), undefinedValue(), 2, false);
            addFunction(m_objectMetaTable, Literal("__eq"), booleanValue(), 2, false);
            addFunction(m_objectMetaTable, Literal("__lt"), booleanValue(), 2, false);
            addFunction(m_objectMetaTable, Literal("__le"), booleanValue(), 2, false);
            
            m_globalObject->setMember(Literal("_G"), m_globalObject);
            m_globalObject->setMember(Literal("_VERSION"), stringValue());
            
            addFunction(m_globalObject, Literal("assert"), 1, false);
            addFunction(m_globalObject, Literal("collectgarbage"), booleanValue(), 1, false);
            addFunction(m_globalObject, Literal("dofile"), undefinedValue(), 1, false);
            addFunction(m_globalObject, Literal("error"), 0, true);
            addFunction(m_globalObject, Literal("getmetatable"), undefinedValue(), 1, false);
            
            f = addFunction(m_globalObject, Literal("ipairs"), 1, false);
            f->addReturnValue(undefinedValue());
            f->addReturnValue(undefinedValue());
            
            f = addFunction(m_globalObject, Literal("loadfile"), 0, false);
            f->addArgument(stringValue(), Literal("file"));
            f->addArgument(stringValue(), Literal("mode"));
            f->addArgument(undefinedValue(), Literal("env"));
            
            f = addFunction(m_globalObject, Literal("load"), 0, false);
            f->addArgument(stringValue(), Literal("file"));
            f->addArgument(stringValue(), Literal("chunk"));
            f->addArgument(stringValue(), Literal("mode"));
            f->addArgument(undefinedValue(), Literal("env"));
            
            f = addFunction(m_globalObject, Literal("next"), undefinedValue(), 0, false);
            f->addArgument(tableValue(), Literal("table"));
            f->addArgument(undefinedValue(), Literal("iterator"));
            f->addReturnValue(undefinedValue());
            
            f = addFunction(m_globalObject, Literal("pairs"), undefinedValue(), 1, false);
            f->addReturnValue(undefinedValue());
            f->addReturnValue(undefinedValue());
            
            f = addFunction(m_globalObject, Literal("pcall"), undefinedValue(), 1, false);
            f->addReturnValue(undefinedValue());
            
            addFunction(m_globalObject, Literal("print"), 0, true);
            addFunction(m_globalObject, Literal("rawequal"), booleanValue(), 2, false);
            addFunction(m_globalObject, Literal("rawlen"), numberValue(), 1, false);
            
            f = addFunction(m_globalObject, Literal("rawget"), undefinedValue(), 0, false);
            f->addArgument(tableValue(), Literal("table"));
            f->addArgument(undefinedValue(), Literal("key"));
            
            f = addFunction(m_globalObject, Literal("rawset"), 0, false);
            f->addArgument(tableValue(), Literal("table"));
            f->addArgument(undefinedValue(), Literal("key"));
            f->addArgument(undefinedValue(), Literal("value"));
            
            addFunction(m_globalObject, Literal("select"), 0, true);
            
            f = addFunction(m_globalObject, Literal("setmetatable"), 0, false);
            f->addArgument(tableValue(), Literal("table"));
            f->addArgument(undefinedValue(), Literal("metatable"));
            
            f = addFunction(m_globalObject, Literal("tonumber"), numberValue(), 0, false);
            f->addArgument(undefinedValue(), Literal("value"));
            f->addArgument(numberValue(), Literal("base"));
            
            addFunction(m_globalObject, Literal("tostring"), stringValue(), 1, false);
            addFunction(m_globalObject, Literal("type"), stringValue(), 1, false);
            
            f = addFunction(m_globalObject, Literal("xpcall"), 0, false);
            f->addArgument(undefinedValue(), Literal("func"));
            f->addArgument(undefinedValue(), Literal("errorhandler"));
            
            TableValue * debugTable = newTable();
            m_globalObject->setMember(Literal("debug"), debugTable);
            
            addFunction(debugTable, Literal("debug"), 0, 0);
            addFunction(debugTable, Literal("getuservalue"), undefinedValue(), 1, false);
            
            f = addFunction(debugTable, Literal("gethook"), 1, false);
            f->addReturnValue(undefinedValue());
            f->addReturnValue(stringValue());
            f->addReturnValue(numberValue());
            
            addFunction(debugTable, Literal("getinfo"), tableValue(), 1, true);
            
            f = addFunction(debugTable, Literal("getlocal"), 0, false);
            f->addArgument(numberValue(), Literal("stacklevel"));
            f->addArgument(undefinedValue(), Literal("localvar"));
            f->addReturnValue(stringValue());
            f->addReturnValue(undefinedValue());
            
            addFunction(debugTable, Literal("getregistry"), undefinedValue(), 0, false);
            addFunction(debugTable, Literal("getmetatable"), tableValue(), 1, false);
            
            f = addFunction(debugTable, Literal("getupvalue"), undefinedValue(), 0, false);
            f->addArgument(numberValue(), Literal("index"));
            
            addFunction(debugTable, Literal("upvaluejoin"), undefinedValue(), 0, true);
            addFunction(debugTable, Literal("upvalueid"), undefinedValue(), 0, true);
            addFunction(debugTable, Literal("setuservalue"), undefinedValue(), 2, false);
            
            f = addFunction(debugTable, Literal("sethook"), 3, false);
            f->addArgument(undefinedValue(), Literal("hook"));
            f->addArgument(stringValue(), Literal("mask"));
            f->addArgument(numberValue(), Literal("count"));
            
            f = addFunction(debugTable, Literal("setlocal"), 3, false);
            f->addArgument(numberValue(), Literal("level"));
            f->addArgument(undefinedValue(), Literal("local"));
            f->addArgument(undefinedValue(), Literal("value"));
            
            addFunction(debugTable, Literal("setmetatable"), 2, false);
            
            f = addFunction(debugTable, Literal("setupvalue"), 0, false);
            f->addArgument(undefinedValue(), Literal("func"));
            f->addArgument(numberValue(), Literal("index"));
            f->addArgument(undefinedValue(), Literal("value"));
            
            addFunction(debugTable, Literal("traceback"), 0, true);
        }
    };
    
    SharedValueOwner * ValueOwner::sharedValueOwner()
    {
        static SharedValueOwner static_owner;
        return &static_owner;
    }
    
    ValueOwner::ValueOwner(const SharedValueOwner * shared)
    {
        if(shared)
            m_valueOwner = shared;
        else
            m_valueOwner = sharedValueOwner();
    }
    
    ValueOwner::~ValueOwner()
    {
#ifdef LUAAST_QT
        qDeleteAll(m_registeredValues);
#else
        for(ValueList::iterator it = m_registeredValues.begin() ; it != m_registeredValues.end() ; ++it)
            delete (*it);
 #endif
    }
    
    const UndefinedValue * ValueOwner::undefinedValue() const
    {
        return &m_valueOwner->m_undefinedValue;
    }
    
    const NilValue * ValueOwner::nilValue() const
    {
        return &m_valueOwner->m_nilValue;
    }
    
    const BooleanValue * ValueOwner::booleanValue() const
    {
        return &m_valueOwner->m_booleanValue;
    }
    
    const NumberValue * ValueOwner::numberValue() const
    {
        return &m_valueOwner->m_numberValue;
    }
    
    const StringValue * ValueOwner::stringValue() const
    {
        return &m_valueOwner->m_stringValue;
    }
    
    const ObjectValue * ValueOwner::globalObject() const
    {
        return m_valueOwner->m_globalObject;
    }
    
    const TableValue * ValueOwner::tableValue() const
    {
        return m_valueOwner->m_tableValue;
    }
    
    TableValue * ValueOwner::newTable()
    {
        return newTable(m_valueOwner->m_objectMetaTable);
    }
    
    TableValue * ValueOwner::newTable(const ObjectValue * metaTable)
    {
        TableValue * value = new TableValue(this);
        value->setMetaTable(metaTable);
        return value;
    }
    
    Function * ValueOwner::addFunction(ObjectValue * object, const String& name, const Value * result, int argumentCount, bool variadic)
    {
        Function * f = addFunction(object, name, argumentCount, variadic);
        f->addReturnValue(result);
        return f;
    }
    
    Function * ValueOwner::addFunction(ObjectValue * object, const String& name, int argumentCount, bool variadic)
    {
        Function * f = new Function(this);
        for(int i = 0 ; i < argumentCount ; ++i)
            f->addArgument(undefinedValue());
        f->setVariadic(variadic);
        object->setMember(name, f);
        return f;
    }

    void ValueOwner::registerValue(Value * value)
    {
        m_registeredValues.push_back(value);
    }
    
}

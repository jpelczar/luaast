// Copyright (c) 2014 Jaroslaw Pelczar <jpelczar@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#ifndef __libluaast__LuaValueOwner__
#define __libluaast__LuaValueOwner__

#include "LuaValue.h"

namespace LuaAST {
    
    class SharedValueOwner;
    
    class ValueOwner
    {
    public:
        static SharedValueOwner * sharedValueOwner();
        ValueOwner(const SharedValueOwner * shared = 0);
        ~ValueOwner();
        
        const UndefinedValue * undefinedValue() const;
        const NilValue * nilValue() const;
        const BooleanValue * booleanValue() const;
        const NumberValue * numberValue() const;
        const StringValue * stringValue() const;
        
        const ObjectValue * globalObject() const;
        const TableValue * tableValue() const;
        
        TableValue * newTable();
        TableValue * newTable(const ObjectValue * metaTable);
        Function * addFunction(ObjectValue * object, const String& name, const Value * result, int argumentCount, bool variadic);
        Function * addFunction(ObjectValue * object, const String& name, int argumentCount, bool variadic);

        void registerValue(Value * value);

    private:
        const SharedValueOwner  * m_valueOwner;
        ValueList m_registeredValues;
    };
    
}

#endif /* defined(__libluaast__LuaValueOwner__) */

#-------------------------------------------------
#
# Project created by QtCreator 2014-11-15T18:25:03
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = libluaast
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11

DEFINES += LUAAST_QT

SOURCES += main.cpp \
    LuaAST.cpp \
    LuaASTManaged.cpp \
    LuaASTMatcher.cpp \
    LuaASTVisitor.cpp \
    LuaDocPrinter.cpp \
    LuaDocument.cpp \
    LuaError.cpp \
    LuaLexer.cpp \
    LuaParser.cpp \
    LuaStringRef.cpp \
    LuaToken.cpp \
    LuaValue.cpp \
    LuaASTBind.cpp \
    LuaASTPathBuilder.cpp \
    LuaContext.cpp \
    LuaEvaluator.cpp \
    LuaScopeChain.cpp \
    LuaValueOwner.cpp

HEADERS += \
    LuaAST.h \
    LuaASTFwd.h \
    LuaASTManaged.h \
    LuaASTMatcher.h \
    LuaASTVisitor.h \
    LuaDocPrinter.h \
    LuaDocument.h \
    LuaError.h \
    LuaLexer.h \
    LuaParser.h \
    LuaStringRef.h \
    LuaToken.h \
    LuaValue.h \
    LuaASTBind.h \
    LuaASTPathBuilder.h \
    LuaContext.h \
    LuaEvaluator.h \
    LuaScopeChain.h \
    LuaValueOwner.h
